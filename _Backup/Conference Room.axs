PROGRAM_NAME='Conference Room'

(* ------------------------------------------------------- *)
(*                                                         *)
(*  FILE CREATED ON: 08/22/2016  AT: 11:28:35              *)
(*  FILE_LAST_MODIFIED_ON: 09/10/2016  AT: 15:29:07        *)
(*                                                         *)
(*  System Type : NetLinx                                  *)
(*                                                         *)
(* ------------------------------------------------------- *)
(*                                                         *)
(*  Jonathan Brenner                                       *)
(*  AVI Systems                                            *)
(*                                                         *)
(*  Client: Children's Hospital Association                *)
(*  Project: Lenexa Office - Conference Rooms              *)
(*                                                         *)
(* ------------------------------------------------------- *)

DEFINE_DEVICE

    dvMaster = 0:1:0;
    dvBiamp = 0:3:0;
    
    dvCOM1 = 5001:1:0;      // Kramer Video Switch
    dvCOM2 = 5001:2:0;      // Cisco Codec
    dvCOM3 = 5001:3:0;      // USB Switch
    dvCOM4 = 5001:4:0;      // Sharp Display
    
    dvTP_Main = 10001:1:0;
    dvTP_Presentation = 10001:2:0;
    dvTP_AudioCall = 10001:3:0;
    dvTP_VideoCall = 10001:4:0;
    dvTP_WebConf = 10001:5:0;



DEFINE_CONSTANT

    BIAMP_IP = '192.168.1.101';

    // Timelines
    TL_FEEDBACK = 1;
    TL_COOLING = 2;
    TL_WARMING = 3;
    TL_VOLUP = 4;
    TL_VOLDN = 5;
    
    // System States
    SYS_OFF = 0;
    SYS_WARM = 1;
    SYS_ON = 2;
    SYS_COOL = 3;
    
    // System Modes
    INACTIVE = 0;
    PRESENTATION = 21;
    AUDIO_CALL = 22;
    VIDEO_CALL = 23;
    WEB_CONF = 24;
    
    // Video Sources
    NOTHING = 0;
    LAPTOP = 1;
    ROOM_PC = 2;
    VIA = 3;
    VTC_CODEC = 4;
    
    // Display 
    DISPLAY_ON = 'POWR1   ';
    DISPLAY_OFF = 'POWR0   ';
    DISPLAY_HDMI = 'IAVD1   ';
    DISPLAY_RSPW = 'RSPW1   ';
    
    COOL_TIME = 12;
    WARM_TIME = 12;
    
    // Audio
    VOL_STEP = 3;
    MIN_VOL = -30.0;
    MAX_VOL = 0.0;
    
    

DEFINE_VARIABLE

    integer biampConnected = FALSE;

    char codecQueue[65535];
    char biampQueue[65535];

    integer systemState = SYS_OFF;
    integer systemMode = INACTIVE;

    integer displayOn = FALSE;
    integer videoSource = NOTHING;
    integer presentationSource = NOTHING;
    
    integer volumeLevel = 0;
    integer volumeMuted = FALSE;
    integer micMuted = FALSE;
    
    integer dialKeys[] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 25, 26 };
    char dialDigits[] = '0123456789*#.';
    
    integer acOffHook = FALSE;
    integer vcOffHook = FALSE;
    
    char acDialNumber[50];
    char vcDialNumber[50];
    char vcKeyboardText[255];
    
    integer vcContentSource = NOTHING;
    
    integer vcDoNotDisturb = FALSE;
    
    integer selfviewEnabled = FALSE;
    integer selfviewFullscreen = FALSE;
    char selfviewMonitor[20];
    char selfviewPosition[20];
    
    char incomingVideoCall_Number[50];
    char incomingVideoCall_Name[50];
    char incomingVideoCall_ID[10];
    
    char incomingAudioCall_Number[50];
    char incomingAudioCall_Name[50];
    
    integer cameraControls[] = { 132, 133, 134, 135, 158, 159 };
    integer cameraPresets[] = { 151, 152, 153, 154, 155, 156 };
    
    integer cameraPresetUpdated = FALSE;
    
    integer webConfSource = VIA;
    
    long tlFeedbackSteps[] = {100};
    long tlVolRampSteps[] = {250};
    long tlWarmingSteps[] = {1000};
    long tlCoolingSteps[] = {1000};
    
    char acRegisterCommands[][255] = {
        'SESSION set verbose false',
        'Program subscribe level 1 regProgramLevel 300',
        'Program subscribe mute 1 regProgramMute 300',
        'AudioCall subscribe level 1 regACLevel 300',
        'AudioCall subscribe mute 1 regACMute 300',
        'VideoCall subscribe level 1 regVCLevel 300',
        'VideoCall subscribe mute 1 regVCMute 300',
        'Mics subscribe mute 1 regMicMute 300',
        'Dialer subscribe callState 1 regDialerCallState 300'
    };
    
    char vcRegisterCommands[][255] = {
	'xPreferences outputmode terminal',
	'xFeedback register event/IncomingCallIndication',
	'xFeedback register event/Message',
	'xFeedback register event/SString',
	'xFeedback register event/TString',
	'xFeedback register Status/Standby',
	'xFeedback register Status/Call',
	//'xFeedback register Status/SystemUnit',
	//'xFeedback register Status/Network',
	'xFeedback register Status/Audio/Microphones',
	'xFeedback register Status/Conference',
	'xFeedback register Status/Video/Input',
	'xFeedback register Status/Preset',
	'xFeedback register Configuration',
	
	'xConfiguration Video/OSD',
	'xConfiguration Video/Selfview',
	//'xConfiguration Video/MainVideoSource',
	'xConfiguration Video/DefaultPresentationSource',
	'xConfiguration Video/Input/Source',
	//'xConfiguration H323',
	//'xConfiguration SIP',
	//'xConfiguration SystemUnit/Name',
	//'xConfiguration Conference',
	'xConfiguration Audio/Volume',
	
	//'xConfiguration Conference 1 AutoAnswer Mode',
	//'xConfiguration Conference 1 FarEndControl Mode',
	//'xConfiguration Video OSD Mode',
	'xConfiguration Conference 1 DoNotDisturb Mode',		 
	'xConfiguration Video Selfview',
	'xConfiguration Standby Control',
	
	'xStatus Conference',
	//'xStatus H323',
	//'xStatus Network',
	'xStatus Audio/Microphones',
	'xStatus Audio/Volume',
	'xStatus Standby',
	'xStatus Video Input',
	//'xStatus SystemUnit Software Version',
	//'xStatus Preset Defined',
	//'xStatus SystemUnit State',
	'xStatus Call'
    }; 
    

//----------------------------------------//
//  Functions
//----------------------------------------//
    
    //----------------------------------------//
    // System Startup
    //----------------------------------------//
    // wVideo: boolean
    //      0 -> Don't turn display(s) on
    //     !0 -> Turn display(s) on
    //----------------------------------------//
    DEFINE_FUNCTION systemStartup(integer wVideo)
    {
        // Start Warmup
        TIMELINE_CREATE(TL_WARMING, tlWarmingSteps, length_array(tlWarmingSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        send_command dvTP_Main, "'@PPN-Warming'";
        
        // Startup Display
        if (wVideo)
        {
            send_string dvCOM4, "DISPLAY_ON, $0D";
            
            wait 120 
	    {
		send_string dvCOM4, "DISPLAY_HDMI, $0D";
	    }
        }
        
        // Unmute Audio
        volumeMute(0);
        micMute(0);
        
        systemState = SYS_ON;
    }

    //----------------------------------------//
    // System Shutdown
    //----------------------------------------//
    DEFINE_FUNCTION systemShutdown()
    {
        // Start Cooldown
        TIMELINE_CREATE(TL_COOLING, tlCoolingSteps, length_array(tlCoolingSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        send_command dvTP_Main, "'PAGE-Start'";
        send_command dvTP_Main, "'@PPN-Cooling'";
        
        sourceSwitch(NOTHING);
        
        send_string dvCOM4, "DISPLAY_RSPW, $0D";
        
        wait 20 
	{
	    send_string dvCOM4, "DISPLAY_OFF, $0D";
	}
        
        volumeMute(1);
        micMute(1);
        
        acHangUp();
        vcHangUp();
        send_string dvCOM2, "'xCommand Standby Activate', $0D";
        
        systemState = SYS_OFF;
    }
    
    //----------------------------------------//
    // Main Source Selection
    //----------------------------------------//
    // src: input number (0-4)
    //----------------------------------------//
    DEFINE_FUNCTION sourceSwitch(integer src)
    {
	videoSource = src;
	
	// Unmute and Switch Video or mute if src is 0
	if (src > 0)
	    send_string dvCOM1, "'#VMUTE 1,0|#VID ', itoa(src), '>1', $0D";
	else
	    send_string dvCOM1, "'#VMUTE 1,1', $0D";
	
	// Switch Audio
	send_string dvBiamp, "'SourceSelector set sourceSelection ', itoa(src), $0D";
        
        // Switch USB
        if (src == ROOM_PC || src == VIA)
            send_string dvCOM3, "itoa(src-1), '!'";
    }

    //----------------------------------------//
    // Content Source Selection
    //----------------------------------------//
    // src: input number (0-4)
    //----------------------------------------//
    DEFINE_FUNCTION contentSourceSwitch(integer src)
    {
	vcContentSource = src;
	
	// Unmute and Switch Video or mute if src is 0
	if (src > 0)
	    send_string dvCOM1, "'#VMUTE 2,0|#VID ', itoa(src), '>2', $0D";
	else
	    send_string dvCOM1, "'#VMUTE 2,1', $0D";
	    
        // Switch Audio
	send_string dvBiamp, "'SourceSelector set sourceSelection ', itoa(src), $0D";
    }
    
    //----------------------------------------//
    // Volume Controls
    //----------------------------------------//
    DEFINE_FUNCTION volumeUp()
    {
	send_string 0, "'Program increment level 1 ', itoa(VOL_STEP), $0D";
	
        //if (!volumeMuted)
            send_string dvBiamp, "'Program increment level 1 ', itoa(VOL_STEP), $0D";
    }

    DEFINE_FUNCTION volumeDown()
    {
        //if (!volumeMuted)
            send_string dvBiamp, "'Program decrement level 1 ', itoa(VOL_STEP), $0D";
    }
    
    DEFINE_FUNCTION volumeMute(integer state)
    {
        if (state)
            send_string dvBiamp, "'Program set mute true', $0D";
        else
            send_string dvBiamp, "'Program set mute false', $0D";
    }
    
    DEFINE_FUNCTION volumeMuteToggle()
    {
        send_string dvBiamp, "'Program toggle mute 1', $0D";
    }
    
    DEFINE_FUNCTION micMute(integer state)
    {
        if (state)
            send_string dvBiamp, "'Mics set mute true', $0D";
        else
            send_string dvBiamp, "'Mics set mute false', $0D";
    }
    
    DEFINE_FUNCTION micMuteToggle()
    {
        send_string dvBiamp, "'Mics toggle mute 1', $0D";
    }
    
    //----------------------------------------//
    // Audio Call Controls
    //----------------------------------------//
    DEFINE_FUNCTION acDial()
    {
        send_string dvBiamp, "'Dialer dial 1 1 ', $22, acDialNumber, $22, $0D";
    }
    
    DEFINE_FUNCTION acHangUp()
    {
        send_string dvBiamp, "'Dialer onHook 1 1', $0D";
    }
    
    DEFINE_FUNCTION acKey(integer key)
    {   
	if (key < 25)
	{
	    acDialNumber = "acDialNumber, dialDigits[key-9]";
	    if (acOffHook)
		send_string dvBiamp, "'Dialer dtmf 1 ', dialDigits[key-9], $0D";
	}
	else if (key == 25) // Backspace
	{
	    integer length;
	    length = length_string(acDialNumber);
	    
	    if (length > 1)
		acDialNumber = left_string(acDialNumber, length-1);
	    else
		acDialNumber = '';
	}	    
	else if (key == 26) // Clear
	{
	    acDialNumber = '';
	}

        send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
    }
    
    //----------------------------------------//
    // Video Call Controls
    //----------------------------------------//
    DEFINE_FUNCTION vcDial()
    {
        send_string dvCOM2, "'xCommand Dial Number:', $22, vcDialNumber, $22, $0D";
    }
    
    DEFINE_FUNCTION vcHangUp()
    {
        send_string dvCOM2, "'xCommand Call DisconnectAll', $0D";
    }
    
    DEFINE_FUNCTION vcKey(integer key)
    {
        if (key < 25)
        {
            vcDialNumber = "vcDialNumber, dialDigits[key-9]";
            
            if (vcOffHook)
                send_string dvCOM2, "'xCommand DTMFSend DTMFString:', dialDigits[key-9], $0D";
        }
        else if (key == 25)
        {
            integer length;
            length = length_string(vcDialNumber);
            
            if (length > 1)
                vcDialNumber = left_string(vcDialNumber, length-1);
            else
                vcDialNumber = '';
        }
        else if (key == 26)
        {
            vcDialNumber = '';
        }
        send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
    }
    
    //----------------------------------------//
    // Initialize Codec
    //----------------------------------------//
    DEFINE_FUNCTION initCodec()
    {
        integer i;
        
        for (i = 1; i < 37; i++)
        {
            send_string dvCOM2, "vcRegisterCommands[i], $0D";
        }
    }
    
    //----------------------------------------//
    // Parse Codec Feeback
    //----------------------------------------//
    DEFINE_FUNCTION parseCodecFeedback(char str[])
    {
        char prefix[4];
    
        // Strip carraige return and line feed
        str = left_string(str, length_string(str)-2);
    
        prefix = remove_string(str, ' ', 1);
           
        if (prefix == '*s ')
        {
            if (find_string(str, '*s Call ', 1))
            {
                char status[20];
                
                remove_string(str, 'Status: ', 1);
                
                status = remove_string(str, ' ', 1);
                
                if (status == 'Connected ')
                {
                    vcOffHook = 1;
                    send_command dvTP_Main, '@PPF-IncomingVideoCall';
                }
                else if (status == 'Disconnected ' || status == 'Idle ')
                {
                    vcOffHook = 0;
                    send_command dvTP_Main, '@PPF-IncomingVideoCall';
                }
            }
            else
            {
                char message[255];
                message = remove_string(str, ': ', 1);
                
                select
                {
                    active (message == '*s Conference Presentation LocalSource: '): 
                        vcContentSource = atoi(str);
                    active (message == '*s Conference DoNotDisturb: '): 
                        vcDoNotDisturb = (str == 'Active');
                    active (message == '*s Video Selfview Mode: '):
                        selfviewEnabled = (str == 'On');
                    active (message == '*s Video Selfview FullscreenMode: '):
                        selfviewFullscreen = (str == 'On');
                    active (message == '*s Video Selfview PIPPosition: '):
                        selfviewPosition = str;
                    active (message == '*s Video Selfview OnMonitorRole: '):
                        selfviewMonitor = str;
                }     
            }               
        }
        else if (prefix == '*es ')
        {
            if (str == 'Event CameraPresetListUpdated')
            {
               cameraPresetUpdated = 1;
               wait 30 cameraPresetUpdated = 0;
            }
            else if (remove_string(str, 'Event IncomingCallIndication ', 1))
            {
                send_command dvTP_Main, '@PPN-IncomingVideoCall';
                                
                remove_string(str, 'RemoteURI: ', 1);
                incomingVideoCall_Number = remove_string(str, ' ', 1);
                
                remove_string(str, 'DisplayNameValue: ', 1);
                incomingVideoCall_Name = remove_string(str, ' ', 1);
                
                remove_string(str, 'CallId: ', 1);
                incomingVideoCall_ID = remove_string(str, ' ', 1);
                
                send_command dvTP_VideoCall, "'^TXT-2,0,', incomingVideoCall_Number";
            }
        }
    }
    
    //----------------------------------------//
    // Initialize Biamp
    //----------------------------------------//
    DEFINE_FUNCTION initBiamp()
    {
        integer i;
	
        for (i = 1; i < 10; i++)
        {
            //acRegisterCommand = acRegisterCommands[i];
	    send_string dvBiamp, "acRegisterCommands[i], $0D";
	    send_string 0, "acRegisterCommands[i], $0D";
        }
    }
    
    //----------------------------------------//
    // Parse Biamp Feeback
    //----------------------------------------//
    DEFINE_FUNCTION parseBiampFeedback(char str[])
    {
        // Strip carraige return and line feed
        str = left_string(str, length_string(str)-2);
	
	if (find_string(str, 'Welcome', 1))
	{
	    initBiamp();
	}
        else if (remove_string(str, '! "', 1))
        {
            char tag[50];
            tag = remove_string(str, '" ', 1);
            tag = left_string(tag, length_string(tag)-2);
            
            select
            {
                active (tag == 'regProgramLevel'):
                {
                    float level;
                    level = atof(str);
                    
                    level = ((level - MIN_VOL) * 100) / (MAX_VOL - MIN_VOL);
                    
                    volumeLevel = type_cast(level);
                    send_level dvTP_Main, 1, volumeLevel;
                }
                active (tag == 'regProgramMute'):
                {
                    if (str == 'true')
                        volumeMuted = TRUE;
                    else
                        volumeMuted = FALSE;
                }
                active (tag == 'regMicMute'):
                {
                    if (str == 'true')
                        micMuted = TRUE;
                    else
                        micMuted = FALSE;
                }
                active (tag == 'regDialerCallState'):
                {
                    if (remove_string(str, '[[[', 1))
                    {
                        integer callState, lineID, callID;
                        char callerID[100];
                        
                        callState = atoi(remove_string(str, ' ', 1));
                        lineID = atoi(remove_string(str, ' ', 1));
                        callID = atoi(remove_string(str, ' ', 1));
                        
                        if (lineID == 0 && callID == 0)
                        {
                            if (callState == 13)
                            {
                                send_command dvTP_Main, '@PPF-IncomingAudioCall';
                                acOffHook = TRUE;
                            }
                            else
                            {
                                acOffHook = FALSE;
                                
                                if (callState == 8)
                                {
                                    remove_string(str, ' "', 1);
                                    callerID = remove_string(str, '" ', 1);
                                    callerID = left_string(callerID, length_string(callerID)-1);
                                    remove_string(callerID, "$5C, $22, $5C, $22", 1);
                                    
                                    incomingAudioCall_Number = remove_string(callerID, "$5C, $22, $5C, $22", 1);
                                    incomingAudioCall_Number = left_string(incomingAudioCall_Number, length_string(incomingAudioCall_Number)-4);
                                    
                                    incomingAudioCall_Name = remove_string(callerID, "$5C", 1);
                                    incomingAudioCall_Name = left_string(incomingAudioCall_Name, length_string(incomingAudioCall_Name)-1);
                                    
                                    send_command dvTP_Main, '@PPN-IncomingAudioCall';
                                    send_command dvTP_AudioCall, "'^TXT-2,0,', incomingAudioCall_Number";
                                }
                            }
                        }   
                    }
                }
            }
        }	
    }


DEFINE_START
    
    CREATE_BUFFER dvCOM2, codecQueue;
    CREATE_BUFFER dvBiamp, biampQueue;



DEFINE_EVENT

//----------------------------------------//
//  Logo
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_Main, 10]
    {
        HOLD[100]:
        {
            // TODO: Setup Page
        }
    }
    

//----------------------------------------//
//  Shutdown
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_Main, 9]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Shutdown'";
        }
    }
    
    BUTTON_EVENT[dvTP_Main, 11]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPF-Shutdown'";
        }
    }
    
    BUTTON_EVENT[dvTP_Main, 12]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPF-Shutdown'";
            systemShutdown();
        }
    }
    

//----------------------------------------//
//  Startup Modes               
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_Main, PRESENTATION]   // Presentation Mode
    BUTTON_EVENT[dvTP_Main, AUDIO_CALL]     // Audio Call Mode
    BUTTON_EVENT[dvTP_Main, VIDEO_CALL]     // Video Call Mode
    BUTTON_EVENT[dvTP_Main, WEB_CONF]       // Web Conference Mode
    {
        PUSH:
        {    
            integer usingVideo;
            usingVideo = 1;
                    
            switch (button.input.channel)
            {
                case PRESENTATION:
                {
		    if (presentationSource <> 0)
			sourceSwitch(presentationSource);
		    else
			sourceSwitch(LAPTOP);
                    send_command dvTP_Main, "'PAGE-Presentation'";
                }
                case AUDIO_CALL:
                {
                    usingVideo = 0;
                    send_command dvTP_Main, "'PAGE-Audio Call'";
                }
                case VIDEO_CALL:
                {
                    sourceSwitch(VTC_CODEC);
                    send_command dvTP_Main, "'PAGE-Video Call'";   
                    send_command dvTP_Main, "'@PPN-Dial;Video Call'";
                    send_string dvCOM2, "'xCommand Standby Deactivate', $0D";
                }
                case WEB_CONF:
                {
                    sourceSwitch(webConfSource);
                    send_command dvTP_Main, "'PAGE-Web Call'";
                    send_string dvCOM2, "'xCommand Standby Deactivate', $0D";
                }
            }
            
            if (systemState == SYS_OFF)
                systemStartup(usingVideo);
        }
    }
    
    
//----------------------------------------//
//  Volume Controls             
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_Main, 30]
    {
        PUSH:
        { 
            volumeUp();
        }
        HOLD[10]:
        { 
            TIMELINE_CREATE(TL_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE); 
        }
        RELEASE:
        { 
            TIMELINE_KILL(TL_VOLUP); 
        }
    }
    
    BUTTON_EVENT[dvTP_Main, 31]
    {
        PUSH:
        { 
            volumeDown();
        }
        HOLD[10]:
        { 
            TIMELINE_CREATE(TL_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE); 
        }
        RELEASE:
        { 
            TIMELINE_KILL(TL_VOLDN); 
        }
    }
    
    BUTTON_EVENT[dvTP_Main, 32]
    {
        PUSH:
        { 
            volumeMuteToggle();
        }
    }
    
    BUTTON_EVENT[dvTP_Main, 35]
    {
        PUSH:
        { 
            micMuteToggle();
        }
    }
    
    
//----------------------------------------//
//  Source Selection
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_Presentation, 0]
    {
        PUSH:
        {
            sourceSwitch(button.input.channel);
	    presentationSource = button.input.channel;
        }
    }
    
    
//----------------------------------------//
//  Audio Call Controls
//----------------------------------------//

    // Dial Button
    BUTTON_EVENT[dvTP_AudioCall, 5]
    {
        PUSH:
        { 
            acDial(); 
        }
    }
    
    // Hang Up Button
    BUTTON_EVENT[dvTP_AudioCall, 6]
    {
        PUSH:
        { 
            acHangUp(); 
        }
    }
    
    // Answer Incoming Call
    BUTTON_EVENT[dvTP_AudioCall, 7]
    {
        PUSH:
        { 
            send_command dvTP_Main, '@PPF-IncomingAudioCall'; 
            send_string dvBiamp, "'Dialer answer 1 1', $0D";
            send_command dvTP_Main, 'PAGE-AudioCall';
        }
    }
    
    // Reject Incoming Call
    BUTTON_EVENT[dvTP_AudioCall, 8]
    {
        PUSH:
        { 
            send_command dvTP_Main, '@PPF-IncomingAudioCall'; 
            send_string dvBiamp, "'Dialer answer 1 1', $0D";     
            acHangUp();
        }
    }
    
    // Keypad Keys
    BUTTON_EVENT[dvTP_AudioCall, dialKeys]
    {
        PUSH:
        { 
            acKey(button.input.channel); 
        }
    }
    
    
//----------------------------------------//
//  Video Call Controls        
//----------------------------------------//
    
    // Text Area Press (to bring up keyboard)
    BUTTON_EVENT[dvTP_VideoCall, 1]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@AKB-;Enter Address'";
        }
    }
    
    // Flip to Content Page
    BUTTON_EVENT[dvTP_VideoCall, 2]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Content'";
        }
    }
    
    // Flip to Dial Page
    BUTTON_EVENT[dvTP_VideoCall, 3]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Dial'";
        }
    }
    
    // Dial Button
    BUTTON_EVENT[dvTP_VideoCall, 5]
    {
        PUSH:
        { 
            vcDial(); 
        }
    }
    
    // Hang Up Button
    BUTTON_EVENT[dvTP_VideoCall, 6]
    {
        PUSH:
        { 
            vcHangUp(); 
        }
    }
    
    // Answer Incoming Call
    BUTTON_EVENT[dvTP_VideoCall, 7]
    {
        PUSH:
        { 
            send_string dvCOM2, "'xCommand Call Accept\n'";
        }
    }
    
    // Reject Incoming Call
    BUTTON_EVENT[dvTP_VideoCall, 8]
    {
        PUSH:
        { 
            send_string dvCOM2, "'xCommand Call Reject\n'";
        }
    }
    
    // Keypad Keys
    BUTTON_EVENT[dvTP_VideoCall, dialKeys]
    {
        PUSH:
        { 
            vcKey(button.input.channel); 
        }
    }
    
    // Menu Navigation
    BUTTON_EVENT[dvTP_VideoCall, 45]
    BUTTON_EVENT[dvTP_VideoCall, 46]
    BUTTON_EVENT[dvTP_VideoCall, 47]
    BUTTON_EVENT[dvTP_VideoCall, 48]
    BUTTON_EVENT[dvTP_VideoCall, 49]
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 45:{ send_string dvCOM2, "'xCommand Key Press Key: Up', $0D"; }
                case 46:{ send_string dvCOM2, "'xCommand Key Press Key: Down', $0D"; }
                case 47:{ send_string dvCOM2, "'xCommand Key Press Key: Left', $0D"; }
                case 48:{ send_string dvCOM2, "'xCommand Key Press Key: Right', $0D"; }
                case 49:{ send_string dvCOM2, "'xCommand Key Press Key: Ok', $0D"; }
            }
        }
        
        RELEASE:
        {
            switch(button.input.channel)
            {
                case 45:{ send_string dvCOM2, "'xCommand Key Release Key: Up', $0D"; }
                case 46:{ send_string dvCOM2, "'xCommand Key Release Key: Down', $0D"; }
                case 47:{ send_string dvCOM2, "'xCommand Key Release Key: Left', $0D"; }
                case 48:{ send_string dvCOM2, "'xCommand Key Release Key: Right', $0D"; }
                case 49:{ send_string dvCOM2, "'xCommand Key Release Key: Ok', $0D"; }
            }
        }
    }
    
    // Home Button
    BUTTON_EVENT[dvTP_VideoCall, 50]
    {
        PUSH:{ send_string dvCOM2, "'xCommand Key Click Key: Home', $0D"; }
    }
    // Back Button
    BUTTON_EVENT[dvTP_VideoCall, 81]
    {
        PUSH:{ send_string dvCOM2, "'xCommand Key Click Key: C', $0D"; }
    }
    // Layout Button
    BUTTON_EVENT[dvTP_VideoCall, 99]
    {
        PUSH:{ send_string dvCOM2, "'xCommand Key Click Key: Layout', $0D"; }
    }
    // Directory Button
    BUTTON_EVENT[dvTP_VideoCall, 105]
    {
        PUSH:{ send_string dvCOM2, "'xCommand Key Click Key: PhoneBook', $0D"; }
    }
    
    // Content Source Selection
    BUTTON_EVENT[dvTP_VideoCall, 120]
    BUTTON_EVENT[dvTP_VideoCall, 121]
    BUTTON_EVENT[dvTP_VideoCall, 122]
    BUTTON_EVENT[dvTP_VideoCall, 123]
    {
        PUSH:
        {
            contentSourceSwitch(button.input.channel-120);
	    
	    if (button.input.channel-120)
		send_string dvCOM2, "'xCommand Presentation Stop', $0D";
	    else
		send_string dvCOM2, "'xCommand Presentation Start PresentationSource: 3', $0D";
        }
    }    
    
    // Self View On/Off
    BUTTON_EVENT[dvTP_VideoCall, 160]
    BUTTON_EVENT[dvTP_VideoCall, 161]
    {
        PUSH:
        {
            if (button.input.channel - 160)
                send_string dvCOM2, "'xConfiguration Video Selfview: On', $0D";
            else
                send_string dvCOM2, "'xConfiguration Video Selfview: Off', $0D";
        }
    }
    
    // Self View Fullscreen On/Off
    BUTTON_EVENT[dvTP_VideoCall, 162]
    BUTTON_EVENT[dvTP_VideoCall, 163]
    {
        PUSH:
        {
            if (button.input.channel - 162)
                send_string dvCOM2, "'xCommand Video Selfview Set FullscreenMode: On', $0D";
            else
                send_string dvCOM2, "'xCommand Video Selfview Set FullscreenMode: Off', $0D";
        }
    }
    
    // Self View Show on Left/Right
    BUTTON_EVENT[dvTP_VideoCall, 164]
    BUTTON_EVENT[dvTP_VideoCall, 165]
    {
        PUSH:
        {
            if (button.input.channel - 164)
                send_string dvCOM2, "'xCommand Video Selfview Set OnMonitorRole: First', $0D";
            else
                send_string dvCOM2, "'xCommand Video Selfview Set OnMonitorRole: Second', $0D";
        }
    }
    
    // Self View PIP position
    BUTTON_EVENT[dvTP_VideoCall, 171]   // UpperLeft
    BUTTON_EVENT[dvTP_VideoCall, 172]   // UpperCenter
    BUTTON_EVENT[dvTP_VideoCall, 173]   // UpperRight
    BUTTON_EVENT[dvTP_VideoCall, 174]   // CenterLeft
    BUTTON_EVENT[dvTP_VideoCall, 175]   // CenterRight
    BUTTON_EVENT[dvTP_VideoCall, 176]   // LowerLeft
    BUTTON_EVENT[dvTP_VideoCall, 177]   // LowerRight
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 171:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: UpperLeft', $0D"; }
                case 172:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: UpperCenter', $0D"; }
                case 173:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: UpperRight', $0D"; }
                case 174:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: CenterLeft', $0D"; }
                case 175:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: CenterRight', $0D"; }
                case 176:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: LowerLeft', $0D"; }
                case 177:{ send_string dvCOM2, "'xCommand Video Selfview Set PIPPosition: LowerRight', $0D"; }                
            }
        }
    }
    
        
//----------------------------------------//
//  Camera Controls             
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_VideoCall, cameraControls]
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 132:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Tilt: Up', $0D"; }
                case 133:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Tilt: Down', $0D"; }
                case 134:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Pan: Left', $0D"; }
                case 135:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Pan: Right', $0D"; }
                case 158:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Zoom: Out', $0D"; }
                case 159:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Zoom: In', $0D"; }
            }
        }
        
        RELEASE:
        {
            switch(button.input.channel)
            {
                case 132:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Tilt: Stop', $0D"; }
                case 133:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Tilt: Stop', $0D"; }
                case 134:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Pan: Stop', $0D"; }
                case 135:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Pan: Stop', $0D"; }
                case 158:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Zoom: Stop', $0D"; }
                case 159:{ send_string dvCOM2, "'xCommand Camera Ramp CameraId: 1 Zoom: Stop', $0D"; }
            }
        }
    }
    
    BUTTON_EVENT[dvTP_VideoCall, cameraPresets]
    {
        PUSH:
        {
            send_string dvCOM2, "'xCommand Camera PositionActivateFromPreset CameraId: 1 PresetId: ', itoa(button.input.channel-150), $0D";
        }
        
        HOLD[30]:
        {
            send_string dvCOM2, "'xCommand Camera Preset Store CameraId: 1 PresetId: ', itoa(button.input.channel-150), $0D";
        }
    }
    
    
//----------------------------------------//
//  Web Conference              
//----------------------------------------//
    
    BUTTON_EVENT[dvTP_WebConf, 122]     // Use Room PC
    BUTTON_EVENT[dvTP_WebConf, 123]     // Use VIA
    {
        PUSH:
        {
            sourceSwitch(button.input.channel-120);
        }
    }
    
    
//----------------------------------------//
//  Timelines                   
//----------------------------------------//
    
    TIMELINE_EVENT[TL_FEEDBACK]
    {
        // Main
        [dvTP_Main, PRESENTATION] = (systemMode == PRESENTATION);
        [dvTP_Main, AUDIO_CALL] = (systemMode == AUDIO_CALL);
        [dvTP_Main, VIDEO_CALL] = (systemMode == VIDEO_CALL);
        [dvTP_Main, WEB_CONF] = (systemMode == WEB_CONF);
        
        [dvTP_Main, 32] = volumeMuted;
        [dvTP_Main, 35] = micMuted;
        
        
        // Presentation
        [dvTP_Presentation, ROOM_PC] = (videoSource == ROOM_PC);
        [dvTP_Presentation, LAPTOP] = (videoSource == LAPTOP);
        [dvTP_Presentation, VIA] = (videoSource == VIA);
        
        
        // Video Call 
        [dvTP_VideoCall, 120] = (vcContentSource == NOTHING);
        [dvTP_VideoCall, 121] = (vcContentSource == ROOM_PC);
        [dvTP_VideoCall, 122] = (vcContentSource == LAPTOP);
        [dvTP_VideoCall, 123] = (vcContentSource == VIA);
        
        [dvTP_VideoCall, 160] = selfviewEnabled;
        [dvTP_VideoCall, 161] = selfviewEnabled;
        
        [dvTP_VideoCall, 162] = selfviewFullscreen;
        [dvTP_VideoCall, 163] = selfviewFullscreen;
        
        [dvTP_VideoCall, 164] = selfviewMonitor;
        [dvTP_VideoCall, 165] = selfviewMonitor;
        
        [dvTP_VideoCall, 171] = (selfviewPosition == 1);
        [dvTP_VideoCall, 172] = (selfviewPosition == 2);
        [dvTP_VideoCall, 173] = (selfviewPosition == 3);
        [dvTP_VideoCall, 174] = (selfviewPosition == 4);
        [dvTP_VideoCall, 175] = (selfviewPosition == 5);
        [dvTP_VideoCall, 176] = (selfviewPosition == 6);
        [dvTP_VideoCall, 177] = (selfviewPosition == 7);
    }

    TIMELINE_EVENT [TL_COOLING]  // Projector Cooling Countdown
    {   
        integer progress;
        progress = type_cast(((TIMELINE.REPETITION * 100) / COOL_TIME));
        
        if (progress >= 0 && progress < 101)
        {
            send_level dvTP_Main, 6, progress;
        }
        else
        {
            send_command dvTP_Main, "'@PPF-Cooling'";
            TIMELINE_KILL(TL_COOLING);
        }
    }
    
    TIMELINE_EVENT [TL_WARMING]  // Projector Warming Countdown
    {   
        integer progress;
        progress = type_cast(((TIMELINE.REPETITION * 100) / WARM_TIME));
        
        if (progress >= 0 && progress < 101)
        {
            send_level dvTP_Main, 5, progress;
        }
        else
        {
            send_command dvTP_Main, "'@PPF-Warming'";
            TIMELINE_KILL(TL_WARMING);
        }
    }


//----------------------------------------//
//  DATA EVENTS                 
//----------------------------------------//

    DATA_EVENT [dvMaster]
    {
	ONLINE:
	{
	    TIMELINE_CREATE(TL_FEEDBACK, tlFeedbackSteps, length_array(tlFeedbackSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
	    IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
	}
    }

    DATA_EVENT [dvCOM1] // Video Switcher
    {
        ONLINE:
        {
            send_command dvCOM1, "'SET BAUD 115200,N,8,1'";
	    
	    send_string 0, "'[Switch]{Online}'";
        }
	
	STRING:
	{
	    send_string 0, "'[Switch]{String}:', DATA.TEXT";
	}
	
	ONERROR:
	{
	    send_string 0, "'[Switch]{Error}: [', itoa(data.number), '] ', data.text";
	}
    }

    DATA_EVENT [dvCOM2] // VTC Codec
    {
        ONLINE:
        {
            send_command dvCOM2, "'SET BAUD 38400,N,8,1'";
	    
            send_string 0, "'[Codec]{Online}'";
	    
            wait 30 initCodec();
        }

        STRING:
        {
	    send_string 0, "'[Codec]{String}:', data.text";
	    
            while (find_string(codecQueue, "$0D, $0A", 1))
            {
                parseCodecFeedback(remove_string(codecQueue, "$0D, $0A", 1));
            }
            
        }
	
	ONERROR:
	{
	    send_string 0, "'[Codec]{Error}: [', itoa(data.number), '] ', data.text";
	}
    }

    DATA_EVENT [dvCOM3] // USB Switch
    {
        ONLINE:
        {
            send_command dvCOM3, "'SET BAUD 9600,N,8,1'";
	    
	    send_string 0, "'[USB Switch]{Online}'";
        }
	
	STRING:
	{
	    send_string 0, "'[USB Switch]{String}: ', data.text";
	}
	
	ONERROR:
	{
	    send_string 0, "'[USB Switch]{Error}: [', itoa(data.number), '] ', data.text";
	}
    }

    DATA_EVENT [dvCOM4] // Display
    {
        ONLINE:
        {
            send_command dvCOM4, "'SET BAUD 9600,N,8,1'";
	    
	    send_string 0, "'[Display]{Online}'";
        }
	
	STRING:
	{
	    send_string 0, "'[Display]{String}:', DATA.TEXT";
	}
	
	ONERROR:
	{
	    send_string 0, "'[Display]{Error}: [', itoa(data.number), '] ', data.text";
	}
    }
    
    DATA_EVENT [dvBiamp] // Biamp TesiraForte
    {
        ONLINE:
        {
            send_string 0, "'[Biamp]{Online}'";
            biampConnected = 1;
	    //initBiamp();
        }

        STRING:
        {    
	    send_string 0, "'[Biamp]{String}:', DATA.TEXT";
	    while (remove_string(biampQueue, "$FF, $FD", 1))
	    {
		send_string dvBiamp, "$FF, $FC, left_string(biampQueue, 1)";
	    }
	    
            while (find_string(biampQueue, "$0D, $0A", 1))
            {
                parseBiampFeedback(remove_string(biampQueue, "$0D, $0A", 1));
            }
        }

        OFFLINE:
        {
            send_string 0, "'[Biamp]{Offline}'";

            biampConnected = 0;
            
            wait 50 IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
        }

        ONERROR:
        {
            send_string 0, "'[Biamp]{Error}: [', itoa(data.number), '] ', data.text";
            
            wait 50
            {
                if (!biampConnected)
                    IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
            }
        }
    }
    
    
DEFINE_PROGRAM