PROGRAM_NAME='Conference Room'

(* ------------------------------------------------------- *)
(*                                                         *)
(*  FILE CREATED ON: 08/22/2016  AT: 11:28:35		   *)
(*  FILE_LAST_MODIFIED_ON: 01/27/2017  AT: 15:38:10        *)
(*                                                         *)
(*  System Type : NetLinx                                  *)
(*                                                         *)
(* ------------------------------------------------------- *)
(*                                                         *)
(*  Jonathan Brenner                                       *)
(*  AVI Systems                                            *)
(*                                                         *)
(*  Client: Children's Hospital Association                *)
(*  Project: Lenexa Office - Conference Center             *)
(*                                                         *)
(* ------------------------------------------------------- *)

DEFINE_DEVICE

    dvMaster = 0:1:0;       // NX-2200

    dvBiamp = 0:3:0;        // Biamp Tesira VI
    dvMic1 = 0:4:0;         // Shure MXA310
    dvMic2 = 0:5:0;         // Shure MXA310

    dvCom1 = 5001:1:0;      // Kramer Video Switch
    dvCom2 = 5001:2:0;      // Cisco Codec
    dvCom3 = 5001:3:0;      // USB Switch
    dvCom4 = 5001:4:0;      // Sharp Display

    dvTP_Main = 10001:1:0;
    dvTP_Presentation = 10001:2:0;
    dvTP_AudioCall = 10001:3:0;
    dvTP_VideoCall = 10001:4:0;
    dvTP_WebConf = 10001:5:0;



DEFINE_CONSTANT

    // IP Addresses
    BIAMP_IP = '192.168.1.101';
    MIC1_IP = '192.168.1.102';
    MIC2_IP = '192.168.1.103';

    // Timelines
    TL_FEEDBACK = 1;
    TL_COOLING = 2;
    TL_WARMING = 3;
    TL_VOLUP = 4;
    TL_VOLDN = 5;
    TL_ACIN_VOLUP = 6;
    TL_ACIN_VOLDN = 7;
    TL_ACOUT_VOLUP = 8;
    TL_ACOUT_VOLDN = 9;
    TL_VCIN_VOLUP = 10;
    TL_VCIN_VOLDN = 11;
    TL_VCOUT_VOLUP = 12;
    TL_VCOUT_VOLDN = 13;

    // System States
    SYS_OFF = 0;
    SYS_WARM = 1;
    SYS_ON = 2;
    SYS_COOL = 3;

    // System Modes
    INACTIVE = 0;
    PRESENTATION = 21;
    AUDIO_CALL = 22;
    VIDEO_CALL = 23;
    WEB_CONF = 24;

    // Video Sources
    NOTHING = 0;
    LAPTOP = 1;
    ROOM_PC = 2;
    VIA = 3;
    VTC_CODEC = 4;

    // Display Commands
    DISPLAY_ON = 'POWR1   ';
    DISPLAY_OFF = 'POWR0   ';
    DISPLAY_HDMI = 'IAVD1   ';
    DISPLAY_RSPW = 'RSPW1   ';

    COOL_TIME = 5;
    WARM_TIME = 12;

    // Audio
    MIN_VOL = -40.0;
    MAX_VOL = 6.0;
    MIN_VOL_ALT = -20.0;
    MAX_VOL_ALT = 3.0;



DEFINE_VARIABLE

    integer biampConnected = FALSE;
    integer mic1Connected = FALSE;
    integer mic2COnnected = FALSE;

    char codecQueue[65535];
    char biampQueue[65535];
    char mic1Queue[100];
    char mic2Queue[100];

    integer systemState = SYS_OFF;
    integer systemMode = INACTIVE;

    integer videoSource = NOTHING;
    integer presentationSource = NOTHING;

    integer volumeMuted = FALSE;
    integer volumeMuted_ACIN = FALSE;
    integer volumeMuted_ACOUT = FALSE;
    integer volumeMuted_VCIN = FALSE;
    integer volumeMuted_VCOUT = FALSE;

    integer micMuted = FALSE;

    integer dialKeys[] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 25, 26 };
    char dialDigits[] = '0123456789*#.';

    integer acOffHook = FALSE;
    integer vcOffHook = FALSE;

    char acDialNumber[50];
    char vcDialNumber[50];
    char vcKeyboardText[255];

    integer vcContentSource = NOTHING;

    integer vcDoNotDisturb = FALSE;

    integer selfviewEnabled = FALSE;
    integer selfviewFullscreen = FALSE;
    char selfviewMonitor[20];
    char selfviewPosition[20];

    char incomingVideoCall_Number[50];
    char incomingVideoCall_Name[50];
    char incomingVideoCall_ID[10];

    char incomingAudioCall_Number[50];
    char incomingAudioCall_Name[50];

    integer selectedCamera = 1;
    integer cameraControls[] = { 132, 133, 134, 135, 158, 159 };
    integer cameraPresets[] = { 151, 152, 153, 154, 155, 156 };

    integer cameraPresetUpdated = FALSE;

    integer webConfSource = VIA;

    long tlFeedbackSteps[] = {100};
    long tlVolRampSteps[] = {250};
    long tlWarmingSteps[] = {1000};
    long tlCoolingSteps[] = {1000};

    char vcContacts[][255] =
    {
        '172.20.10.141',
        '172.20.10.142',
        //'172.17.11.12',
        '172.17.11.9',
        '172.17.11.95'
    }

    char vcContactsNames[][255] =
    {
        'Avery Room',
        'Salk Room',
        //'Mission Room',
        'Champions Room',
        'Conference Center'
    }

    char acRegisterCommands[][255] = {
        'SESSION set verbose false',
        'Program subscribe level 1 regProgramLevel 300',
        'Program subscribe mute 1 regProgramMute 300',
        'AudioCallIn subscribe level 1 regACINLevel 300',
        'AudioCallIn subscribe mute 1 regACINMute 300',
        'AudioCallOut subscribe level 1 regACOUTLevel 300',
        'AudioCallOut subscribe mute 1 regACOUTMute 300',
        'VideoCallIn subscribe level 1 regVCINLevel 300',
        'VideoCallIn subscribe mute 1 regVCINMute 300',
        'VideoCallOut subscribe level 1 regVCOUTLevel 300',
        'VideoCallOut subscribe mute 1 regVCOUTMute 300',
        'Privacy subscribe mute 1 regPrivacyMute 300',
        'Dialer subscribe callState regDialerCallState 300'
    };

    char vcRegisterCommands[][255] = {
        'xPreferences outputmode terminal',
        'xFeedback register event/IncomingCallIndication',
        'xFeedback register event/Message',
        'xFeedback register event/SString',
        'xFeedback register event/TString',
        'xFeedback register Status/Standby',
        'xFeedback register Status/Call',
        'xFeedback register Status/Audio/Microphones',
        'xFeedback register Status/Conference',
        'xFeedback register Status/Video/Selfview/Mode',
        'xFeedback register Status/Video/Selfview/FullscreenMode',
        'xFeedback register Status/Video/Selfview/PIPPosition',
        'xFeedback register Status/Preset',
        'xFeedback register Configuration',
        'xConfiguration Video/OSD',
        'xConfiguration Video/Selfview',
        'xConfiguration Video/DefaultPresentationSource',
        'xConfiguration Video/Input/Source',
        'xConfiguration Audio/Volume',
        'xConfiguration Conference 1 DoNotDisturb Mode',
        'xConfiguration Video Selfview',
        'xConfiguration Standby Control',
        'xStatus Conference',
        'xStatus Audio/Microphones',
        'xStatus Audio/Volume',
        'xStatus Standby',
        'xStatus Video Input',
        'xStatus Call'
    };


//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // System Startup
    //--------------------------------------------------------------------------
    //   wVideo: boolean | Turn displays on with system or not
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION systemStartup(integer wVideo)
    {
        // Startup Display
        if (wVideo)
        {
            // Start Warmup
            TIMELINE_CREATE(TL_WARMING, tlWarmingSteps, length_array(tlWarmingSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);

            send_string dvCom4, "DISPLAY_ON, $0D";

            systemState = SYS_ON;

            wait 120
                send_string dvCom4, "DISPLAY_HDMI, $0D";
        }

        // Unmute Audio
        volumeMute('Program', FALSE);
        volumeMute('Privacy', FALSE);
    }

    //--------------------------------------------------------------------------
    // System Shutdown
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION systemShutdown()
    {
        // Start Cooldown
        TIMELINE_CREATE(TL_COOLING, tlCoolingSteps, length_array(tlCoolingSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        send_command dvTP_Main, "'PAGE-Start'";
        send_command dvTP_Main, "'@PPN-Cooling;Start'";

        send_string dvCom4, "DISPLAY_RSPW, $0D";

        wait 5
            send_string dvCom4, "DISPLAY_OFF, $0D";

        sourceSwitch(NOTHING);
        contentSourceSwitch(NOTHING);

        volumeMute('Program', TRUE);
        volumeMute('Privacy', TRUE);

        acHangUp();
        vcHangUp();

        send_string dvBiamp, "'AudioCallIn set level 1 -6', $0D";
        send_string dvBiamp, "'AudioCallOut set level 1 -6', $0D";
        send_string dvBiamp, "'VideoCallIn set level 1 -6', $0D";
        send_string dvBiamp, "'VideoCallOut set level 1 -6', $0D";

        send_string dvBiamp, "'Program set level 1 -12', $0D";

        wait 20
            send_string dvCom2, "'xCommand Standby Activate', $0D";

        systemState = SYS_OFF;
    }

    //--------------------------------------------------------------------------
    // Main Source Selection
    //--------------------------------------------------------------------------
    //   src: input number (0-4)
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION sourceSwitch(integer src)
    {
        videoSource = src;

        // Unmute and Switch Video or mute if src is 0
        if (src > 0)
        {
            send_string dvCom1, "'#VMUTE 1,0', $0D";
            send_string dvCom1, "'#VID ', itoa(src), '>1', $0D";
        }
        else
        {
            send_string dvCom1, "'#VMUTE 1,1', $0D";
        }

        // Switch Audio
        send_string dvBiamp, "'SourceSelector set sourceSelection ', itoa(src), $0D";

        // Switch USB
        if (src == ROOM_PC || src == VIA)
            send_string dvCom3, "itoa(src-1), '!'";
    }

    //--------------------------------------------------------------------------
    // Content Source Selection
    //--------------------------------------------------------------------------
    //   src: input number (0-4)
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION contentSourceSwitch(integer src)
    {
        vcContentSource = src;

        // Unmute and Switch Video or mute if src is 0
        if (src > 0)
        {
            send_string dvCom1, "'#VMUTE 2,0', $0D";
            send_string dvCom1, "'#VID ', itoa(src), '>2', $0D";
        }
        else
        {
            send_string dvCom1, "'#VMUTE 2,1', $0D";
        }

        // Switch Audio
        send_string dvBiamp, "'SourceSelector set sourceSelection ', itoa(src), $0D";
    }

    //--------------------------------------------------------------------------
    // Volume Controls
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION volumeUp(char id[], integer step)
    {
        send_string dvBiamp, "id, ' increment level 1 ', itoa(step), $0D";
    }

    DEFINE_FUNCTION volumeDown(char id[], integer step)
    {
        send_string dvBiamp, "id, ' decrement level 1 ', itoa(step), $0D";
    }

    DEFINE_FUNCTION volumeMute(char id[], integer state)
    {
        if (state)
            send_string dvBiamp, "id, ' set mute 1 true', $0D";
        else
            send_string dvBiamp, "id, ' set mute 1 false', $0D";
    }

    DEFINE_FUNCTION volumeMuteToggle(char id[])
    {
        send_string dvBiamp, "id, ' toggle mute 1', $0D";
    }

    //--------------------------------------------------------------------------
    // Audio Call Controls
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION acDial()
    {
        send_string dvBiamp, "'Dialer dial 1 1 ', $22, acDialNumber, $22, $0D";
    }

    DEFINE_FUNCTION acHangUp()
    {
        send_string dvBiamp, "'Dialer onHook 1 1', $0D";
        acDialNumber = '';
        send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
    }

    DEFINE_FUNCTION acKey(integer key)
    {
        if (key < 25)
        {
            acDialNumber = "acDialNumber, dialDigits[key-9]";
            if (acOffHook)
                send_string dvBiamp, "'Dialer dtmf 1 ', dialDigits[key-9], $0D";
        }
        else if (key == 25) // Backspace
        {
            integer length;
            length = length_string(acDialNumber);

            if (length > 1)
                acDialNumber = left_string(acDialNumber, length-1);
            else
                acDialNumber = '';
        }
        else if (key == 26) // Clear
        {
            acDialNumber = '';
        }

        send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
    }

    //--------------------------------------------------------------------------
    // Video Call Controls
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION vcDial(char number[])
    {
        send_string dvCom2, "'xCommand Dial Number:', $22, number, $22, $0D";
    }

    DEFINE_FUNCTION vcHangUp()
    {
        vcDialNumber = '';
        send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
        send_string dvCom2, "'xCommand Call DisconnectAll', $0D";
    }

    DEFINE_FUNCTION vcKey(integer key)
    {
        if (key < 25)
        {
            vcDialNumber = "vcDialNumber, dialDigits[key-9]";

            if (vcOffHook)
                send_string dvCom2, "'xCommand DTMFSend DTMFString:', dialDigits[key-9], $0D";
        }
        else if (key == 25)
        {
            integer length;
            length = length_string(vcDialNumber);

            if (length > 1)
                vcDialNumber = left_string(vcDialNumber, length-1);
            else
                vcDialNumber = '';
        }
        else if (key == 26)
        {
            vcDialNumber = '';
        }

        send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
    }

    //--------------------------------------------------------------------------
    // Initialize Codec
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION initCodec()
    {
        integer i;

        for (i = 1; i <= 28; i++)
        {
            send_string dvCom2, "vcRegisterCommands[i], $0D";
        }
    }

    //--------------------------------------------------------------------------
    // Parse Codec Feeback
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION parseCodecFeedback(char str[])
    {
        char prefix[4];

        // Strip carraige return and line feed
        str = left_string(str, length_string(str)-2);

        prefix = remove_string(str, ' ', 1);

        if (prefix == '*s ')
        {
            if (find_string(str, 'Call ', 1))
            {
                char status[20];

                if (remove_string(str, 'Status: ', 1))
                {
                    status = remove_string(str, ' ', 1);

                    if (status == 'Connected ')
                    {
                        vcOffHook = 1;
                        send_command dvTP_Main, '@PPF-IncomingVideoCall';
                    }
                    else if (status == 'Disconnected ' || status == 'Idle ')
                    {
                        vcOffHook = 0;
                        vcDialNumber = '';
                        send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
                        send_command dvTP_Main, '@PPF-IncomingVideoCall';
                    }
                }
            }
            else
            {
                char message[255];
                message = remove_string(str, ': ', 1);

                select
                {
                    active (message == 'Conference DoNotDisturb: '):
                        vcDoNotDisturb = (str == 'On');
                    active (message == 'Video Selfview Mode: '):
                        selfviewEnabled = (str == 'On');
                    active (message == 'Video Selfview FullscreenMode: '):
                        selfviewFullscreen = (str == 'On');
                    active (message == 'Video Selfview PIPPosition: '):
                        selfviewPosition = str;
                    active (message == 'Video Selfview OnMonitorRole: '):
                        selfviewMonitor = str;
                }
            }
        }
        else if (prefix == '*e ')
        {
            if (str == 'CameraPresetListUpdated')
            {
               cameraPresetUpdated = 1;
               wait 30
                    cameraPresetUpdated = 0;
            }
            else if (remove_string(str, 'IncomingCallIndication ', 1))
            {
                send_command dvTP_Main, '@PPN-IncomingVideoCall';

                remove_string(str, 'RemoteURI: ', 1);
                incomingVideoCall_Number = remove_string(str, ' ', 1);

                remove_string(str, 'DisplayNameValue: ', 1);
                incomingVideoCall_Name = remove_string(str, ' ', 1);

                remove_string(str, 'CallId: ', 1);
                incomingVideoCall_ID = remove_string(str, ' ', 1);

                send_command dvTP_VideoCall, "'^TXT-2,0,', incomingVideoCall_Name";
            }
        }
    }

    //--------------------------------------------------------------------------
    // Parse Level (Normal Range)
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION float parseLevel(char str[])
    {
        float level;
        level = atof(str);
        return ((level - MIN_VOL) * 100) / (MAX_VOL - MIN_VOL);
    }

    //--------------------------------------------------------------------------
    // Parse Level (Alternate Range)
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION float parseLevel_Alt(char str[])
    {
        float level;
        level = atof(str);
        return ((level - MIN_VOL_ALT) * 100) / (MAX_VOL_ALT - MIN_VOL_ALT);
    }

    //--------------------------------------------------------------------------
    // Initialize Biamp
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION initBiamp()
    {
        integer i;

        for (i = 1; i <= 13; i++)
        {
            send_string dvBiamp, "acRegisterCommands[i], $0D";
        }
    }

    //--------------------------------------------------------------------------
    // Parse Biamp Feeback
    //--------------------------------------------------------------------------
    DEFINE_FUNCTION parseBiampFeedback(char str[])
    {
        // Strip carraige return and line feed
        str = left_string(str, length_string(str)-2);

        if (find_string(str, 'Welcome', 1))
        {
            initBiamp();
        }
        else if (remove_string(str, '! "', 1))
        {
            char tag[50];
            tag = remove_string(str, '" ', 1);
            tag = left_string(tag, length_string(tag)-2);

            select
            {
                active (tag == 'regProgramLevel'): send_level dvTP_Main, 1, type_cast(parseLevel(str));
                active (tag == 'regProgramMute'):  volumeMuted = (str == 'true');

                active (tag == 'regACINLevel'): send_level dvTP_Main, 2, type_cast(parseLevel_Alt(str));
                active (tag == 'regACINMute'):  volumeMuted_ACIN = (str == 'true');

                active (tag == 'regACOUTLevel'): send_level dvTP_Main, 3, type_cast(parseLevel_Alt(str));
                active (tag == 'regACOUTMute'):  volumeMuted_ACOUT = (str == 'true');

                active (tag == 'regVCINLevel'): send_level dvTP_Main, 4, type_cast(parseLevel_Alt(str));
                active (tag == 'regVCINMute'): volumeMuted_VCIN = (str == 'true');

                active (tag == 'regVCOUTLevel'): send_level dvTP_Main, 5, type_cast(parseLevel_Alt(str));
                active (tag == 'regVCOUTMute'): volumeMuted_VCOUT = (str == 'true');

                active (tag == 'regPrivacyMute'):
                {
                    micMuted = (str == 'true');

                    if (micMuted)
                    {
                        send_string dvMic1, "'< SET DEVICE_AUDIO_MUTE ON >', $0D";
                        send_string dvMic2, "'< SET DEVICE_AUDIO_MUTE ON >', $0D";
                    }
                    else
                    {
                        send_string dvMic1, "'< SET DEVICE_AUDIO_MUTE OFF >', $0D";
                        send_string dvMic2, "'< SET DEVICE_AUDIO_MUTE OFF >', $0D";
                    }
                }

                active (tag == 'regDialerCallState'):
                {
                    if (remove_string(str, '[[[', 1))
                    {
                        integer callState, lineID, callID;
                        char callerID[100];

                        callState = atoi(remove_string(str, ' ', 1));
                        lineID = atoi(remove_string(str, ' ', 1));
                        callID = atoi(remove_string(str, ' ', 1));

                        if (lineID == 0 && callID == 0)
                        {
                            if (callState == 13)
                            {
                                send_command dvTP_Main, '@PPF-IncomingAudioCall';
                                acOffHook = TRUE;
                            }
                            else
                            {
                                acOffHook = FALSE;

                                if (callState == 8)
                                {
                                    remove_string(str, ' "', 1);
                                    callerID = remove_string(str, '" ', 1);
                                    callerID = left_string(callerID, length_string(callerID)-1);
                                    remove_string(callerID, "$5C, $22, $5C, $22", 1);

                                    incomingAudioCall_Number = remove_string(callerID, "$5C, $22, $5C, $22", 1);
                                    incomingAudioCall_Number = left_string(incomingAudioCall_Number, length_string(incomingAudioCall_Number)-4);

                                    incomingAudioCall_Name = remove_string(callerID, "$5C", 1);
                                    incomingAudioCall_Name = left_string(incomingAudioCall_Name, length_string(incomingAudioCall_Name)-1);

                                    send_command dvTP_Main, '@PPN-IncomingAudioCall';
                                    send_command dvTP_AudioCall, "'^TXT2,0,', incomingAudioCall_Number";
                                }
                            }
                        }
                    }
                }
            }
        }
    }


DEFINE_START

    CREATE_BUFFER dvCom2, codecQueue;
    CREATE_BUFFER dvBiamp, biampQueue;
    CREATE_BUFFER dvMic1, mic1Queue;
    CREATE_BUFFER dvMic2, mic2Queue;


DEFINE_EVENT

//------------------------------------------------------------------------------
//  Logo
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 10]
    {
        HOLD[100]:
        {
            // TODO: Setup Page
        }
    }


//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 9]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Shutdown'";
        }
    }

    BUTTON_EVENT[dvTP_Main, 11]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPF-Shutdown'";
        }
    }

    BUTTON_EVENT[dvTP_Main, 12]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPF-Shutdown'";
            systemShutdown();
        }
    }


//------------------------------------------------------------------------------
//  Display Manual On/Off
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 13]
    {
        PUSH:
        {
            send_string dvCom4, "DISPLAY_ON, $0D";
            wait 120
                send_string dvCom4, "DISPLAY_HDMI, $0D";
        }
    }

    BUTTON_EVENT[dvTP_Main, 14]
    {
        PUSH:
        {
            send_string dvCom4, "DISPLAY_RSPW, $0D";
            wait 20
                send_string dvCom4, "DISPLAY_OFF, $0D";
        }
    }


//------------------------------------------------------------------------------
//  Startup Modes
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 20] // Settings
    {
        PUSH:
        {
            send_command dvTP_Main, "'PAGE-Settings'";
            systemMode = 20;
        }
    }

    BUTTON_EVENT[dvTP_Main, PRESENTATION]   // Presentation Mode
    BUTTON_EVENT[dvTP_Main, AUDIO_CALL]     // Audio Call Mode
    BUTTON_EVENT[dvTP_Main, VIDEO_CALL]     // Video Call Mode
    BUTTON_EVENT[dvTP_Main, WEB_CONF]       // Web Conference Mode
    {
        PUSH:
        {
            integer usingVideo;
            usingVideo = 1;

            switch (button.input.channel)
            {
                case PRESENTATION:
                {
                    if (presentationSource <> 0)
                        sourceSwitch(presentationSource);
                    else
                        sourceSwitch(ROOM_PC);

                    send_command dvTP_Main, "'PAGE-Presentation'";
                }
                case AUDIO_CALL:
                {
                    usingVideo = 0;
                    send_command dvTP_Main, "'PAGE-Audio Call'";
                }
                case VIDEO_CALL:
                {
                    sourceSwitch(VTC_CODEC);
                    send_command dvTP_Main, "'PAGE-Video Call'";
                    send_command dvTP_Main, "'@PPN-Dial;Video Call'";
                    send_string dvCom2, "'xCommand Standby Deactivate', $0D";
                }
                case WEB_CONF:
                {
                    sourceSwitch(webConfSource);
                    send_command dvTP_Main, "'PAGE-Web Call'";
                    send_string dvCom2, "'xCommand Standby Deactivate', $0D";
                }
            }

            systemMode = button.input.channel;
            selectedCamera = 1;

            if (systemState == SYS_OFF)
                systemStartup(usingVideo);
        }
    }


//------------------------------------------------------------------------------
//  Volume Controls
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 30]
    {
        PUSH:
        {
            volumeUp('Program', 3);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VOLUP))
                TIMELINE_KILL(TL_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 31]
    {
        PUSH:
        {
            volumeDown('Program', 3);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VOLDN))
                TIMELINE_KILL(TL_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 32]
    {
        PUSH:
        {
            volumeMuteToggle('Program');
        }
    }

    BUTTON_EVENT[dvTP_Main, 35]
    {
        PUSH:
        {
            volumeMuteToggle('Privacy');
        }
    }


//------------------------------------------------------------------------------
//  Audio Call In Volume Controls
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 40]
    {
        PUSH:
        {
            volumeUp('AudioCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACIN_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACIN_VOLUP))
                TIMELINE_KILL(TL_ACIN_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 41]
    {
        PUSH:
        {
            volumeDown('AudioCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACIN_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACIN_VOLDN))
                TIMELINE_KILL(TL_ACIN_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 42]
    {
        PUSH:
        {
            volumeMuteToggle('AudioCallIn');
        }
    }


//------------------------------------------------------------------------------
//  Default Audio Call Volume Settings
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 45]
    {
        PUSH:
        {
            send_string dvBiamp, "'AudioCallIn set level 1 -6', $0D";
            send_string dvBiamp, "'AudioCallOut set level 1 -6', $0D";
        }
    }


//------------------------------------------------------------------------------
//  Audio Call Out Volume Controls
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 50]
    {
        PUSH:
        {
            volumeUp('AudioCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACOUT_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACOUT_VOLUP))
                TIMELINE_KILL(TL_ACOUT_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 51]
    {
        PUSH:
        {
            volumeDown('AudioCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACOUT_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACOUT_VOLDN))
                TIMELINE_KILL(TL_ACOUT_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 52]
    {
        PUSH:
        {
            volumeMuteToggle('AudioCallOut');
        }
    }


//------------------------------------------------------------------------------
//  Video Call In Volume Controls
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 60]
    {
        PUSH:
        {
            volumeUp('VideoCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCIN_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCIN_VOLUP))
                TIMELINE_KILL(TL_VCIN_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 61]
    {
        PUSH:
        {
            volumeDown('VideoCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCIN_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCIN_VOLDN))
                TIMELINE_KILL(TL_VCIN_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 62]
    {
        PUSH:
        {
            volumeMuteToggle('VideoCallIn');
        }
    }


//------------------------------------------------------------------------------
//  Default Video Call Volume Settings
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 65]
    {
        PUSH:
        {
            send_string dvBiamp, "'VideoCallIn set level 1 -6', $0D";
            send_string dvBiamp, "'VideoCallOut set level 1 -6', $0D";
        }
    }


//------------------------------------------------------------------------------
//  Video Call Out Volume Controls
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Main, 70]
    {
        PUSH:
        {
            volumeUp('VideoCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCOUT_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCOUT_VOLUP))
                TIMELINE_KILL(TL_VCOUT_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 71]
    {
        PUSH:
        {
            volumeDown('VideoCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCOUT_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCOUT_VOLDN))
                TIMELINE_KILL(TL_VCOUT_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 72]
    {
        PUSH:
        {
            volumeMuteToggle('VideoCallOut');
        }
    }


//------------------------------------------------------------------------------
//  Source Selection
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_Presentation, 0]
    {
        PUSH:
        {
            sourceSwitch(button.input.channel);
            presentationSource = button.input.channel;
        }
    }


//------------------------------------------------------------------------------
//  Audio Call Controls
//------------------------------------------------------------------------------

    // Dial Preset [Webex]
    BUTTON_EVENT[dvTP_AudioCall, 4]
    {
        PUSH:
        {
            acDialNumber = '818552448681';
            acDial();
        }
    }

    // Dial Button
    BUTTON_EVENT[dvTP_AudioCall, 5]
    {
        PUSH:
        {
            acDial();
        }
    }

    // Hang Up Button
    BUTTON_EVENT[dvTP_AudioCall, 6]
    {
        PUSH:
        {
            acHangUp();
        }
    }

    // Answer Incoming Call
    BUTTON_EVENT[dvTP_AudioCall, 7]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingAudioCall';
            send_string dvBiamp, "'Dialer answer 1 1', $0D";
            send_command dvTP_Main, 'PAGE-AudioCall';
            systemMode = AUDIO_CALL;
        }
    }

    // Reject Incoming Call
    BUTTON_EVENT[dvTP_AudioCall, 8]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingAudioCall';
            send_string dvBiamp, "'Dialer answer 1 1', $0D";
            acHangUp();
        }
    }

    // Keypad Keys
    BUTTON_EVENT[dvTP_AudioCall, dialKeys]
    {
        PUSH:
        {
            acKey(button.input.channel);
        }
    }


//------------------------------------------------------------------------------
//  Video Call Controls
//------------------------------------------------------------------------------

    // Text Area Press (to bring up keyboard)
    BUTTON_EVENT[dvTP_VideoCall, 1]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@AKB-', vcDialNumber, ';Enter Address'";
        }
    }

    // Flip to Content Page
    BUTTON_EVENT[dvTP_VideoCall, 2]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Content;Video Call'";
        }
    }

    // Flip to Dial Page
    BUTTON_EVENT[dvTP_VideoCall, 3]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Dial;Video Call'";
        }
    }

    // Popup speed dial entries
    BUTTON_EVENT[dvTP_VideoCall, 4]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-VTCSpeedDial;Video Call'";
        }
    }

    // Dial Button
    BUTTON_EVENT[dvTP_VideoCall, 5]
    {
        PUSH:
        {
            vcDial(vcDialNumber);
        }
    }

    // Hang Up Button
    BUTTON_EVENT[dvTP_VideoCall, 6]
    {
        PUSH:
        {
            vcHangUp();
        }
    }

    // Answer Incoming Call
    BUTTON_EVENT[dvTP_VideoCall, 7]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingVideoCall';
            send_string dvCom2, "'xCommand Call Accept', $0D";
            send_command dvTP_Main, 'PAGE-VideoCall';
            systemMode = VIDEO_CALL;
        }
    }

    // Reject Incoming Call
    BUTTON_EVENT[dvTP_VideoCall, 8]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingVideoCall';
            send_string dvCom2, "'xCommand Call Reject', $0D";
        }
    }

    // Keypad Keys
    BUTTON_EVENT[dvTP_VideoCall, dialKeys]
    {
        PUSH:
        {
            vcKey(button.input.channel);
        }
    }

    // Open 'Speed-Dial' selection
    BUTTON_EVENT[dvTP_VideoCall, 30]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-VTCSpeedDial';
        }
    }

    // Call 'Speed Dial' entries
    BUTTON_EVENT[dvTP_VideoCall, 31]
    BUTTON_EVENT[dvTP_VideoCall, 32]
    BUTTON_EVENT[dvTP_VideoCall, 33]
    BUTTON_EVENT[dvTP_VideoCall, 34]
    BUTTON_EVENT[dvTP_VideoCall, 35]
    {
        PUSH:
        {
            integer index;
            index = button.input.channel-30;

            send_command dvTP_Main, '@PPF-VTCSpeedDial';
            vcDial(vcContacts[index]);
            vcDialNumber = vcContacts[index];
            send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
        }
    }

    // Menu Navigation
    BUTTON_EVENT[dvTP_VideoCall, 45]
    BUTTON_EVENT[dvTP_VideoCall, 46]
    BUTTON_EVENT[dvTP_VideoCall, 47]
    BUTTON_EVENT[dvTP_VideoCall, 48]
    BUTTON_EVENT[dvTP_VideoCall, 49]
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 45:{ send_string dvCom2, "'xCommand Key Press Key: Up', $0D"; }
                case 46:{ send_string dvCom2, "'xCommand Key Press Key: Down', $0D"; }
                case 47:{ send_string dvCom2, "'xCommand Key Press Key: Left', $0D"; }
                case 48:{ send_string dvCom2, "'xCommand Key Press Key: Right', $0D"; }
                case 49:{ send_string dvCom2, "'xCommand Key Press Key: Ok', $0D"; }
            }
        }

        RELEASE:
        {
            switch(button.input.channel)
            {
                case 45:{ send_string dvCom2, "'xCommand Key Release Key: Up', $0D"; }
                case 46:{ send_string dvCom2, "'xCommand Key Release Key: Down', $0D"; }
                case 47:{ send_string dvCom2, "'xCommand Key Release Key: Left', $0D"; }
                case 48:{ send_string dvCom2, "'xCommand Key Release Key: Right', $0D"; }
                case 49:{ send_string dvCom2, "'xCommand Key Release Key: Ok', $0D"; }
            }
        }
    }

    // Home Button
    BUTTON_EVENT[dvTP_VideoCall, 50]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: Home', $0D"; }
    }
    // Back Button
    BUTTON_EVENT[dvTP_VideoCall, 81]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: C', $0D"; }
    }
    // Layout Button
    BUTTON_EVENT[dvTP_VideoCall, 99]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: Layout', $0D"; }
    }
    // Directory Button
    BUTTON_EVENT[dvTP_VideoCall, 105]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: PhoneBook', $0D"; }
    }

    // Content Source Selection
    BUTTON_EVENT[dvTP_VideoCall, 120]
    BUTTON_EVENT[dvTP_VideoCall, 121]
    BUTTON_EVENT[dvTP_VideoCall, 122]
    BUTTON_EVENT[dvTP_VideoCall, 123]
    {
        PUSH:
        {
            contentSourceSwitch(button.input.channel-120);

            if (button.input.channel-120)
                send_string dvCom2, "'xCommand Presentation Start PresentationSource: 3', $0D";
            else
                send_string dvCom2, "'xCommand Presentation Stop', $0D";
        }
    }

    // Self View On/Off
    BUTTON_EVENT[dvTP_VideoCall, 160]
    BUTTON_EVENT[dvTP_VideoCall, 161]
    {
        PUSH:
        {
            if (button.input.channel - 160)
                send_string dvCom2, "'xConfiguration Video Selfview: On', $0D";
            else
                send_string dvCom2, "'xConfiguration Video Selfview: Off', $0D";
        }
    }

    // Self View Fullscreen On/Off
    BUTTON_EVENT[dvTP_VideoCall, 162]
    BUTTON_EVENT[dvTP_VideoCall, 163]
    {
        PUSH:
        {
            if (button.input.channel - 162)
                send_string dvCom2, "'xCommand Video Selfview Set FullscreenMode: On', $0D";
            else
                send_string dvCom2, "'xCommand Video Selfview Set FullscreenMode: Off', $0D";
        }
    }

    // Self View Show on Left/Right
    BUTTON_EVENT[dvTP_VideoCall, 164]
    BUTTON_EVENT[dvTP_VideoCall, 165]
    {
        PUSH:
        {
            if (button.input.channel - 164)
                send_string dvCom2, "'xCommand Video Selfview Set OnMonitorRole: First', $0D";
            else
                send_string dvCom2, "'xCommand Video Selfview Set OnMonitorRole: Second', $0D";
        }
    }

    // Self View PIP position
    BUTTON_EVENT[dvTP_VideoCall, 171]   // UpperLeft
    BUTTON_EVENT[dvTP_VideoCall, 172]   // UpperCenter
    BUTTON_EVENT[dvTP_VideoCall, 173]   // UpperRight
    BUTTON_EVENT[dvTP_VideoCall, 174]   // CenterLeft
    BUTTON_EVENT[dvTP_VideoCall, 175]   // CenterRight
    BUTTON_EVENT[dvTP_VideoCall, 176]   // LowerLeft
    BUTTON_EVENT[dvTP_VideoCall, 177]   // LowerRight
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 171:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: UpperLeft', $0D"; }
                case 172:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: UpperCenter', $0D"; }
                case 173:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: UpperRight', $0D"; }
                case 174:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: CenterLeft', $0D"; }
                case 175:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: CenterRight', $0D"; }
                case 176:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: LowerLeft', $0D"; }
                case 177:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: LowerRight', $0D"; }
            }
        }
    }


//------------------------------------------------------------------------------
//  Camera Controls
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_VideoCall, 141]
    BUTTON_EVENT[dvTP_VideoCall, 142]
    BUTTON_EVENT[dvTP_VideoCall, 143]
    {
        PUSH:
        {
            selectedCamera = (button.input.channel - 140);
        }
    }

    BUTTON_EVENT[dvTP_VideoCall, cameraControls]
    {
        PUSH:
        {
            if (selectedCamera == 3)
            {
                switch(button.input.channel)
                {
                    case 132:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Up', $0D"; }
                    case 133:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Down', $0D"; }
                    case 134:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Left', $0D"; }
                    case 135:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Right', $0D"; }
                    case 158:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: ZoomOut', $0D"; }
                    case 159:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: ZoomIn', $0D"; }
                }
            }
            else
            {
                switch(button.input.channel)
                {
                    case 132:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Tilt: Up', $0D"; }
                    case 133:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Tilt: Down', $0D"; }
                    case 134:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Pan: Left', $0D"; }
                    case 135:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Pan: Right', $0D"; }
                    case 158:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Zoom: Out', $0D"; }
                    case 159:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Zoom: In', $0D"; }
                }
            }
        }

        RELEASE:
        {
            if (selectedCamera == 3)
            {
                send_string dvCom2, "'xCommand FarEndControl Camera Stop', $0D";
            }
            else
            {
                switch(button.input.channel)
                {
                    case 132:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Tilt: Stop', $0D"; }
                    case 133:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Tilt: Stop', $0D"; }
                    case 134:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Pan: Stop', $0D"; }
                    case 135:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Pan: Stop', $0D"; }
                    case 158:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Zoom: Stop', $0D"; }
                    case 159:{ send_string dvCom2, "'xCommand Camera Ramp CameraId: ', itoa(selectedCamera), ' Zoom: Stop', $0D"; }
                }
            }
        }
    }

    BUTTON_EVENT[dvTP_VideoCall, cameraPresets]
    {
        RELEASE:
        {
            if (selectedCamera == 3)
                send_string dvCom2, "'xCommand FarEndControl Preset Activate PresetId: ', itoa(button.input.channel-150), $0D";
            else
                send_string dvCom2, "'xCommand Preset Activate PresetId: ', itoa(button.input.channel-150), $0D";
        }

        HOLD[30]:
        {
            send_string dvCom2, "'xCommand Preset Store PresetId: ', itoa(button.input.channel-150), ' Type: Camera', $0D";
        }
    }


//------------------------------------------------------------------------------
//  Web Conference
//------------------------------------------------------------------------------

    BUTTON_EVENT[dvTP_WebConf, 122]     // Use Room PC
    BUTTON_EVENT[dvTP_WebConf, 123]     // Use VIA
    {
        PUSH:
        {
            sourceSwitch(button.input.channel-120);
            webConfSource = button.input.channel-120;
        }
    }


//------------------------------------------------------------------------------
//  Timelines
//------------------------------------------------------------------------------

    TIMELINE_EVENT[TL_FEEDBACK]
    {
        // Main
        [dvTP_Main, PRESENTATION] = (systemMode == PRESENTATION);
        [dvTP_Main, AUDIO_CALL] = (systemMode == AUDIO_CALL);
        [dvTP_Main, VIDEO_CALL] = (systemMode == VIDEO_CALL);
        [dvTP_Main, WEB_CONF] = (systemMode == WEB_CONF);

        [dvTP_Main, 20] = (systemMode == 20);

        [dvTP_Main, 32] = volumeMuted;
        [dvTP_Main, 35] = micMuted;

        [dvTP_Main, 42] = volumeMuted_ACIN;
        [dvTP_Main, 52] = volumeMuted_ACOUT;
        [dvTP_Main, 62] = volumeMuted_VCIN;
        [dvTP_Main, 72] = volumeMuted_VCOUT;

        // Presentation
        [dvTP_Presentation, ROOM_PC] = (videoSource == ROOM_PC);
        [dvTP_Presentation, LAPTOP] = (videoSource == LAPTOP);
        [dvTP_Presentation, VIA] = (videoSource == VIA);

        // Video Call
        [dvTP_VideoCall, 120] = (vcContentSource == NOTHING);
        [dvTP_VideoCall, 121] = (vcContentSource == LAPTOP);
        [dvTP_VideoCall, 122] = (vcContentSource == ROOM_PC);
        [dvTP_VideoCall, 123] = (vcContentSource == VIA);

        [dvTP_VideoCall, 141] = (selectedCamera == 1);
        [dvTP_VideoCall, 142] = (selectedCamera == 2);
        [dvTP_VideoCall, 143] = (selectedCamera == 3);

        [dvTP_VideoCall, 160] = !selfviewEnabled;
        [dvTP_VideoCall, 161] = selfviewEnabled;

        [dvTP_VideoCall, 162] = !selfviewFullscreen;
        [dvTP_VideoCall, 163] = selfviewFullscreen;

        [dvTP_VideoCall, 164] = !selfviewMonitor;
        [dvTP_VideoCall, 165] = selfviewMonitor;

        [dvTP_VideoCall, 171] = (selfviewPosition == 'UpperLeft');
        [dvTP_VideoCall, 172] = (selfviewPosition == 'UpperCenter');
        [dvTP_VideoCall, 173] = (selfviewPosition == 'UpperRight');
        [dvTP_VideoCall, 174] = (selfviewPosition == 'CenterLeft');
        [dvTP_VideoCall, 175] = (selfviewPosition == 'CenterRight');
        [dvTP_VideoCall, 176] = (selfviewPosition == 'LowerLeft');
        [dvTP_VideoCall, 177] = (selfviewPosition == 'LowerRight');

        // Web Conference
        [dvTP_WebConf, 122] = (webConfSource == ROOM_PC);
        [dvTP_WebConf, 123] = (webConfSource == VIA);
    }

    TIMELINE_EVENT [TL_COOLING]  // Projector Cooling Countdown
    {
        integer progress;
        progress = type_cast(((TIMELINE.REPETITION * 100) / COOL_TIME));

        if (progress >= 0 && progress < 101)
        {
            systemState = SYS_COOL;
            send_level dvTP_Main, 6, progress;
        }
        else
        {
            send_command dvTP_Main, "'@PPF-Cooling'";
            systemState = SYS_OFF;
            TIMELINE_KILL(TL_COOLING);
        }
    }

    TIMELINE_EVENT [TL_WARMING]  // Projector Warming Countdown
    {
        integer progress;
        progress = type_cast(((TIMELINE.REPETITION * 100) / WARM_TIME));

        if (progress >= 0 && progress < 101)
        {
            send_command dvTP_Main, "'@PPN-Warming'";
            systemState = SYS_WARM;
            send_level dvTP_Main, 7, progress;
        }
        else
        {
            send_command dvTP_Main, "'@PPK-Warming'";
            systemState = SYS_ON;
            TIMELINE_KILL(TL_WARMING);
        }
    }

    TIMELINE_EVENT [TL_VOLUP]
    {
        volumeUp('Program', 3);
    }

    TIMELINE_EVENT [TL_VOLDN]
    {
        volumeDown('Program', 3);
    }

    TIMELINE_EVENT [TL_ACIN_VOLUP]
    {
        volumeUp('AudioCallIn', 1);
    }

    TIMELINE_EVENT [TL_ACIN_VOLDN]
    {
        volumeDown('AudioCallIn', 1);
    }

    TIMELINE_EVENT [TL_ACOUT_VOLUP]
    {
        volumeUp('AudioCallOut', 1);
    }

    TIMELINE_EVENT [TL_ACOUT_VOLDN]
    {
        volumeDown('AudioCallOut', 1);
    }

    TIMELINE_EVENT [TL_VCIN_VOLUP]
    {
        volumeUp('VideoCallIn', 1);
    }

    TIMELINE_EVENT [TL_VCIN_VOLDN]
    {
        volumeDown('VideoCallIn', 1);
    }

    TIMELINE_EVENT [TL_VCOUT_VOLUP]
    {
        volumeUp('VideoCallOut', 1);
    }

    TIMELINE_EVENT [TL_VCOUT_VOLDN]
    {
        volumeDown('VideoCallOut', 1);
    }


//------------------------------------------------------------------------------
//  DATA EVENTS
//------------------------------------------------------------------------------

    DATA_EVENT [dvMaster]
    {
        ONLINE:
        {
            TIMELINE_CREATE(TL_FEEDBACK, tlFeedbackSteps, length_array(tlFeedbackSteps), TIMELINE_RELATIVE, TIMELINE_RELATIVE);
            IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
            IP_CLIENT_OPEN(dvMic1.port, MIC1_IP, 2202, IP_TCP);
            IP_CLIENT_OPEN(dvMic2.port, MIC2_IP, 2202, IP_TCP);
        }
    }

    DATA_EVENT [dvTP_Main]
    {
        ONLINE:
        {
            integer i;

            send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
            send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";

            send_string dvBiamp, "'Program get level 1', $0D";
            send_string dvBiamp, "'Program get mute 1', $0D";
            send_string dvBiamp, "'AudioCallIn get level 1', $0D";
            send_string dvBiamp, "'AudioCallIn get mute 1', $0D";
            send_string dvBiamp, "'AudioCallOut get level 1', $0D";
            send_string dvBiamp, "'AudioCallOut get mute 1', $0D";
            send_string dvBiamp, "'VideoCallIn get level 1', $0D";
            send_string dvBiamp, "'VideoCallIn get mute 1', $0D";
            send_string dvBiamp, "'VideoCallOut get level 1', $0D";
            send_string dvBiamp, "'VideoCallOut get mute 1', $0D";
            send_string dvBiamp, "'Privacy get mute 1', $0D";

            for (i = 1; i <= 4; i++)
                send_command dvTP_VideoCall, "'^TXT-1', itoa(i),',0,', vcContactsNames[i]";
        }

        STRING:
        {
            if (remove_string(data.text, 'KEYB-', 1))
            {
                send_string 0, "'[Keyboard]{String}:', data.text";

                if (!find_string(data.text, 'ABORT', 1) && !find_string(data.text, 'SWAP', 1))
                {
                    vcDialNumber = data.text;
                    send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
                }
            }
        }
    }

    DATA_EVENT [dvCom1] // Video Switcher
    {
        ONLINE:
        {
            send_command dvCom1, "'SET BAUD 115200,N,8,1'";

            send_string 0, "'[Switch]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Switch]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Switch]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom2] // VTC Codec
    {
        ONLINE:
        {
            send_command dvCom2, "'SET BAUD 38400,N,8,1'";

            send_string 0, "'[Codec]{Online}'";

            wait 30
                initCodec();
        }

        STRING:
        {
            send_string 0, "'[Codec]{String}:', data.text";

            while (find_string(codecQueue, "$0D, $0A", 1))
            {
                parseCodecFeedback(remove_string(codecQueue, "$0D, $0A", 1));
            }
        }

        ONERROR:
        {
            send_string 0, "'[Codec]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom3] // USB Switch
    {
        ONLINE:
        {
            send_command dvCom3, "'SET BAUD 9600,N,8,1'";

            send_string 0, "'[USB Switch]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[USB Switch]{String}: ', data.text";
        }

        ONERROR:
        {
            send_string 0, "'[USB Switch]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom4] // Display
    {
        ONLINE:
        {
            send_command dvCom4, "'SET BAUD 9600,N,8,1'";

            send_string 0, "'[Display]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Display]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Display]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvBiamp] // Biamp TesiraForte
    {
        ONLINE:
        {
            send_string 0, "'[Biamp]{Online}'";
            biampConnected = 1;
            initBiamp();
        }

        STRING:
        {
            send_string 0, "'[Biamp]{String}:', DATA.TEXT";
            while (remove_string(biampQueue, "$FF, $FD", 1))
            {
                send_string dvBiamp, "$FF, $FC, left_string(biampQueue, 1)";
            }

            while (find_string(biampQueue, "$0D, $0A", 1))
            {
                parseBiampFeedback(remove_string(biampQueue, "$0D, $0A", 1));
            }
        }

        OFFLINE:
        {
            send_string 0, "'[Biamp]{Offline}'";

            biampConnected = 0;

            wait 50
                IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
        }

        ONERROR:
        {
            send_string 0, "'[Biamp]{Error}: [', itoa(data.number), '] ', data.text";

            wait 50
            {
                if (!biampConnected)
                    IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
            }
        }
    }

    DATA_EVENT [dvMic1] // Shure MXA310
    {
        ONLINE:
        {
            send_string 0, "'[Mic1]{Online}'";
            mic1Connected = TRUE;
        }

        STRING:
        {
            char temp[50];

            send_string 0, "'[Mic1]{String}:', DATA.TEXT";

            while (find_string(mic1Queue, '>', 1))
            {
                temp = remove_string(mic1Queue, '>', 1);

                if (find_string(temp, '< REP DEVICE_AUDIO_MUTE ON >', 1))
                    volumeMute('Privacy', TRUE);
                else if (find_string(temp, '< REP DEVICE_AUDIO_MUTE OFF >', 1))
                    volumeMute('Privacy', FALSE);
            }
        }

        OFFLINE:
        {
            send_string 0, "'[Mic1]{Offline}'";

            mic1Connected = FALSE;

            wait 50
                IP_CLIENT_OPEN(dvMic1.port, MIC1_IP, 2202, IP_TCP);
        }

        ONERROR:
        {
            send_string 0, "'[Mic1]{Error}: [', itoa(data.number), '] ', data.text";

            wait 50
            {
                if (!mic1Connected)
                    IP_CLIENT_OPEN(dvMic1.port, MIC1_IP, 2202, IP_TCP);
            }
        }
    }

    DATA_EVENT [dvMic2] // Shure MXA310
    {
        ONLINE:
        {
            send_string 0, "'[Mic2]{Online}'";
            mic2Connected = TRUE;
        }

        STRING:
        {
            char temp[50];

            send_string 0, "'[Mic2]{String}:', DATA.TEXT";

            while (find_string(mic2Queue, '>', 1))
            {
                temp = remove_string(mic2Queue, '>', 1);

                if (find_string(temp, '< REP DEVICE_AUDIO_MUTE ON >', 1))
                    volumeMute('Privacy', TRUE);
                else if (find_string(temp, '< REP DEVICE_AUDIO_MUTE OFF >', 1))
                    volumeMute('Privacy', FALSE);
            }
        }

        OFFLINE:
        {
            send_string 0, "'[Mic2]{Offline}'";

            mic2Connected = FALSE;

            wait 50
                IP_CLIENT_OPEN(dvMic2.port, MIC2_IP, 2202, IP_TCP);
        }

        ONERROR:
        {
            send_string 0, "'[Mic2]{Error}: [', itoa(data.number), '] ', data.text";

            wait 50
            {
                if (!mic2Connected)
                    IP_CLIENT_OPEN(dvMic2.port, MIC2_IP, 2202, IP_TCP);
            }
        }
    }


DEFINE_PROGRAM