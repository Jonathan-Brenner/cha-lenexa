PROGRAM_NAME='Conference Room'

(* ------------------------------------------------------- *)
(*                                                         *)
(*  FILE CREATED ON: 08/22/2016  AT: 11:28:35              *)
(*  FILE_LAST_MODIFIED_ON: 10/20/2016  AT: 11:44:01        *)
(*                                                         *)
(*  System Type : NetLinx                                  *)
(*                                                         *)
(* ------------------------------------------------------- *)
(*                                                         *)
(*  Jonathan Brenner                                       *)
(*  AVI Systems                                            *)
(*                                                         *)
(*  Client: Children's Hospital Association                *)
(*  Project: Lenexa Office - Conference Center             *)
(*                                                         *)
(* ------------------------------------------------------- *)

DEFINE_DEVICE

    dvMaster = 0:1:0;       // DVX-3155HD-T

    dvBiamp = 0:3:0;        // Biamp Nexia TC

    dvCom1 = 5001:1:0;      // Extron Video Switch
    dvCom2 = 5001:2:0;      // Cisco Codec
    dvCom3 = 5001:3:0;      // USB Switch
    dvCom4 = 5001:4:0;      // Camera 1
    dvCom5 = 5001:5:0;      // Camera 2
    dvCom6 = 5001:6:0;      // Biamp (incoming call FB)

    dvRelays = 5001:8:0;    // Projector Screens

    dvSwitcher = 5002:1:0;  // Onboard Switcher

    dvDisplay1 = 6001:1:0;  // DXLink Receiver
    dvDisplay2 = 6002:1:0;  // DXLink Receiver

    // Touchpanel Button Groups
    dvTP_Main = 10001:1:0;
    dvTP_Presentation = 10001:2:0;
    dvTP_AudioCall = 10001:3:0;
    dvTP_VideoCall = 10001:4:0;
    dvTP_WebConf = 10001:5:0;

    vdvVolume_MAIN = 40001:1:0;
    vdvVolume_PRIV = 40002:1:0;
    vdvVolume_ACIN = 40003:1:0;
    vdvVolume_AOUT = 40004:1:0;
    vdvVolume_VCIN = 40005:1:0;
    vdvVolume_VOUT = 40006:1:0;



DEFINE_MUTUALLY_EXCLUSIVE

    ([dvRelays, 1], [dvRelays, 2])  // Left Screen Up/Down
    ([dvRelays, 3], [dvRelays, 4])  // Right Screen Up/Down



DEFINE_CONSTANT

    BIAMP_IP = '192.168.1.101';

    // Timelines
    TL_FEEDBACK = 1;
    TL_COOLING = 2;
    TL_WARMING = 3;
    TL_VOLUP = 4;
    TL_VOLDN = 5;
    TL_ACIN_VOLUP = 6;
    TL_ACIN_VOLDN = 7;
    TL_ACOUT_VOLUP = 8;
    TL_ACOUT_VOLDN = 9;
    TL_VCIN_VOLUP = 10;
    TL_VCIN_VOLDN = 11;
    TL_VCOUT_VOLUP = 12;
    TL_VCOUT_VOLDN = 13;

    // System States
    SYS_OFF = 0;
    SYS_WARM = 1;
    SYS_ON = 2;
    SYS_COOL = 3;

    // System Modes
    INACTIVE = 0;
    PRESENTATION = 21;
    AUDIO_CALL = 22;
    VIDEO_CALL = 23;
    WEB_CONF = 24;

    // Video Sources
    NOTHING = 0;
    ROOM_PC = 5;
    VIA = 6;
    VTC_CODEC = 7;
    LAPTOP_1 = 9;
    LAPTOP_2 = 10;

    // Display
    DISPLAY_ON = "$BE, $EF, $03, $06, $00, $BA, $D2, $01, $00, $00, $60, $01, $00";
    DISPLAY_OFF = "$BE, $EF, $03, $06, $00, $2A, $D3, $01, $00, $00, $60, $00, $00";
    DISPLAY_HDMI = "$BE, $EF, $03, $06, $00, $0E, $D2, $01, $00, $00, $20, $03, $00";

    COOL_TIME = 90;
    WARM_TIME = 45;

    // Audio
    MIN_VOL = -40.0;
    MAX_VOL = 6.0;
    MIN_VOL_ALT = -20.0;
    MAX_VOL_ALT = 3.0;



DEFINE_VARIABLE

    integer biampConnected = FALSE;

    char codecQueue[65535];
    char biampQueue[65535];

    integer systemState = SYS_OFF;
    integer systemMode = INACTIVE;

    integer displayOn = FALSE;
    integer videoSource = NOTHING;
    integer presentationSource = NOTHING;

    integer volumeMuted_MAIN = FALSE;
    integer volumeMuted_ACIN = FALSE;
    integer volumeMuted_AOUT = FALSE;
    integer volumeMuted_VCIN = FALSE;
    integer volumeMuted_VOUT = FALSE;
    integer volumeMuted_MICS = FALSE;

    integer dialKeys[] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 25, 26 };
    char dialDigits[] = '0123456789*#.';

    integer acOffHook = FALSE;
    integer vcOffHook = FALSE;

    char acDialNumber[50];
    char vcDialNumber[50];
    char vcKeyboardText[255];

    integer vcContentSource = NOTHING;

    integer vcDoNotDisturb = FALSE;

    integer selfviewEnabled = FALSE;
    integer selfviewFullscreen = FALSE;
    char selfviewMonitor[20];
    char selfviewPosition[20];

    char incomingVideoCall_Number[50];
    char incomingVideoCall_Name[50];
    char incomingVideoCall_ID[10];

    char incomingAudioCall_Number[50];
    char incomingAudioCall_Name[50];

    integer selectedCamera = 1;
    integer cameraControls[] = { 132, 133, 134, 135, 158, 159 };
    integer cameraPresets[] = { 151, 152, 153, 154, 155, 156 };

    integer cameraPresetUpdated = FALSE;

    integer webConfSource = VIA;

    integer dualProjectors = FALSE;
    integer allowDualProjectorSwitch = TRUE;

    long tlFeedbackSteps[] = {100};
    long tlVolRampSteps[] = {250};
    long tlWarmingSteps[] = {1000};
    long tlCoolingSteps[] = {1000};

    char vcContacts[][255] = {
        '172.20.10.141',
        '172.20.10.142',
        '172.17.11.9',
        '172.17.11.12'
    }

    char vcContactsNames[][255] = {
        'Avery Room',
        'Salk Room',
        'Champions Room',
        'Mission Room'
    }

    char acRegisterCommands[][255] = {
        'GETD 1 FDRLVL Program 1 ',
        'GETD 1 FDRMUTE Program 1 ',
        'GETD 1 FDRLVL AudioCallIn 1 ',
        'GETD 1 FDRMUTE AudioCallIn 1 ',
        'GETD 1 FDRLVL AudioCallOut 1 ',
        'GETD 1 FDRMUTE AudioCallOut 1 ',
        'GETD 1 FDRLVL VideoCallIn 1 ',
        'GETD 1 FDRMUTE VideoCallIn 1 ',
        'GETD 1 FDRLVL VideoCallOut 1 ',
        'GETD 1 FDRMUTE VideoCallOut 1 ',
        'GETD 1 FDRMUTE Privacy 1 ',
        'GETD 1 TIHOOKSTATE Dialer '
    };

    char vcRegisterCommands[][255] = {
        'xFeedback register Status/Conference/Presentation/LocalSource',
        'xFeedback register Status/Audio/Volume',
        'xFeedback register Status/Audio/VolumeMute',
        'xFeedback register Status/Audio/Microphones/Mute',
        'xFeedback register Status/Call',
        'xFeedback register Event/CallDisconnect',
        'xFeedback register Event/CallSuccessful',
        'xFeedback register Event/CameraPresetListUpdated',
        'xFeedback register Event/IncomingCallIndication',
        'xFeedback register Event/Peripherals',
        'xFeedback register Event/PresentationStarted',
        'xFeedback register Event/PresentationStopped',
        'xFeedback register Status/Standby',
        'xFeedback register Configuration/Conference/AutoAnswer',
        'xFeedback register Configuration/Conference/FarEndControl/Mode',
        'xFeedback register Status/Conference/DoNotDisturb',
        'xFeedback register Configuration/UserInterface/OSD',
        'xFeedback register Status/Video/Selfview',
        'xFeedback register Configuration/NetworkServices/SIP Mode',
        'xFeedback register Configuration/NetworkServices/H323 Mode',
        'xFeedback register Status/Cameras/SpeakerTrack',
        'xFeedback register Configuration/Network 1/IPv4',
        'xFeedback register Status/SystemUnit/State/NumberOfActiveCalls',
        'xStatus Audio Volume',
        'xStatus Audio Microphones Mute',
        'xStatus Call',
        'xStatus Conference Presentation LocalSource',
        'xStatus Standby Active',
        'xConfiguration Conference 1 AutoAnswer Mode\nxConfiguration Conference 1 AutoAnswer',
        'xConfiguration Conference 1 FarEndControl Mode',
        'xStatus Conference DoNotDisturb',
        'xConfiguration UserInterface OSD Output',
        'xStatus Video Selfview',
        'xStatus Cameras SpeakerTrack Status',
        'xStatus Network 1 IPv4',
        'xStatus SystemUnit'
    };


//----------------------------------------//
//  Functions
//----------------------------------------//

    //----------------------------------------//
    // System Startup
    //----------------------------------------//
    // wVideo: boolean
    //      0 -> Don't turn display(s) on
    //     !0 -> Turn display(s) on
    //----------------------------------------//
    DEFINE_FUNCTION systemStartup(integer wVideo)
    {
        // Startup Display
        if (wVideo)
        {
            // Start Warmup
            TIMELINE_CREATE(TL_WARMING, tlWarmingSteps, length_array(tlWarmingSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);

            send_string dvDisplay1, "$BE, $EF, $03, $06, $00, $BA, $D2, $01, $00, $00, $60, $01, $00";
            PULSE[dvRelays, 2];

        systemState = SYS_ON;

            wait 450
                send_string dvDisplay1, "$BE, $EF, $03, $06, $00, $0E, $D2, $01, $00, $00, $20, $03, $00";
        }

        // Unmute Audio
        volumeMute('Program', FALSE);
        wait 20
            volumeMute('Privacy', FALSE);
    }

    //----------------------------------------//
    // System Shutdown
    //----------------------------------------//
    DEFINE_FUNCTION systemShutdown()
    {
        // Start Cooldown
        TIMELINE_CREATE(TL_COOLING, tlCoolingSteps, length_array(tlCoolingSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        send_command dvTP_Main, "'PAGE-Start'";
        send_command dvTP_Main, "'@PPN-Cooling;Start'";

        sourceSwitch(NOTHING);

        send_string dvDisplay1, "$BE, $EF, $03, $06, $00, $2A, $D3, $01, $00, $00, $60, $00, $00";
        send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $2A, $D3, $01, $00, $00, $60, $00, $00";

        PULSE[dvRelays, 1];
        PULSE[dvRelays, 3];

        volumeMute('Program', TRUE);
        wait 20
            volumeMute('Privacy', TRUE);

        acHangUp();
        vcHangUp();

        wait 20 send_string dvCom2, "'xCommand Standby Activate', $0D";

        wait 40 send_string dvBiamp, "'SETD 1 FDRLVL AudioCallIn 1 -6', $0D";
        wait 60 send_string dvBiamp, "'SETD 1 FDRLVL AudioCallOut 1 -6', $0D";
        wait 80 send_string dvBiamp, "'SETD 1 FDRLVL VideoCallIn 1 -6', $0D";
        wait 100 send_string dvBiamp, "'SETD 1 FDRLVL VideoCallOut 1 -6', $0D";

        wait 120 send_string dvBiamp, "'SETD 1 FDRLVL Program 1 -12', $0D";

        dualProjectors = 0
    }

    //----------------------------------------//
    // Main Source Selection
    //----------------------------------------//
    // src: input number (5, 6, 7, 9, 10)
    //----------------------------------------//
    DEFINE_FUNCTION sourceSwitch(integer src)
    {
        videoSource = src;

        send_command dvSwitcher, "'CLVIDEOI', itoa(src), 'O1T'";
        send_command dvSwitcher, "'CLVIDEOI', itoa(src), 'O3T'";

        if (src <> VTC_CODEC)
            send_command dvSwitcher, "'CLAUDIOI', itoa(src), 'O2T'";
        else
            send_command dvSwitcher, "'CLAUDIOI0O2T'";

        // Switch USB
        if (src == ROOM_PC || src == VIA)
            send_string dvCom3, "itoa(src-4), '!'";
    }

    //----------------------------------------//
    // Content Source Selection
    //----------------------------------------//
    // src: input number (5, 6, 9, 10)
    //----------------------------------------//
    DEFINE_FUNCTION contentSourceSwitch(integer src)
    {
        vcContentSource = src;
        send_command dvSwitcher, "'CLVIDEOI', itoa(src), 'O2T'";
        send_command dvSwitcher, "'CLVIDEOI', itoa(src), 'O2T'";
        send_command dvSwitcher, "'CLAUDIOI', itoa(src), 'O2T'";
    }


    //----------------------------------------//
    // Volume Controls
    //----------------------------------------//
    DEFINE_FUNCTION volumeUp(char id[], integer step)
    {
        send_string dvBiamp, "'INCD 1 FDRLVL ', id, ' 1 ', itoa(step), $0A";
    }

    DEFINE_FUNCTION volumeDown(char id[], integer step)
    {
        send_string dvBiamp, "'DECD 1 FDRLVL ', id, ' 1 ', itoa(step), $0A";
    }

    DEFINE_FUNCTION volumeMute(char id[], integer state)
    {
        send_string dvBiamp, "'SETD 1 FDRMUTE ', id, ' 1 ', itoa(state), $0A";
    }


    //----------------------------------------//
    // Audio Call Controls
    //----------------------------------------//
    DEFINE_FUNCTION acDial()
    {
        send_string dvBiamp, "'DIAL 1 TIPHONENUM Dialer ', acDialNumber, $0A";
    }

    DEFINE_FUNCTION acHangUp()
    {
        send_string dvBiamp, "'SETD 1 TIHOOKSTATE Dialer 1', $0A";
        acDialNumber = '';
        send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
    }

    DEFINE_FUNCTION acKey(integer key)
    {
        if (key < 25)
        {
            acDialNumber = "acDialNumber, dialDigits[key-9]";
            if (acOffHook)
                send_string dvBiamp, "'DIAL 1 TIPHONENUM Dialer ', dialDigits[key-9], $0A";
        }
        else if (key == 25) // Backspace
        {
            integer length;
            length = length_string(acDialNumber);

            if (length > 1)
                acDialNumber = left_string(acDialNumber, length-1);
            else
                acDialNumber = '';
        }
        else if (key == 26) // Clear
        {
            acDialNumber = '';
        }

        send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
    }

    //----------------------------------------//
    // Video Call Controls
    //----------------------------------------//
    DEFINE_FUNCTION vcDial(char number[])
    {
        send_string dvCom2, "'xCommand Dial Number:', $22, number, $22, $0D";
    }

    DEFINE_FUNCTION vcHangUp()
    {
        send_string dvCom2, "'xCommand Call DisconnectAll', $0D";
    }

    DEFINE_FUNCTION vcKey(integer key)
    {
        if (key < 25)
        {
            vcDialNumber = "vcDialNumber, dialDigits[key-9]";

            if (vcOffHook)
                send_string dvCom2, "'xCommand DTMFSend DTMFString:', dialDigits[key-9], $0D";
        }
        else if (key == 25)
        {
            integer length;
            length = length_string(vcDialNumber);

            if (length > 1)
                vcDialNumber = left_string(vcDialNumber, length-1);
            else
                vcDialNumber = '';
        }
        else if (key == 26)
        {
            vcDialNumber = '';
        }
        send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
    }

    //----------------------------------------//
    // Initialize Codec
    //----------------------------------------//
    DEFINE_FUNCTION initCodec()
    {
        integer i;

        for (i = 1; i <= 36; i++)
        {
            send_string dvCom2, "vcRegisterCommands[i], $0D";
        }
    }

    //----------------------------------------//
    // Parse Codec Feeback
    //----------------------------------------//
    DEFINE_FUNCTION parseCodecFeedback(char str[])
    {
        char prefix[4];

        // Strip carraige return and line feed
        str = left_string(str, length_string(str)-2);

        prefix = remove_string(str, ' ', 1);

        if (prefix == '*s ')
        {
            if (find_string(str, 'Call ', 1))
            {
                char status[20];

                remove_string(str, 'Status: ', 1);

                status = remove_string(str, ' ', 1);

                if (status == 'Connected ')
                {
                    vcOffHook = 1;
                    send_command dvTP_Main, '@PPF-IncomingVideoCall';
                }
                else if (status == 'Disconnected ' || status == 'Idle ')
                {
                    vcOffHook = 0;
                    send_command dvTP_Main, '@PPF-IncomingVideoCall';
                }
            }
            else
            {
                char message[255];
                message = remove_string(str, ': ', 1);

                select
                {
                    active (message == 'Conference DoNotDisturb: '):
                        vcDoNotDisturb = (str == 'Active');
                    active (message == 'Video Selfview Mode: '):
                        selfviewEnabled = (str == 'On');
                    active (message == 'Video Selfview FullscreenMode: '):
                        selfviewFullscreen = (str == 'On');
                    active (message == 'Video Selfview PIPPosition: '):
                        selfviewPosition = str;
                    active (message == 'Video Selfview OnMonitorRole: '):
                        selfviewMonitor = str;
                }
            }
        }
        else if (prefix == '*e ')
        {
            if (str == 'CameraPresetListUpdated')
            {
               cameraPresetUpdated = 1;
               wait 30
                    cameraPresetUpdated = 0;
            }
            else if (str == 'IncomingCallIndication ')
            {
                send_command dvTP_Main, '@PPN-IncomingVideoCall';

                remove_string(str, 'RemoteURI: ', 1);
                incomingVideoCall_Number = remove_string(str, ' ', 1);

                remove_string(str, 'DisplayNameValue: ', 1);
                incomingVideoCall_Name = remove_string(str, ' ', 1);

                remove_string(str, 'CallId: ', 1);
                incomingVideoCall_ID = remove_string(str, ' ', 1);

                send_command dvTP_VideoCall, "'^TXT-2,0,', incomingVideoCall_Number";
            }
        }
    }

    //----------------------------------------//
    // Parse Level (Normal Range)
    //----------------------------------------//
    DEFINE_FUNCTION float parseLevel(char str[])
    {
        float level;
        level = atof(str);
        return ((level - MIN_VOL) * 100) / (MAX_VOL - MIN_VOL);
    }

    //----------------------------------------//
    // Parse Level (Alternate Range)
    //----------------------------------------//
    DEFINE_FUNCTION float parseLevel_Alt(char str[])
    {
        float level;
        level = atof(str);
        return ((level - MIN_VOL_ALT) * 100) / (MAX_VOL_ALT - MIN_VOL_ALT);
    }

    //----------------------------------------//
    // Initialize Biamp
    //----------------------------------------//
    DEFINE_FUNCTION initBiamp(integer i)
    {

        send_string dvBiamp, "acRegisterCommands[i], $0D";
        i++;

        wait 20
        {
            if (i <= 13)
                initBiamp(i);
        }
    }

    //----------------------------------------//
    // Parse Biamp Feeback
    //----------------------------------------//
    DEFINE_FUNCTION parseBiampFeedback(char str[])
    {
        // Strip carraige return and line feed
        str = left_string(str, length_string(str)-2);

        send_string 0, "'[Biamp]{String}:', str";

        if (find_string(str, '#', 1))
        {
            char tag[25];
            char id[50];
                                                            // #INCD 1 FDRLVL Program 1 0.0 +OK
            remove_string(str, ' ', 1);                     // 1 FDRLVL Program 1 0.0 +OK
            remove_string(str, ' ', 1);                     // FDRLVL Program 1 0.0 +OK
            tag = remove_string(str, ' ', 1);               // Program 1 0.0 +OK
            tag = left_string(tag, length_string(tag) - 1);
            id = remove_string(str, ' ', 1);                // 1 0.0 +OK
            id = left_string(id, length_string(id) - 1);

            if (id <> 'Dialer')
                remove_string(str, ' ', 1);                 // 0.0 +OK

            if (find_string(str, '+OK', 1))
                str = left_string(str, length_string(str)-4);   // 0.0

            select
            {
                active (id == 'Program'):
                {
                    if (tag == 'FDRLVL')
                        send_level dvTP_Main, 1, type_cast(parseLevel(str));
                    else if (tag == 'FDRMUTE')
                        volumeMuted_MAIN = (str == '1');
                }

                active (id == 'AudioCallIn'):
                {
                    if (tag == 'FDRLVL')
                        send_level dvTP_Main, 2, type_cast(parseLevel_Alt(str));
                    else if (tag == 'FDRMUTE')
                        volumeMuted_ACIN = (str == '1');
                }

                active (id == 'AudioCallOut'):
                {
                    if (tag == 'FDRLVL')
                        send_level dvTP_Main, 3, type_cast(parseLevel_Alt(str));
                    else if (tag == 'FDRMUTE')
                        volumeMuted_AOUT = (str == '1');
                }

                active (id == 'VideoCallIn'):
                {
                    if (tag == 'FDRLVL')
                        send_level dvTP_Main, 4, type_cast(parseLevel_Alt(str));
                    else if (tag == 'FDRMUTE')
                        volumeMuted_VCIN = (str == '1');
                }

                active (id == 'VideoCallOut'):
                {
                    if (tag == 'FDRLVL')
                        send_level dvTP_Main, 5, type_cast(parseLevel_Alt(str));
                    else if (tag == 'FDRMUTE')
                        volumeMuted_VOUT = (str == '1');
                }

                active (id == 'Privacy'):
                {
                    volumeMuted_MICS = (str == '1');
                }

                active (id == 'Dialer'):
                {
                    if (tag == 'TIHOOKSTATE')
                    {
                        acOffHook = (str == '0');
                    }
                    else if (tag == 'TICIDUSER')
                    {
                        char cidName[10];
                        char cidNumber[10];

                        remove_string(str, ' ', 1);
                        incomingAudioCall_Number = remove_string(str, ' ', 1);
                        incomingAudioCall_Number = mid_string(incomingAudioCall_Number, 2, length_string(incomingAudioCall_Number) - 3);
                        incomingAudioCall_Number = itoa(atoi(incomingAudioCall_Number));  // Remove leading 0's from string

                        incomingAudioCall_Name = mid_string(incomingAudioCall_Name, 2, length_string(incomingAudioCall_Name) - 2);

                        send_command dvTP_AudioCall, "'^TXT2,0,', incomingAudioCall_Number";
                    }
                }
            }
        }
    }


DEFINE_START

    CREATE_BUFFER dvCom2, codecQueue;
    CREATE_BUFFER dvBiamp, biampQueue;



DEFINE_EVENT

//----------------------------------------//
//  Logo
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 10]
    {
        HOLD[100]:
        {
            // TODO: Setup Page
        }
    }


//----------------------------------------//
//  Shutdown
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 9]
    {
        PUSH:
        {
            if (systemState == SYS_ON)
                send_command dvTP_Main, "'@PPN-Shutdown'";
        }
    }

    BUTTON_EVENT[dvTP_Main, 11]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPF-Shutdown'";
        }
    }

    BUTTON_EVENT[dvTP_Main, 12]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPF-Shutdown'";
            systemShutdown();
        }
    }


//----------------------------------------//
//  Display Manual On/Off
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 13]
    {
        PUSH:
        {
            send_string dvDisplay1, "$BE, $EF, $03, $06, $00, $BA, $D2, $01, $00, $00, $60, $01, $00";
            PULSE[dvRelays, 2];

            wait 450
                send_string dvDisplay1, "$BE, $EF, $03, $06, $00, $0E, $D2, $01, $00, $00, $20, $03, $00";
        }
    }

    BUTTON_EVENT[dvTP_Main, 14]
    {
        PUSH:
        {
            send_string dvDisplay1, "$BE, $EF, $03, $06, $00, $2A, $D3, $01, $00, $00, $60, $00, $00";
            PULSE[dvRelays, 1];
        }
    }

    BUTTON_EVENT[dvTP_Main, 15]
    {
        PUSH:
        {
            send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $BA, $D2, $01, $00, $00, $60, $01, $00";
            PULSE[dvRelays, 4];

            wait 450
                send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $0E, $D2, $01, $00, $00, $20, $03, $00";
        }
    }

    BUTTON_EVENT[dvTP_Main, 16]
    {
        PUSH:
        {
            send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $2A, $D3, $01, $00, $00, $60, $00, $00";
            PULSE[dvRelays, 3];
        }
    }


//----------------------------------------//
//  Startup Modes
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 20] // Settings
    {
        PUSH:
        {
            send_command dvTP_Main, "'PAGE-Settings'";
            systemMode = 20;
        }
    }

    BUTTON_EVENT[dvTP_Main, PRESENTATION]   // Presentation Mode
    BUTTON_EVENT[dvTP_Main, AUDIO_CALL]     // Audio Call Mode
    BUTTON_EVENT[dvTP_Main, VIDEO_CALL]     // Video Call Mode
    BUTTON_EVENT[dvTP_Main, WEB_CONF]       // Web Conference Mode
    {
        PUSH:
        {
            integer usingVideo;
            usingVideo = 1;

            switch (button.input.channel)
            {
                case PRESENTATION:
                {
                    if (presentationSource <> 0)
                        sourceSwitch(presentationSource);
                    else
                        sourceSwitch(ROOM_PC);

                    send_command dvTP_Main, "'PAGE-Presentation'";
                }
                case AUDIO_CALL:
                {
                    usingVideo = 0;
                    send_command dvTP_Main, "'PAGE-Audio Call'";
                }
                case VIDEO_CALL:
                {
                    sourceSwitch(VTC_CODEC);
                    send_command dvTP_Main, "'PAGE-Video Call'";
                    send_command dvTP_Main, "'@PPN-Dial;Video Call'";
                    send_string dvCom2, "'xCommand Standby Deactivate', $0D";
                }
                case WEB_CONF:
                {
                    sourceSwitch(webConfSource);
                    send_command dvTP_Main, "'PAGE-Web Call'";
                    send_string dvCom2, "'xCommand Standby Deactivate', $0D";
                }
            }

            systemMode = button.input.channel;

            selectedCamera = 1;
            send_string dvCom1, "'1!'";

            if (systemState == SYS_OFF)
                systemStartup(usingVideo);
        }
    }


//----------------------------------------//
//  Volume Controls
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 30]
    {
        PUSH:
        {
            volumeUp('Program', 3);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VOLUP))
                TIMELINE_KILL(TL_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 31]
    {
        PUSH:
        {
            volumeDown('Program', 3);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VOLDN))
                TIMELINE_KILL(TL_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 32]
    {
        PUSH:
        {
            volumeMute('Program', !volumeMuted_MAIN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 35]
    {
        PUSH:
        {
            volumeMute('Privacy', !volumeMuted_MICS);
        }
    }


//----------------------------------------//
//  Audio Call In Volume Controls
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 40]
    {
        PUSH:
        {
            volumeUp('AudioCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACIN_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACIN_VOLUP))
                TIMELINE_KILL(TL_ACIN_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 41]
    {
        PUSH:
        {
            volumeDown('AudioCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACIN_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACIN_VOLDN))
                TIMELINE_KILL(TL_ACIN_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 42]
    {
        PUSH:
        {
            volumeMute('AudioCallIn', !volumeMuted_ACIN);
        }
    }

//----------------------------------------//
//  Default Audio Call Volume Settings
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 45]
    {
        PUSH:
        {
            send_string dvBiamp, "'SETD 1 FDRLVL AudioCallIn 1 -6', $0D";
            wait 20
                send_string dvBiamp, "'SETD 1 FDRLVL AudioCallOut 1 -6', $0D";
        }
    }


//----------------------------------------//
//  Audio Call Out Volume Controls
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 50]
    {
        PUSH:
        {
            volumeUp('AudioCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACOUT_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACOUT_VOLUP))
                TIMELINE_KILL(TL_ACOUT_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 51]
    {
        PUSH:
        {
            volumeDown('AudioCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_ACOUT_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_ACOUT_VOLDN))
                TIMELINE_KILL(TL_ACOUT_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 52]
    {
        PUSH:
        {
            volumeMute('AudioCallOut', !volumeMuted_AOUT);
        }
    }


//----------------------------------------//
//  Video Call In Volume Controls
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 60]
    {
        PUSH:
        {
            volumeUp('VideoCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCIN_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCIN_VOLUP))
                TIMELINE_KILL(TL_VCIN_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 61]
    {
        PUSH:
        {
            volumeDown('VideoCallIn', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCIN_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCIN_VOLDN))
                TIMELINE_KILL(TL_VCIN_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 62]
    {
        PUSH:
        {
            volumeMute('VideoCallIn', !volumeMuted_VCIN);
        }
    }


//----------------------------------------//
//  Default Video Call Volume Settings
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 65]
    {
        PUSH:
        {
            send_string dvBiamp, "'SETD 1 FDRLVL VideoCallIn 1 -6', $0D";
            wait 20
                send_string dvBiamp, "'SETD 1 FDRLVL VideoCallOut 1 -6', $0D";
        }
    }


//----------------------------------------//
//  Video Call Out Volume Controls
//----------------------------------------//

    BUTTON_EVENT[dvTP_Main, 70]
    {
        PUSH:
        {
            volumeUp('VideoCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCOUT_VOLUP, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCOUT_VOLUP))
                TIMELINE_KILL(TL_VCOUT_VOLUP);
        }
    }

    BUTTON_EVENT[dvTP_Main, 71]
    {
        PUSH:
        {
            volumeDown('VideoCallOut', 1);
        }
        HOLD[10]:
        {
            TIMELINE_CREATE(TL_VCOUT_VOLDN, tlVolRampSteps, length_array(tlVolRampSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
        }
        RELEASE:
        {
            if (TIMELINE_ACTIVE(TL_VCOUT_VOLDN))
                TIMELINE_KILL(TL_VCOUT_VOLDN);
        }
    }

    BUTTON_EVENT[dvTP_Main, 72]
    {
        PUSH:
        {
            volumeMute('VideoCallOut', !volumeMuted_VOUT);
        }
    }


//----------------------------------------//
//  Source Selection
//----------------------------------------//

    BUTTON_EVENT[dvTP_Presentation, 0]
    {
        PUSH:
        {
            sourceSwitch(button.input.channel);
            presentationSource = button.input.channel;
        }
    }


//----------------------------------------//
//  Audio Call Controls
//----------------------------------------//

    // Dial Button
    BUTTON_EVENT[dvTP_AudioCall, 5]
    {
        PUSH:
        {
            acDial();
        }
    }

    // Hang Up Button
    BUTTON_EVENT[dvTP_AudioCall, 6]
    {
        PUSH:
        {
            acHangUp();
        }
    }

    // Answer Incoming Call
    BUTTON_EVENT[dvTP_AudioCall, 7]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingAudioCall';
            send_string dvBiamp, "'SETD 1 TIHOOKSTATE Dialer 0', $0D";
            send_command dvTP_Main, 'PAGE-AudioCall';
            systemMode = AUDIO_CALL;
        }
    }

    // Reject Incoming Call
    BUTTON_EVENT[dvTP_AudioCall, 8]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingAudioCall';
            send_string dvBiamp, "'SETD 1 TIHOOKSTATE Dialer 0', $0D";
            wait 20
                send_string dvBiamp, "'SETD 1 TIHOOKSTATE Dialer 1', $0D";
        }
    }

    // Keypad Keys
    BUTTON_EVENT[dvTP_AudioCall, dialKeys]
    {
        PUSH:
        {
            acKey(button.input.channel);
        }
    }

    // WebEx Number
    BUTTON_EVENT[dvTP_AudioCall, 100]
    {
        PUSH:
        {
            acDialNumber = '818552448681';
            acDial();
        }
    }


//----------------------------------------//
//  Video Call Controls
//----------------------------------------//

    // Text Area Press (to bring up keyboard)
    BUTTON_EVENT[dvTP_VideoCall, 1]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@AKB-', vcDialNumber, ';Enter Address'";
        }
    }

    // Flip to Content Page
    BUTTON_EVENT[dvTP_VideoCall, 2]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Content;Video Call'";
        }
    }

    // Flip to Dial Page
    BUTTON_EVENT[dvTP_VideoCall, 3]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-Dial;Video Call'";
        }
    }

    // Popup speed dial entries
    BUTTON_EVENT[dvTP_VideoCall, 4]
    {
        PUSH:
        {
            send_command dvTP_Main, "'@PPN-VTCSpeedDial;Video Call'";
        }
    }

    // Dial Button
    BUTTON_EVENT[dvTP_VideoCall, 5]
    {
        PUSH:
        {
            vcDial(vcDialNumber);
        }
    }

    // Hang Up Button
    BUTTON_EVENT[dvTP_VideoCall, 6]
    {
        PUSH:
        {
            vcHangUp();
        }
    }

    // Answer Incoming Call
    BUTTON_EVENT[dvTP_VideoCall, 7]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingVideoCall';
            send_string dvCom2, "'xCommand Call Accept', $0D";
            send_command dvTP_Main, 'PAGE-VideoCall';
            systemMode = VIDEO_CALL;
        }
    }

    // Reject Incoming Call
    BUTTON_EVENT[dvTP_VideoCall, 8]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-IncomingVideoCall';
            send_string dvCom2, "'xCommand Call Reject', $0D";
        }
    }

    BUTTON_EVENT[dvTP_VideoCall, 9] //Dual Projector
    {
        PUSH:
        {
            if (allowDualProjectorSwitch)
            {
                allowDualProjectorSwitch = FALSE;

                dualProjectors = !dualProjectors;

                if (dualProjectors)
                {
                    // Start Warmup
                    if (TIMELINE_ACTIVE(TL_WARMING))
                        TIMELINE_RESTART(TL_WARMING);
                    else
                        TIMELINE_CREATE(TL_WARMING, tlWarmingSteps, length_array(tlWarmingSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);

                    send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $BA, $D2, $01, $00, $00, $60, $01, $00";
                    PULSE[dvRelays, 4];

                    wait 450
                        send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $0E, $D2, $01, $00, $00, $20, $03, $00";
                }
                else
                {
                    send_string dvDisplay2, "$BE, $EF, $03, $06, $00, $2A, $D3, $01, $00, $00, $60, $00, $00";
                    PULSE[dvRelays, 3];
                }

                wait 450
                    allowDualProjectorSwitch = TRUE;
            }
        }
    }

    // Keypad Keys
    BUTTON_EVENT[dvTP_VideoCall, dialKeys]
    {
        PUSH:
        {
            vcKey(button.input.channel);
        }
    }

    // Open 'Speed-Dial' selection
    BUTTON_EVENT[dvTP_VideoCall, 30]
    {
        PUSH:
        {
            send_command dvTP_Main, '@PPF-VTCSpeedDial';
        }
    }

    // Call 'Speed Dial' entries
    BUTTON_EVENT[dvTP_VideoCall, 31]
    BUTTON_EVENT[dvTP_VideoCall, 32]
    BUTTON_EVENT[dvTP_VideoCall, 33]
    BUTTON_EVENT[dvTP_VideoCall, 34]
    BUTTON_EVENT[dvTP_VideoCall, 35]
    {
        PUSH:
        {
            integer index;
            index = button.input.channel-30;

            send_command dvTP_Main, '@PPF-VTCSpeedDial';
            vcDial(vcContacts[index]);
            vcDialNumber = vcContacts[index];
            send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
        }
    }

    // Menu Navigation
    BUTTON_EVENT[dvTP_VideoCall, 45]
    BUTTON_EVENT[dvTP_VideoCall, 46]
    BUTTON_EVENT[dvTP_VideoCall, 47]
    BUTTON_EVENT[dvTP_VideoCall, 48]
    BUTTON_EVENT[dvTP_VideoCall, 49]
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 45:{ send_string dvCom2, "'xCommand Key Press Key: Up', $0D"; }
                case 46:{ send_string dvCom2, "'xCommand Key Press Key: Down', $0D"; }
                case 47:{ send_string dvCom2, "'xCommand Key Press Key: Left', $0D"; }
                case 48:{ send_string dvCom2, "'xCommand Key Press Key: Right', $0D"; }
                case 49:{ send_string dvCom2, "'xCommand Key Press Key: Ok', $0D"; }
            }
        }

        RELEASE:
        {
            switch(button.input.channel)
            {
                case 45:{ send_string dvCom2, "'xCommand Key Release Key: Up', $0D"; }
                case 46:{ send_string dvCom2, "'xCommand Key Release Key: Down', $0D"; }
                case 47:{ send_string dvCom2, "'xCommand Key Release Key: Left', $0D"; }
                case 48:{ send_string dvCom2, "'xCommand Key Release Key: Right', $0D"; }
                case 49:{ send_string dvCom2, "'xCommand Key Release Key: Ok', $0D"; }
            }
        }
    }

    // Home Button
    BUTTON_EVENT[dvTP_VideoCall, 50]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: Home', $0D"; }
    }
    // Back Button
    BUTTON_EVENT[dvTP_VideoCall, 81]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: C', $0D"; }
    }
    // Layout Button
    BUTTON_EVENT[dvTP_VideoCall, 99]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: Layout', $0D"; }
    }
    // Directory Button
    BUTTON_EVENT[dvTP_VideoCall, 105]
    {
        PUSH:{ send_string dvCom2, "'xCommand Key Click Key: PhoneBook', $0D"; }
    }

    // Content Source Selection
    BUTTON_EVENT[dvTP_VideoCall, 120]
    BUTTON_EVENT[dvTP_VideoCall, 125]
    BUTTON_EVENT[dvTP_VideoCall, 126]
    BUTTON_EVENT[dvTP_VideoCall, 129]
    BUTTON_EVENT[dvTP_VideoCall, 130]
    {
        PUSH:
        {
            contentSourceSwitch(button.input.channel-120);

            if (button.input.channel-120)
                send_string dvCom2, "'xCommand Presentation Start PresentationSource: 2', $0D";
            else
                send_string dvCom2, "'xCommand Presentation Stop', $0D";
        }
    }

    // Self View On/Off
    BUTTON_EVENT[dvTP_VideoCall, 160]
    BUTTON_EVENT[dvTP_VideoCall, 161]
    {
        PUSH:
        {
            if (button.input.channel - 160)
                send_string dvCom2, "'xCommand Video Selfview Set Mode: On', $0D";
            else
                send_string dvCom2, "'xCommand Video Selfview Set Mode: Off', $0D";
        }
    }

    // Self View Fullscreen On/Off
    BUTTON_EVENT[dvTP_VideoCall, 162]
    BUTTON_EVENT[dvTP_VideoCall, 163]
    {
        PUSH:
        {
            if (button.input.channel - 162)
                send_string dvCom2, "'xCommand Video Selfview Set FullscreenMode: On', $0D";
            else
                send_string dvCom2, "'xCommand Video Selfview Set FullscreenMode: Off', $0D";
        }
    }

    // Self View Show on Left/Right
    BUTTON_EVENT[dvTP_VideoCall, 164]
    BUTTON_EVENT[dvTP_VideoCall, 165]
    {
        PUSH:
        {
            if (button.input.channel - 164)
                send_string dvCom2, "'xCommand Video Selfview Set OnMonitorRole: First', $0D";
            else
                send_string dvCom2, "'xCommand Video Selfview Set OnMonitorRole: Second', $0D";
        }
    }

    // Self View PIP position
    BUTTON_EVENT[dvTP_VideoCall, 171]   // UpperLeft
    BUTTON_EVENT[dvTP_VideoCall, 172]   // UpperCenter
    BUTTON_EVENT[dvTP_VideoCall, 173]   // UpperRight
    BUTTON_EVENT[dvTP_VideoCall, 174]   // CenterLeft
    BUTTON_EVENT[dvTP_VideoCall, 175]   // CenterRight
    BUTTON_EVENT[dvTP_VideoCall, 176]   // LowerLeft
    BUTTON_EVENT[dvTP_VideoCall, 177]   // LowerRight
    {
        PUSH:
        {
            switch(button.input.channel)
            {
                case 171:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: UpperLeft', $0D"; }
                case 172:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: UpperCenter', $0D"; }
                case 173:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: UpperRight', $0D"; }
                case 174:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: CenterLeft', $0D"; }
                case 175:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: CenterRight', $0D"; }
                case 176:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: LowerLeft', $0D"; }
                case 177:{ send_string dvCom2, "'xCommand Video Selfview Set PIPPosition: LowerRight', $0D"; }
            }
        }
    }


//----------------------------------------//
//  Camera Controls
//----------------------------------------//

    BUTTON_EVENT[dvTP_VideoCall, 141]
    BUTTON_EVENT[dvTP_VideoCall, 142]
    BUTTON_EVENT[dvTP_VideoCall, 143]
    {
        PUSH:
        {
            selectedCamera = (button.input.channel - 140);

            if (selectedCamera <> 3)
                send_string dvCom1, "itoa(selectedCamera), '!'";
        }
    }

    BUTTON_EVENT[dvTP_VideoCall, cameraControls]
    {
        PUSH:
        {
            switch (selectedCamera)
            {
                case 1:
                {
                    switch(button.input.channel)
                    {
                        case 132:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $01, $03, $FF"; }
                        case 133:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $02, $03, $FF"; }
                        case 134:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $01, $03, $03, $FF"; }
                        case 135:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $02, $03, $03, $FF"; }
                        case 158:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $03, $02, $FF"; }
                        case 159:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $03, $01, $FF"; }
                    }
                }
                case 2:
                {
                    switch(button.input.channel)
                    {
                        case 132:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $01, $03, $FF"; }
                        case 133:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $02, $03, $FF"; }
                        case 134:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $01, $03, $03, $FF"; }
                        case 135:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $02, $03, $03, $FF"; }
                        case 158:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $03, $02, $FF"; }
                        case 159:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $03, $01, $FF"; }
                    }
                }
                case 3:
                {
                    switch(button.input.channel)
                    {
                        case 132:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Up', $0D"; }
                        case 133:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Down', $0D"; }
                        case 134:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Left', $0D"; }
                        case 135:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: Right', $0D"; }
                        case 158:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: ZoomOut', $0D"; }
                        case 159:{ send_string dvCom2, "'xCommand FarEndControl Camera Move Value: In', $0D"; }
                    }
                }
            }
        }

        RELEASE:
        {
            switch (selectedCamera)
            {
                case 1:{ send_string dvCom4, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $03, $03, $FF"; }
                case 2:{ send_string dvCom5, "$81, $01, $06, $0A, $0C, $0A, $04, $03, $03, $03, $FF"; }
                case 3:{ send_string dvCom2, "'xCommand FarEndControl Camera Stop', $0D"; }
            }
        }
    }

    BUTTON_EVENT[dvTP_VideoCall, cameraPresets]
    {
        RELEASE:
        {
            integer preset;
            preset = (button.input.channel-150);

            switch (selectedCamera)
            {
                case 1:{ send_string dvCom4, "$81, $01, $04, $3F, $02, preset, $FF"; }
                case 2:{ send_string dvCom5, "$81, $01, $04, $3F, $02, preset, $FF"; }
                case 3:{ send_string dvCom2, "'xCommand FarEndControl Preset Activate PresetId: ', itoa(preset), $0D"; }
            }
        }

        HOLD[30]:
        {
            integer preset;
            preset = (button.input.channel-150);

            switch (selectedCamera)
            {
                case 1:{ send_string dvCom4, "$81, $01, $04, $3F, $01, preset, $FF"; }
                case 2:{ send_string dvCom5, "$81, $01, $04, $3F, $01, preset, $FF"; }
            }
        }
    }


//----------------------------------------//
//  Web Conference
//----------------------------------------//

    BUTTON_EVENT[dvTP_WebConf, 125]     // Use Room PC
    BUTTON_EVENT[dvTP_WebConf, 126]     // Use VIA
    {
        PUSH:
        {
            sourceSwitch(button.input.channel-120);
            webConfSource = button.input.channel-120;
        }
    }


//----------------------------------------//
//  Timelines
//----------------------------------------//

    TIMELINE_EVENT[TL_FEEDBACK]
    {
        // Main
        [dvTP_Main, PRESENTATION] = (systemMode == PRESENTATION);
        [dvTP_Main, AUDIO_CALL] = (systemMode == AUDIO_CALL);
        [dvTP_Main, VIDEO_CALL] = (systemMode == VIDEO_CALL);
        [dvTP_Main, WEB_CONF] = (systemMode == WEB_CONF);

        [dvTP_Main, 20] = (systemMode == 20);

        [dvTP_Main, 32] = volumeMuted_MAIN;
        [dvTP_Main, 35] = volumeMuted_MICS;

        [dvTP_Main, 42] = volumeMuted_ACIN;
        [dvTP_Main, 52] = volumeMuted_AOUT;
        [dvTP_Main, 62] = volumeMuted_VCIN;
        [dvTP_Main, 72] = volumeMuted_VOUT;

        // Presentation
        [dvTP_Presentation, ROOM_PC] = (videoSource == ROOM_PC);
        [dvTP_Presentation, LAPTOP_1] = (videoSource == LAPTOP_1);
        [dvTP_Presentation, LAPTOP_2] = (videoSource == LAPTOP_2);
        [dvTP_Presentation, VIA] = (videoSource == VIA);

        // Video Call
        [dvTP_VideoCall, 120] = (vcContentSource == NOTHING);
        [dvTP_VideoCall, 125] = (vcContentSource == ROOM_PC);
        [dvTP_VideoCall, 126] = (vcContentSource == VIA);
        [dvTP_VideoCall, 129] = (vcContentSource == LAPTOP_1);
        [dvTP_VideoCall, 130] = (vcContentSource == LAPTOP_2);

        [dvTP_VideoCall, 141] = (selectedCamera == 1);
        [dvTP_VideoCall, 142] = (selectedCamera == 2);
        [dvTP_VideoCall, 143] = (selectedCamera == 3);

        [dvTP_VideoCall, 160] = !selfviewEnabled;
        [dvTP_VideoCall, 161] = selfviewEnabled;

        [dvTP_VideoCall, 162] = !selfviewFullscreen;
        [dvTP_VideoCall, 163] = selfviewFullscreen;

        [dvTP_VideoCall, 164] = !selfviewMonitor;
        [dvTP_VideoCall, 165] = selfviewMonitor;

        [dvTP_VideoCall, 171] = (selfviewPosition == 'UpperLeft');
        [dvTP_VideoCall, 172] = (selfviewPosition == 'UpperCenter');
        [dvTP_VideoCall, 173] = (selfviewPosition == 'UpperRight');
        [dvTP_VideoCall, 174] = (selfviewPosition == 'CenterLeft');
        [dvTP_VideoCall, 175] = (selfviewPosition == 'CenterRight');
        [dvTP_VideoCall, 176] = (selfviewPosition == 'LowerLeft');
        [dvTP_VideoCall, 177] = (selfviewPosition == 'LowerRight');

        // Web Conference
        [dvTP_WebConf, 125] = (webConfSource == ROOM_PC);
        [dvTP_WebConf, 126] = (webConfSource == VIA);

        [dvTP_VideoCall, 9] = dualProjectors;
    }

    TIMELINE_EVENT [TL_COOLING]  // Projector Cooling Countdown
    {
        integer progress;
        progress = type_cast(((TIMELINE.REPETITION * 100) / COOL_TIME));

        if (progress >= 0 && progress < 101)
        {
            systemState = SYS_COOL;
            send_level dvTP_Main, 6, progress;
        }
        else
        {
            send_command dvTP_Main, "'@PPF-Cooling'";
            systemState = SYS_OFF;
            TIMELINE_KILL(TL_COOLING);
        }
    }

    TIMELINE_EVENT [TL_WARMING]  // Projector Warming Countdown
    {
        integer progress;
        progress = type_cast(((TIMELINE.REPETITION * 100) / WARM_TIME));

        if (progress >= 0 && progress < 101)
        {
            send_command dvTP_Main, "'@PPN-Warming'";
            systemState = SYS_WARM;
            send_level dvTP_Main, 7, progress;
        }
        else
        {
            send_command dvTP_Main, "'@PPK-Warming'";
            systemState = SYS_ON;
            TIMELINE_KILL(TL_WARMING);
        }
    }


    TIMELINE_EVENT [TL_VOLUP]
    {
        volumeUp('Program', 3);
    }

    TIMELINE_EVENT [TL_VOLDN]
    {
        volumeDown('Program', 3);
    }

    TIMELINE_EVENT [TL_ACIN_VOLUP]
    {
        volumeUp('AudioCallIn', 1);
    }

    TIMELINE_EVENT [TL_ACIN_VOLDN]
    {
        volumeDown('AudioCallIn', 1);
    }

    TIMELINE_EVENT [TL_ACOUT_VOLUP]
    {
        volumeUp('AudioCallOut', 1);
    }

    TIMELINE_EVENT [TL_ACOUT_VOLDN]
    {
        volumeDown('AudioCallOut', 1);
    }

    TIMELINE_EVENT [TL_VCIN_VOLUP]
    {
        volumeUp('VideoCallIn', 1);
    }

    TIMELINE_EVENT [TL_VCIN_VOLDN]
    {
        volumeDown('VideoCallIn', 1);
    }

    TIMELINE_EVENT [TL_VCOUT_VOLUP]
    {
        volumeUp('VideoCallOut', 1);
    }

    TIMELINE_EVENT [TL_VCOUT_VOLDN]
    {
        volumeDown('VideoCallOut', 1);
    }


//----------------------------------------//
//  DATA EVENTS
//----------------------------------------//

    DATA_EVENT [dvMaster]
    {
        ONLINE:
        {
            TIMELINE_CREATE(TL_FEEDBACK, tlFeedbackSteps, length_array(tlFeedbackSteps), TIMELINE_RELATIVE, TIMELINE_REPEAT);
            IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
        }
    }

    DATA_EVENT [dvTP_Main]
    {
        ONLINE:
        {
            integer i;

            send_command dvTP_Main, "'PAGE-Start'";

            send_command dvTP_AudioCall, "'^TXT-1,0,', acDialNumber";
            send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";

            for (i = 1; i <= 4; i++)
                send_command dvTP_VideoCall, "'^TXT-1', itoa(i),',0,', vcContactsNames[i]";
        }

        STRING:
        {
            if (remove_string(data.text, 'KEYB-', 1))
            {
                send_string 0, "'[Keyboard]{String}:', data.text";

                if (!find_string(data.text, 'ABORT', 1) && !find_string(data.text, 'SWAP', 1))
                {
                    vcDialNumber = data.text;
                    send_command dvTP_VideoCall, "'^TXT-1,0,', vcDialNumber";
                }
            }
        }
    }

    DATA_EVENT [dvCom1] // Video Switcher
    {
        ONLINE:
        {
            send_command dvCom1, "'SET BAUD 9600,N,8,1'";
            send_string 0, "'[Switch]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Switch]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Switch]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom2] // VTC Codec
    {
        ONLINE:
        {
            send_command dvCom2, "'SET BAUD 38400,N,8,1'";
            send_string 0, "'[Codec]{Online}'";

            wait 30
                initCodec();
        }

        STRING:
        {
            send_string 0, "'[Codec]{String}:', data.text";

            while (find_string(codecQueue, "$0D, $0A", 1))
            {
                parseCodecFeedback(remove_string(codecQueue, "$0D, $0A", 1));
            }
        }

        ONERROR:
        {
            send_string 0, "'[Codec]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom3] // USB Switch
    {
        ONLINE:
        {
            send_command dvCom3, "'SET BAUD 9600,N,8,1'";
            send_string 0, "'[USB Switch]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[USB Switch]{String}: ', data.text";
        }

        ONERROR:
        {
            send_string 0, "'[USB Switch]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom4] // Front Camera
    {
        ONLINE:
        {
            send_command dvCom4, "'SET BAUD 9600,N,8,1'";
            send_string 0, "'[Front Camera]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Front Camera]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Front Camera]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom5] // Rear Camera
    {
        ONLINE:
        {
            send_command dvCom5, "'SET BAUD 9600,N,8,1'";
            send_string 0, "'[Rear Camera]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Rear Camera]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Rear Camera]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvCom6] // Biamp (Serial - For Incoming Call FB only)
    {
        ONLINE:
        {
            send_command dvCom6, "'SET BAUD 38400,N,8,1'";
            send_string 0, "'[Biamp232]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Biamp232]{String}:', DATA.TEXT";

            select
            {
                active(find_string(data.text, 'INCOMINGCALL 1', 1)):
                {
                    send_command dvTP_Main, '@PPN-IncomingAudioCall';
                    send_string dvBiamp, "'GETD 1 TICIDUSER Dialer', $0A";
                }
                active(find_string(data.text, 'INCOMINGCALL 0', 1)):
                    send_command dvTP_Main, '@PPF-IncomingAudioCall';
                active(find_string(data.text, 'ONHOOK', 1)):
                    acOffHook = FALSE;
                active(find_string(data.text, 'OFFHOOK', 1)):
                    acOffHook = TRUE;
            }
        }

        ONERROR:
        {
            send_string 0, "'[Biamp232]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvBiamp] // Biamp TesiraForte
    {
        ONLINE:
        {
            send_string 0, "'[Biamp]{Online}'";
            biampConnected = 1;
            send_string dvBiamp, "$FF, $FE, $01";
            initBiamp(1);
        }

        STRING:
        {
            //send_string 0, "'[Biamp]{String}:', DATA.TEXT";

            while (find_string(biampQueue, "$0D, $0A", 1))
            {
                parseBiampFeedback(remove_string(biampQueue, "$0D, $0A", 1));
            }
        }

        OFFLINE:
        {
            send_string 0, "'[Biamp]{Offline}'";

            biampConnected = 0;

            wait 50
                IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
        }

        ONERROR:
        {
            send_string 0, "'[Biamp]{Error}: [', itoa(data.number), '] ', data.text";

            wait 50
            {
                if (!biampConnected)
                    IP_CLIENT_OPEN(dvBiamp.port, BIAMP_IP, 23, IP_TCP);
            }
        }
    }

    DATA_EVENT [dvDisplay1] // Left Projector
    {
        ONLINE:
        {
            send_command dvDisplay1, "'SET BAUD 19200,N,8,1'";

            send_string 0, "'[Display1]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Display1]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Display1]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }

    DATA_EVENT [dvDisplay2] // Right Projector
    {
        ONLINE:
        {
            send_command dvDisplay2, "'SET BAUD 19200,N,8,1'";
            send_string 0, "'[Display2]{Online}'";
        }

        STRING:
        {
            send_string 0, "'[Display2]{String}:', DATA.TEXT";
        }

        ONERROR:
        {
            send_string 0, "'[Display2]{Error}: [', itoa(data.number), '] ', data.text";
        }
    }


DEFINE_PROGRAM
