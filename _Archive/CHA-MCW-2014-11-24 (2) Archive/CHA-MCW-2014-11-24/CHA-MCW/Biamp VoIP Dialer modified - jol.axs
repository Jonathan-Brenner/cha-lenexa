MODULE_NAME='Biamp VoIP Dialer modified - jol' (DEV vdvDialer, DEV dvPanel)
(***********************************************************)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 05/02/2014  AT: 10:33:51        *)
(***********************************************************)
(* System Type : NetLinx                                   *)
(***********************************************************)
(* REV HISTORY:                                            *)
(***********************************************************)
(*
    $History: $
*)    
(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
//#INCLUDE 'SNAPI'

DEFINE_CONSTANT
///////////////////////////////////////////////////////////////////////
// Pulled from SNAPI.axi
DIAL_REDIAL            = 201   // Momentary: Redial
DIAL_FLASH_HOOK        = 208   // Momentary: Flash hook

DIAL_AUTO_ANSWER       = 204   // Momentary: Cycle auto answer state
DIAL_AUTO_ANSWER_FB    = 239   // Feedback:  Auto answer state feedback
////////////////////////////////////////////////////////////////////////

// added - jol
vtDial	= 161


MAX_CALLS = 6;

////////////////////////////////////////////////////////////
// VoIP Dialer Extension Channels
////////////////////////////////////////////////////////////
VOIP_ANSWER = 301;
VOIP_END = 302;
VOIP_SEND = 303;
VOIP_RESUME = 304;
VOIP_CONF = 305;
VOIP_HOLD = 306;

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

STRUCTURE _CALL_STATUS
{
	CHAR cCallState[50];
	CHAR cCallPrompt[50];
	CHAR cCallName[50];
	CHAR cCallNumber[50];
}

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

VOLATILE CHAR bDTMFSend;		// set default to 1 ???

VOLATILE CHAR cDialNumber[50];

VOLATILE INTEGER nCurrentCall;

VOLATILE _CALL_STATUS uCallStatus[MAX_CALLS];

(***********************************************************)
(*               LATCHING DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_LATCHING

(***********************************************************)
(*       MUTUALLY EXCLUSIVE DEFINITIONS GO BELOW           *)
(***********************************************************)
DEFINE_MUTUALLY_EXCLUSIVE

(***********************************************************)
(*        SUBROUTINE/FUNCTION DEFINITIONS GO BELOW         *)
(***********************************************************)

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

(***********************************************************)
(*                THE EVENTS GO BELOW                      *)
(***********************************************************)
DEFINE_EVENT

////////////////////////////////////////////////////////////
// DIAL KEYPAD 
////////////////////////////////////////////////////////////
BUTTON_EVENT[dvPanel, 161]					// DIGIT 0
BUTTON_EVENT[dvPanel, 162]					// DIGIT 1
BUTTON_EVENT[dvPanel, 163]					// DIGIT 2
BUTTON_EVENT[dvPanel, 164]					// DIGIT 3
BUTTON_EVENT[dvPanel, 165]					// DIGIT 4
BUTTON_EVENT[dvPanel, 166]					// DIGIT 5
BUTTON_EVENT[dvPanel, 167]					// DIGIT 6
BUTTON_EVENT[dvPanel, 168]					// DIGIT 7
BUTTON_EVENT[dvPanel, 169]					// DIGIT 8
BUTTON_EVENT[dvPanel, 170]					// DIGIT 9
BUTTON_EVENT[dvPanel, 171]					// DIGIT *
BUTTON_EVENT[dvPanel, 172]					// DIGIT #
{                   
	PUSH:
	{
		STACK_VAR INTEGER nButton;
		STACK_VAR CHAR cDigit[1];
		
		nButton = BUTTON.INPUT.CHANNEL - 161;
		
		TO[BUTTON.INPUT];
		
		SWITCH (nButton)
		{
			CASE 10:
			{
				cDigit = '*';
				BREAK;
			}
			CASE 11:
			{
				cDigit = '#';
				BREAK;
			}
			DEFAULT:
			{
				cDigit = ITOA(nButton);
				BREAK;
			}
		}

		cDialNumber = "cDialNumber, cDigit"
		
		IF (bDTMFSend)
			SEND_COMMAND vdvDialer,"'DTMF-',cDigit";
		
		SEND_COMMAND dvPanel,"'^TXT-',ITOA(vtDial),',0,',cDialNumber";
	}
}

BUTTON_EVENT[dvPanel, 173]					// BACK SPACE
{
	PUSH:
	{
		IF (LENGTH_STRING(cDialNumber))
		{
			TO[BUTTON.INPUT];
			
			SET_LENGTH_STRING(cDialNumber, LENGTH_STRING(cDialNumber) - 1);
			SEND_COMMAND dvPanel,"'^TXT-',ITOA(vtDial),',0,',cDialNumber";
		}
	}
}

BUTTON_EVENT[dvPanel, 174]					// CLEAR
{
	PUSH:
	{
		TO[BUTTON.INPUT];

		cDialNumber = "";
		SEND_COMMAND dvPanel,"'^TXT-',ITOA(vtDial),',0,',cDialNumber";
	}
}

BUTTON_EVENT[dvPanel, 175]					// SEND DTMF
{
	PUSH:
	{
		bDTMFSend = !bDTMFSend;
	}
}

////////////////////////////////////////////////////////////
// CALL SELECTION
////////////////////////////////////////////////////////////
BUTTON_EVENT[dvPanel, 181]					// CALL 1
BUTTON_EVENT[dvPanel, 182]					// CALL 2
BUTTON_EVENT[dvPanel, 183]					// CALL 3
BUTTON_EVENT[dvPanel, 184]					// CALL 4
BUTTON_EVENT[dvPanel, 185]					// CALL 5
BUTTON_EVENT[dvPanel, 186]					// CALL 6
{
	PUSH:
	{
		SEND_COMMAND vdvDialer,"'VOIP_ACTIVE_CALL-',ITOA(BUTTON.INPUT.CHANNEL - 180)";
		
		cDialNumber = "";
		SEND_COMMAND dvPanel,"'^TXT-',ITOA(vtDial),',0,',cDialNumber";
	}
}

////////////////////////////////////////////////////////////
// Answer Call
////////////////////////////////////////////////////////////
BUTTON_EVENT[dvPanel, 201]					// Answer Call 1
BUTTON_EVENT[dvPanel, 202]					// Answer Call 2
BUTTON_EVENT[dvPanel, 203]					// Answer Call 3
BUTTON_EVENT[dvPanel, 204]					// Answer Call 4
BUTTON_EVENT[dvPanel, 205]					// Answer Call 5
BUTTON_EVENT[dvPanel, 206]					// Answer Call 6
{
	PUSH:
	{
		LOCAL_VAR INTEGER nCall;
		
		nCall = (BUTTON.INPUT.CHANNEL - 200);
		
		TO[BUTTON.INPUT];
		
		IF (nCall != nCurrentCall)
		{
			SEND_COMMAND vdvDialer,"'VOIP_ACTIVE_CALL-',ITOA(nCall)";
		
			cDialNumber = "";
			SEND_COMMAND dvPanel,"'^TXT-',ITOA(vtDial),',0,',cDialNumber";
		}
		
		CANCEL_WAIT_UNTIL 'WAITING FOR CALL CHANGE'
		WAIT_UNTIL (nCall == nCurrentCall) 'WAITING FOR CALL CHANGE'
		{
			PULSE[vdvDialer,VOIP_ANSWER];
		}
	}
}
////////////////////////////////////////////////////////////
// CALL CONTROL
////////////////////////////////////////////////////////////
BUTTON_EVENT[dvPanel, 191]					// DIAL NUMBER
{
	PUSH:
	{
		IF (LENGTH_STRING(cDialNumber) > 0)
		{
			TO[BUTTON.INPUT];
			
			SEND_COMMAND vdvDialer,"'DIALNUMBER-',cDialNumber";
		}
	}
}

BUTTON_EVENT[dvPanel, 192]					// END
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, VOIP_END];
		
		cDialNumber=""
		SEND_COMMAND dvPanel,"'^TXT-',ITOA(vtDial),',0,',cDialNumber";// clear number
	}
}

BUTTON_EVENT[dvPanel, 193]					// RESUME
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, VOIP_RESUME];
	}
}

BUTTON_EVENT[dvPanel, 194]					// CONF
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, VOIP_CONF];
	}
}

BUTTON_EVENT[dvPanel, 195]					// SEND
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, VOIP_SEND];
	}
}

BUTTON_EVENT[dvPanel, 196]					// REDIAL
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, DIAL_REDIAL];
	}
}

BUTTON_EVENT[dvPanel, 197]					// FLASH
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, DIAL_FLASH_HOOK];
	}
}

BUTTON_EVENT[dvPanel, 198]					// ANSWER
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, VOIP_ANSWER];
	}
}

BUTTON_EVENT[dvPanel, 199]					// HOLD
{
	PUSH:
	{
		TO[BUTTON.INPUT];
		
		PULSE[vdvDialer, VOIP_HOLD];
	}
}

BUTTON_EVENT[dvPanel, 200]					// AUTO-ANSWER
{
	PUSH:
	{
		PULSE[vdvDialer, DIAL_AUTO_ANSWER];
	}
}

////////////////////////////////////////////////////////////
// DIALER COMMUNICATION
////////////////////////////////////////////////////////////
DATA_EVENT[vdvDialer]
{
	COMMAND:
	{
		STACK_VAR INTEGER nCall;
		
		SELECT
		{
			// INCOMINGCALL-<call>,<number>
			ACTIVE (FIND_STRING(DATA.TEXT,'INCOMINGCALL-',1) > 0):
			{
				REMOVE_STRING(DATA.TEXT,'INCOMINGCALL-',1);
				
				nCall = ATOI(REMOVE_STRING(DATA.TEXT,',',1));
			
				SEND_COMMAND dvPanel,"'^TXT-',ITOA(200 + nCall),',0,',DATA.TEXT";
			
				IF ([vdvDialer, DIAL_AUTO_ANSWER_FB] == FALSE)
					SEND_COMMAND dvPanel,"'@PPN-VoIP Incoming Call',ITOA(nCall)";
			}
			ACTIVE (FIND_STRING(DATA.TEXT,'VOIP_ACTIVE_CALL-',1) > 0):
			{
				REMOVE_STRING(DATA.TEXT,'VOIP_ACTIVE_CALL-',1);
				
				nCurrentCall = ATOI(DATA.TEXT);
				
				IF (nCurrentCall > 0 && nCurrentCall <= MAX_CALLS)
				{
					SEND_COMMAND dvPanel,"'^TXT-181,0,',uCallStatus[nCurrentCall].cCallState";
					SEND_COMMAND dvPanel,"'^TXT-182,0,',uCallStatus[nCurrentCall].cCallPrompt";
					SEND_COMMAND dvPanel,"'^TXT-183,0,',uCallStatus[nCurrentCall].cCallName";
					SEND_COMMAND dvPanel,"'^TXT-184,0,',uCallStatus[nCurrentCall].cCallNumber";
				}
			}
			ACTIVE (FIND_STRING(DATA.TEXT,'VOIP_CALL_STATE-',1) > 0):
			{
				REMOVE_STRING(DATA.TEXT,'VOIP_CALL_STATE-',1);
				
				nCall = ATOI(REMOVE_STRING(DATA.TEXT,',',1));

				IF (nCall > 0 && nCall <= MAX_CALLS)
				{				
					uCallStatus[nCall].cCallState = DATA.TEXT;
				
					IF (nCurrentCall == nCall)
						SEND_COMMAND dvPanel,"'^TXT-181,0,',uCallStatus[nCurrentCall].cCallState"
				}				
			}			
			ACTIVE (FIND_STRING(DATA.TEXT,'VOIP_CALL_PROMPT-',1) > 0):
			{
				REMOVE_STRING(DATA.TEXT,'VOIP_CALL_PROMPT-',1);
				
				nCall = ATOI(REMOVE_STRING(DATA.TEXT,',',1));

				IF (nCall > 0 && nCall <= MAX_CALLS)
				{				
					uCallStatus[nCall].cCallPrompt = DATA.TEXT;
				
					IF (nCurrentCall == nCall)
						SEND_COMMAND dvPanel,"'^TXT-182,0,',uCallStatus[nCurrentCall].cCallPrompt"
				}		
			}			
			ACTIVE (FIND_STRING(DATA.TEXT,'VOIP_CALL_NUMBER-',1) > 0):
			{
				REMOVE_STRING(DATA.TEXT,'VOIP_CALL_NUMBER-',1);

				nCall = ATOI(REMOVE_STRING(DATA.TEXT,',',1));

				IF (nCall > 0 && nCall <= MAX_CALLS)
				{				
					uCallStatus[nCall].cCallNumber = DATA.TEXT;
				
					IF (nCurrentCall == nCall)
						SEND_COMMAND dvPanel,"'^TXT-184,0,',uCallStatus[nCurrentCall].cCallNumber"
				}		
			}			
			ACTIVE (FIND_STRING(DATA.TEXT,'VOIP_CALL_NAME-',1) > 0):
			{
				REMOVE_STRING(DATA.TEXT,'VOIP_CALL_NAME-',1);

				nCall = ATOI(REMOVE_STRING(DATA.TEXT,',',1));

				IF (nCall > 0 && nCall <= MAX_CALLS)
				{				
					uCallStatus[nCall].cCallName = DATA.TEXT;
				
					IF (nCurrentCall == nCall)
						SEND_COMMAND dvPanel,"'^TXT-183,0,',uCallStatus[nCurrentCall].cCallName";
				}		
			}			
		}
	}
}

(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

////////////////////////////////////////////////////////////
// SEND DTMF FEEDBACK
////////////////////////////////////////////////////////////
[dvPanel, 175] = (bDTMFSend == TRUE);

////////////////////////////////////////////////////////////
// CALL SELECTION FEEDBACK
////////////////////////////////////////////////////////////
[dvPanel, 181] = (nCurrentCall == 1);
[dvPanel, 182] = (nCurrentCall == 2);
[dvPanel, 183] = (nCurrentCall == 3);
[dvPanel, 184] = (nCurrentCall == 4);
[dvPanel, 185] = (nCurrentCall == 5);
[dvPanel, 186] = (nCurrentCall == 6);

////////////////////////////////////////////////////////////
// CALL CONTROL FEEDBACK
////////////////////////////////////////////////////////////
[dvPanel, 200] = [vdvDialer, DIAL_AUTO_ANSWER_FB];

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)
