MODULE_NAME='SHARP IP MODULE' (dev vdvDevice,dev dvDevice)
(***********************************************************)
(*  FILE CREATED ON: 05/02/2014  AT: 13:49:02              *)
(***********************************************************)
(***********************************************************)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 06/05/2014  AT: 12:55:51        *)
(***********************************************************)
(* System Type : NetLinx                                   *)
(***********************************************************)
(* REV HISTORY:                                            *)
(***********************************************************)
(*
    $History: $
*)    
(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT

long tlIP_Connect	= 1

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

ip_address_struct ipDevice

long lIP_Connect_Times[]=
{
	30000
}

integer nPort=23

integer olDevice

char cmdQ_Device[100]

integer flPower
char sPwr_On[32]
char sPwr_Off[32]
char sInput_HDMI1[32]

(***********************************************************)
(*        SUBROUTINE/FUNCTION DEFINITIONS GO BELOW         *)
(***********************************************************)
(* EXAMPLE: DEFINE_FUNCTION <RETURN_TYPE> <NAME> (<PARAMETERS>) *)
(* EXAMPLE: DEFINE_CALL '<NAME>' (<PARAMETERS>) *)

define_call 'OpenIPConnection' ()
{
	// Stop any current connection attempts
	If(Timeline_Active(tlIP_Connect))
		Timeline_Kill(tlIP_Connect)
	
	// Start the timeline to attempt reconnection
	Timeline_Create(tlIP_Connect,lIP_Connect_Times,1,Timeline_Absolute,Timeline_Repeat)
	
	// Attempt to connect immediately
	Timeline_Set(tlIP_Connect,(lIP_Connect_Times[1]-10))
}


(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

sPwr_On="'POWR1   ',$0D"
sPwr_Off="'POWR0   ',$0D"

sInput_HDMI1="'IAVD1   ',$0D"


// for CHA only
nPort=50004

(***********************************************************)
(*                THE EVENTS GO BELOW                      *)
(***********************************************************)
DEFINE_EVENT

data_event[dvDevice]
{
	online:{on[olDevice];If(Timeline_Active(tlIP_Connect))
					Timeline_Kill(tlIP_Connect)}
	offline:{off[olDevice]/*;ip_client_close(data.device.port)*/}
	onerror:{send_string 0,"'onerror for device,',itoa(dvDevice.port),',error:',itoa(data.number)"}
}

channel_event[vdvDevice,0]
{
	on:
	{
		switch(channel.channel)
		{
			case 27:{cmdQ_Device="cmdQ_Device,sPwr_On";on[flPower]}
			case 28:{cmdQ_Device="cmdQ_Device,sPwr_Off";off[flPower]}
			case 196:{cmdQ_Device="cmdQ_Device,sInput_HDMI1"}
			case 100:{ip_client_close(dvDevice.port)}
			case 101:{ip_client_open(dvDevice.port,ipDevice.IPADDRESS,nPort,1)}
			
			
			case 241:
			{
				cmdQ_Device=""
			}
			case 242:
			{
				//off[olDevice]
				send_string 0,"'ip>',ipDevice.IPADDRESS"
				send_string 0,"'port>',itoa(nPort)"
			}
		}
	}
}

data_event[vdvDevice]
{
	command:
	{
		if(length_array(remove_string(data.text,'PROPERTY-IP_Address,',1)))
			ipDevice.IPADDRESS=data.text
		if(length_array(remove_string(data.text,'PROPERTY-Port,',1)))
			nPort=atoi(data.text)
		if(find_string(data.text,'REINIT',1))
		{
			if(olDevice)
				ip_client_close(dvDevice.port)
			wait 10
			{
				ip_client_open(dvDevice.port,ipDevice.IPADDRESS,nPort,1)
			}
		}
	}
}

TIMELINE_EVENT[tlIP_Connect]
{
	ip_client_open(dvDevice.PORT,ipDevice.IPADDRESS,nPort,1)	
}

(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

[vdvDevice,255]=flPower

wait 1
{
	if(length_array(cmdQ_Device) && olDevice)
		send_string dvDevice,"remove_string(cmdQ_Device,"$0D",1)"
	else if(!olDevice)
		call 'OpenIPConnection'
}


//wait 300
//{
//	if(!olDevice && length_array(cmdQ_Device))
//		ip_client_open(dvDevice.PORT,ipDevice.IPADDRESS,nPort,1)
//}

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)
