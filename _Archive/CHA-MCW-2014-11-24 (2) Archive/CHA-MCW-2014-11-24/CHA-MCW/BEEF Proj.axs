MODULE_NAME='BEEF Proj'(Dev vdvDevice,Dev dvDevice)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 04/30/2014  AT: 23:13:31        *)
(***********************************************************)

(************************************************************)
(* Notes:		                                           	*)
(*
   Tab spacing = 3
	Indent = 3
	
	- 
*)
(************************************************************)
(*	To be Added:															*)
(*
	- 
*)
(***********************************************************)
(* REV HISTORY:                                            *)
(***********************************************************)
(*
   Tab spacing = 3
	Indent = 3

	Rev 0:		First stable rev. To be used as template for all future modules. Features:
		- Multi-level debug notification
		- IP/RS-232 config & connectivity with enable/disable features
		- Stable IP connectivity with auto reconnect if comm is enabled
		- Customizable command input and string response
*)
(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT

//--- Module operation constants ---//

BUSY = 250

TL_Tx = 1			// Process transmit que
TL_Rx = 2			// Process receive buffer
TL_Connect = 3		// Attempt to reconnect IP

// Debug message levels
DBG_ANY	= 0		// General operation notices, basic IP connectivity
DBG_LVL1 = 1		// Module notifications
DBG_LVL2 = 2 		// Strings to and from device

//--- Device info constants ---//

TL_POLL		= 4

CH_FULLON	= 1
CH_WARMING	= 2
CH_COOLING	= 3
CH_FULLOFF	= 4

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

//--- Module operation variables ---//

Volatile Integer nDebug					// Current debug level
Non_Volatile Integer nBaudRate		// Baud rate of the device
Non_Volatile Integer nIPPort			// IP Port for communication with this device
Volatile Integer nCommDesired			// Do we want communication on or off?, On by default (Define Start)

Long lTimes[2]								// Generic timeline time array
Long lConnectTimes[1] =					// Attempt connection every 30 sec.
{
	30000
}
Long lPollTimes[15]

Char cDeviceDPS[30]						// Physical device's AMX device:port:system info; used for debug messages
Non_Volatile Char cIPAddress[15]		// Max length of 15... 4 octets
Non_Volatile Char cControl[5]			// 'RS232' or 'IP'
Char cIPConnection[6]					// 'OPEN' or 'CLOSED'

Volatile Char cTxQue[250]				// Stores commands to be sent to the device
Volatile Char bfRx[500]					// Buffer for responses from the device
Volatile Char cTrash[100]				// Temporarily hold garbage data for debugging purposes


//--- Device info variables ---//

Volatile Integer nFL_PollingPower	// Power Polling active
Volatile Integer nFL_PollingLamp		// Lamp Polling active
Volatile Integer nFL_VidMute			// Video mute state
Volatile Integer nFL_VidFreeze		// Video freeze state
Volatile Integer nLampHours			// Current runtime of lamps

Volatile Char cLampUsed_High[1]
Volatile Char cLampUsed_Low[1]

(***********************************************************)
(*               LATCHING DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_LATCHING

(***********************************************************)
(*       MUTUALLY EXCLUSIVE DEFINITIONS GO BELOW           *)
(***********************************************************)
DEFINE_MUTUALLY_EXCLUSIVE

//([vdvDevice,CH_FULLON],[vdvDevice,CH_WARMING],[vdvDevice,CH_COOLING],[vdvDevice,CH_FULLOFF])

(***********************************************************)
(*        SUBROUTINE/FUNCTION DEFINITIONS GO BELOW         *)
(***********************************************************)

//////////////////////////////////////////////////////////////////////
//
// Function: FormatStringVal(integer nValue, integer nLength)
//
//////////////////////////////////////////////////////////////////////
Define_Function char[4] FormatStringVal(Char nValue[4], integer nLength)
{
	return Right_String("'000',nValue",nLength)
}

//////////////////////////////////////////////////////////////////////
//
//		Module Config/Operation Functions (Should not need to modify)
//
///////////////////////////////////////////////////////////////////////
Define_Function DebugMsg(Char cMsg[80], Integer nLvl)
{
	// Send debug messages if the level permits it
	If(nDebug >= nLvl)
		Send_String 0,"'Device ',cDeviceDPS,' - ',cMsg"
}

Define_Call 'OpenIPConnection'()
{
	If(nCommDesired == 1)		// Check to see if comm is desired
	{
		// Stop any current connection attempts
		If(Timeline_Active(TL_Connect))
			Timeline_Kill(TL_Connect)
		
		// Start the timeline to attempt reconnection
		Timeline_Create(TL_Connect,lConnectTimes,1,Timeline_Absolute,Timeline_Repeat)
		
		// Attempt to connect immediately
		Timeline_Set(TL_Connect,(lConnectTimes[1]-10))
	}
}

Define_Call 'AddtoTxQue' (Char cCmd[255])
{
	// If we have disabled comm with the unit, tell the programmer
	If(nCommDesired == 0)
		DebugMsg("'Communication disabled, please reenable and try again'", DBG_LVL1)
	
	// If the connection is closed and we want to try opening it, notify the programmer and make the attempt
	Else If((cControl == 'IP') && (cIPConnection == 'CLOSED') && (nCommDesired == 1))
	{
		DebugMsg("'Connection to host lost... attempting to reconnect'", DBG_ANY)
		Call 'OpenIPConnection'()
	}
	
	// Check to see if comm is active - Comment out the line below to allow the que to hold commands till comm is reestablished
	If(((cControl == 'RS232') || (cIPConnection == 'OPEN')) && (nCommDesired == 1))
		cTxQue = "cTxQue,cCmd,$0D"				// Add to Tx Que
}

Define_Call 'SendTxQue' ()
{
   Local_Var Char cCMD[50]
	
	// Check to see if comm is active
	If(((cControl == 'RS232') || (cIPConnection == 'OPEN')) && (nCommDesired == 1))
	{
		If(![dvDevice,BUSY] && Length_String(cTxQue))
		{
			cCMD = Remove_String(cTxQue,"$0D",1)
//			Set_Length_String(cCMD,(Length_String(cCMD)-1))
			Send_String dvDevice,"cCMD"
			DebugMsg("'Sent Cmd: ',cCMD", DBG_LVL2)
			On[dvDevice,BUSY]
			Wait(2) 'Device que'
				Off[dvDevice,BUSY]
		}
	}
}

//////////////////////////////////////////////////////////////////////
//
//		Feedback processing (Edit as needed)
//
//////////////////////////////////////////////////////////////////////
Define_Call 'ProcessRxQue' ()
{
	Local_Var Char cTemp[255],cString[255],cLamp[4]
	Local_Var Integer nTemp
	
	// Process as long as something is in the buffer
	If(Length_String(bfRx))
	{
		// Pull out the next command
//		cString = Remove_String(bfRx,"$0D",1)
		// Remove prefix
		cTemp = Remove_String(bfRx,"$1D",1)
		// Extract relevant info - In some cases it may be "$00,$00"
		cString = Left_String(bfRx,2)
		// Remove extracted info from buffer for cleanup purposes
		cTemp = Remove_String(bfRx,"cString",1)
		
		// Remove $00
		Set_Length_String(cString,(Length_String(cString)-1))
		
		Select
		{
			// Basic feedback
//			Active(Find_String(cString,"$06",1)):	DebugMsg("'ACK'", DBG_LVL1)
//			Active(Find_String(cString,"'?'",1)):	DebugMsg("'NACK'", DBG_LVL1)
			
			// Error message received (Ex. Extron Switch error messages)
			//Active(Find_String(cString,'E01',1)):	DebugMsg("'E01:Invalid input channel number (out of range)'", DBG_LVL1)
			//Active(Find_String(cString,'E10',1)):	DebugMsg("'E10:Invalid command'", DBG_LVL1)
			//Active(Find_String(cString,'E11',1)):	DebugMsg("'E11:Invalid preset'", DBG_LVL1)
			//Active(Find_String(cString,'E13',1)):	DebugMsg("'E13:Invalid value (out of range)'", DBG_LVL1)
			
			// All other feedback from the device
			Active(1):
			{
				Select
				{
					Active(nFL_PollingPower == 1):		// If in the process of requesting feedback
					{
						Select
						{
							Active(FIND_STRING(cString,"$01",1)):	{Send_String vdvDevice,"'STATUS-On'";		PowerFeedback(CH_FULLON)}	// Power On
							Active(FIND_STRING(cString,"$00",1)):	{Send_String vdvDevice,"'STATUS-Off'";		PowerFeedback(CH_FULLOFF)}	// Standby
							Active(FIND_STRING(cString,"$02",1)):	{Send_String vdvDevice,"'STATUS-Cooling'";PowerFeedback(CH_COOLING)}	// Cooling down in process
						}
						nFL_PollingPower = 0
//						Clear_Buffer bfRx
					}
					// Lamp Hours
					Active(nFL_PollingLamp > 0):			// If Polling for lamp hours
					{
						// If polling for the high byte
//						If(nFL_PollingLamp == 1)
//						{
//							// Store the value
//							If(cString == "$15")	{cLampUsed_High = $00}
//							Else						{cLampUsed_High = cString}
//							
//							// Poll for the low byte
//							nFL_PollingLamp  = 2
//							Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$C2,$FF,$02,$00,$90,$10,$00,$00")
//						}
						// If polling for the low byte
						If(nFL_PollingLamp == 1)
//						If(nFL_PollingLamp == 2)
						{
							// Store the value
							If(cString == "$15")	{cLampUsed_Low = $00}
							Else						{cLampUsed_Low = cString}
							
							// Calculate the current lamp hours used
							nLampHours = (cLampUsed_High + cLampUsed_Low)
							
							// Send new values to main program
							Send_String vdvDevice,"'LAMPHOURS-',itoa(nLampHours)"
							
							nFL_PollingLamp = 0
						}
//						Clear_Buffer bfRx
					}
				}
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////
//
//		Misc Device specific functions (Add as needed)
//
//////////////////////////////////////////////////////////////////////

// For some reason, just making these mutually exclusive doesn't update the channels correctly to the virtual device
Define_Function PowerFeedback(Integer nStatus)
{
	Switch(nStatus)
	{
		Case CH_FULLON:
		{
			On[vdvDevice,CH_FULLON]
			Off[vdvDevice,CH_WARMING]
			Off[vdvDevice,CH_COOLING]
			Off[vdvDevice,CH_FULLOFF]
		}
		Case CH_WARMING:
		{
			Off[vdvDevice,CH_FULLON]
			On[vdvDevice,CH_WARMING]
			Off[vdvDevice,CH_COOLING]
			Off[vdvDevice,CH_FULLOFF]
		}
		Case CH_COOLING:
		{
			Off[vdvDevice,CH_FULLON]
			Off[vdvDevice,CH_WARMING]
			On[vdvDevice,CH_COOLING]
			Off[vdvDevice,CH_FULLOFF]
		}
		Case CH_FULLOFF:
		{
			Off[vdvDevice,CH_FULLON]
			Off[vdvDevice,CH_WARMING]
			Off[vdvDevice,CH_COOLING]
			On[vdvDevice,CH_FULLOFF]
		}
	}
}

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

//--- Default values for this device ---//
nCommDesired	= 1
cControl			= 'RS232'
nBaudRate		= 19200
cIPAddress		= '192.168.1.52'//'0.0.0.0'
cIPConnection	= 'CLOSED'
nIPPort			= 50004//23


//--- General ---//
cDeviceDPS = "itoa(dvDevice.number),':',itoa(dvDevice.port),':',itoa(dvDevice.system)"
Create_Buffer dvDevice,bfRx

//--- Timelines ---//
// Transmit timeline
lTimes[1] = 20			// Send commands every .02 sec
Timeline_Create(TL_Tx,lTimes,1,Timeline_Absolute,Timeline_Repeat)

// Feedback timeline
lTimes[1] = 20			// Update feedback every .02 sec
Timeline_Create(TL_Rx,lTimes,1,Timeline_Absolute,Timeline_Repeat)

// Lamp Hour Polling timeline
lPollTimes[1] = 5000
lPollTimes[2] = 10000
lPollTimes[3] = 15000
lPollTimes[4] = 20000
Timeline_Create(TL_POLL,lPollTimes,4,Timeline_Absolute,Timeline_Repeat)

OFF[vdvDevice,CH_FULLON]
OFF[vdvDevice,CH_WARMING]
OFF[vdvDevice,CH_COOLING]
OFF[vdvDevice,CH_FULLOFF]

(***********************************************************)
(*                THE EVENTS GO BELOW                      *)
(***********************************************************)
DEFINE_EVENT

//////////////////////////////////////////////////////////////////////
//
//		Device Communication events (Should not need to modify)
//			- String responses are handled through the device buffer
//
//////////////////////////////////////////////////////////////////////
Data_Event[dvDevice]
{
	Online:
	{
		Switch(cControl)
		{
			Case 'RS232':	// Reconfigure the RS-232 port
			{
				WAIT 100
				{
					Send_Command dvDevice,"'SET BAUD ',itoa(nBaudRate),',N,8,1'"
					Send_Command dvDevice,"'HSOFF'"
				}
			}
			Case 'IP':		// Notify the programmer and stop the reconnect timeline
			{
				cIPConnection = 'OPEN'
				DebugMsg('Connection to host established', DBG_ANY)
				If(Timeline_Active(TL_Connect))
					Timeline_Kill(TL_Connect)
			}
		}
		Wait 2				// Request status update
		{
			nFL_PollingPower = 1
			Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")
		}
	}
	Offline:
	{
		Switch(cControl)
		{
			Case 'IP':
			{
				DebugMsg("'Connection to host lost'", DBG_ANY)
				cIPConnection = 'CLOSED'
				
				Call 'OpenIPConnection'()
			}
		}
	}
	String:
	{
		DebugMsg("'Got String: ',data.text", DBG_LVL2)
	}
	OnError:
	{
		DebugMsg("'Got Error Code: ',itoa(data.number)", DBG_LVL1)
	}
}

//--- Timeline Events ---//
Timeline_Event[TL_Tx]
{
	Call 'SendTxQue'()
}

Timeline_Event[TL_Rx]
{
	Local_Var i
	
	// Feedback from device
	If(nCommDesired == 1)			// If we are processing responses...
		Call 'ProcessRxQue'()
	Else									// If comm is disabled...
		Clear_Buffer bfRx
	
	// Feedback to the main program
}

Timeline_Event[TL_Connect]
{
	// Attempt to reopen the port if we've lost communication
	If(cControl == 'IP')
	{
		Switch(cIPConnection)
		{
			Case 'OPEN':	{Timeline_Kill(TL_Connect)}
			Case 'CLOSED':	{IP_Client_Open(dvDevice.port,cIPAddress,nIPPort,1)}
		}
	}
}

Timeline_Event[TL_POLL]
{
	Switch(Timeline.Sequence)
	{
		Case 1:	{nFL_PollingPower = 1;Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")}
		Case 2:	{nFL_PollingPower = 1;Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")}
		Case 3:	{nFL_PollingPower = 1;Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")}
		Case 4:	{nFL_PollingLamp  = 1;Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$C2,$FF,$02,$00,$90,$10,$00,$00")}
	}
}

//////////////////////////////////////////////////////////////////////
//
//		Virtual Device Commands (Edit as needed)
//
//////////////////////////////////////////////////////////////////////

channel_event[vdvDevice,0]
{
	on:
	{
		switch(channel.channel)
		{
			case 27:{send_command vdvDevice,"'POWER-1'"}
			case 28:{send_command vdvDevice,"'POWER-0'"}
		}
	}
}
Data_Event[vdvDevice]
{
	Command:
	{
		Local_Var Char cIncoming[250]
		
		cIncoming = data.text
		
		Select
		{
			//----- Module Config/Connect Commands (***DO NOT MODIFY***) -----//
			Active(Find_String(cIncoming,'PASSTHRU-',1)):	// PASSTHRU=**
			{
				Remove_String(cIncoming,'PASSTHRU-',1)
				Call 'AddtoTxQue'("cIncoming")
			}
			Active(Find_String(cIncoming,'DEBUG-',1)):		// Set Debug level; See constant definitions... DGB_ANY
			{
				Remove_String(cIncoming,'DEBUG-',1)
				nDebug = atoi(cIncoming)
			}
			Active(Find_String(cIncoming,'SETBAUD-',1)):		// Reset the baud rate for the device
			{
				Remove_String(cIncoming,'SETBAUD-',1)
				nBaudRate = atoi(cIncoming)
				Send_Command dvDevice,"'SET BAUD ',itoa(nBaudRate),',N,8,1'"
			}
			Active(Find_String(cIncoming,'IPADDRESS-',1)):	// Set the IP address for the device
			{
				cTrash = REMOVE_STRING(cIncoming,'IPADDRESS-',1)
				cIPAddress = cIncoming
				
				// Attempt to open the connection if desired and module is set to use IP communication
				If((cControl == 'IP') && (nCommDesired == 1))
					Call 'OpenIPConnection'()
			}
			Active(Find_String(cIncoming,'CONTROL-',1)):
			{
				cTrash = REMOVE_STRING(cIncoming,'CONTROL-',1)
				Switch(cIncoming)
				{
					Case 'RS232':
					Case 'IP':
					{
						cControl = cIncoming
						
						// Attempt to open the IP connection if desired and an IP address has been provided
						If((cControl == 'IP') && (nCommDesired == 1))
						{
							If(Length_String(cIPAddress) > 0)
								Call 'OpenIPConnection'()
							Else
								DebugMsg('Config Error: No IP Address specified', DBG_LVL1)
						}
					}
					Case 'ENABLE':
					{
						nCommDesired = 1
						
						Switch(cControl)
						{
							Case 'RS232':
							{
								If(Timeline_Active(TL_Connect))
									Timeline_Kill(TL_Connect)			// Stop trying to connect IP
								
								Send_Command dvDevice,"'SET BAUD ',itoa(nBaudRate),',N,8,1'"
								Send_Command dvDevice,"'HSOFF'"
							}
							Case 'IP':
							{
								// Attempt to open the IP connection if an IP address has been provided
								If(Length_String(cIPAddress) > 0)
									Call 'OpenIPConnection'()
								Else
									DebugMsg('Config Error: No IP Address specified', DBG_LVL1)
							}
						}
					}
					Case 'DISABLE':
					{
						nCommDesired = 0
						
						Switch(cControl)
						{
							Case 'RS232':
							{
							}
							Case 'IP':
							{
								If(Timeline_Active(TL_Connect))
									Timeline_Kill(TL_Connect)		// Stop trying to connect
								
								IP_Client_Close(dvDevice.port)	// Close the IP port
							}
						}
					}
				}
			}
			
			//----- Device Control Commands (Add new commands here) -----//
			Active(Find_String(cIncoming,'AUTO',1)):		// AV-(1-9)
			{
				Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$91,$D0,$06,$00,$0A,$20,$00,$00")
			}
//			Active(Find_String(cIncoming,'REMOTE-',1)):	// INPUT-<input label>
//			{
//				Remove_String(cIncoming,'REMOTE-',1)
//				Switch(cIncoming)
//				{
//					Case 'MENU,ON':			{Call 'AddtoTxQue'("'C1C'")}
//					Case 'MENU,OFF':			{Call 'AddtoTxQue'("'C1D'")}
//					Case 'CURSOR,UP':			{Call 'AddtoTxQue'("'C3C'")}
//					Case 'CURSOR,DOWN':		{Call 'AddtoTxQue'("'C3D'")}
//					Case 'CURSOR,LEFT':		{Call 'AddtoTxQue'("'C3B'")}
//					Case 'CURSOR,RIGHT':		{Call 'AddtoTxQue'("'C3A'")}
//					Case 'CURSOR,OK':			{Call 'AddtoTxQue'("'C3F'")}
//				}
//			}
			Active(Find_String(cIncoming,'INPUT-',1)):	// INPUT-<input label>
			{
				Remove_String(cIncoming,'INPUT-',1)
				Switch(cIncoming)
				{
					Case 'VIDEO':	{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$6E,$D3,$01,$00,$00,$20,$01,$00")}
					Case 'SVIDEO':	{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$9E,$D3,$01,$00,$00,$20,$02,$00")}
					Case 'YUV':		{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$AE,$D1,$01,$00,$00,$20,$05,$00")}
					Case 'RGB1':	{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$FE,$D2,$01,$00,$00,$20,$00,$00")}
					Case 'RGB2':	{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$3E,$D0,$01,$00,$00,$20,$04,$00")}
					Case 'HDMI':	{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$0E,$D2,$01,$00,$00,$20,$03,$00")}
				}
			}
			Active(Find_String(cIncoming,'MUTE-',1)):		// MUTE-(1/0)
			{
				IF([vdvDevice,CH_FULLON]=1)
				{
					Remove_String(cIncoming,'MUTE-',1)
					SWITCH(cIncoming)
					{
						CASE '1':		{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$6B,$D9,$01,$00,$20,$30,$01,$00");ON[nFL_VidMute]}
						CASE '0':		{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$FB,$D8,$01,$00,$20,$30,$00,$00");OFF[nFL_VidMute]}
						CASE 'T':
						{
							IF(nFL_VidMute == 0)		{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$6B,$D9,$01,$00,$20,$30,$01,$00");ON[nFL_VidMute]}
							ELSE							{Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$FB,$D8,$01,$00,$20,$30,$00,$00");OFF[nFL_VidMute]}
						}
					}
				}
			}
//			Active(Find_String(cIncoming,'FREEZE-',1)):		// FREEZE-(1/0)
//			{
//				IF([vdvDevice,CH_FULLON]=1)
//				{
//					Remove_String(cIncoming,'FREEZE-',1)
//					SWITCH(cIncoming)
//					{
//						CASE '1':		{Call 'AddtoTxQue'("'C43'");ON[nFL_VidFreeze]}
//						CASE '0':		{Call 'AddtoTxQue'("'C44'");OFF[nFL_VidFreeze]}
//					}
//				}
//			}
//			Active(Find_String(cIncoming,'LENSSHIFT-',1)):	// PASSTHRU=**
//			{
//				Remove_String(cIncoming,'LENSSHIFT-',1)
//				SWITCH(cIncoming)
//				{
//					CASE 'UP':		{Call 'AddtoTxQue'("'C5D'")}
//					CASE 'DOWN':	{Call 'AddtoTxQue'("'C5E'")}
//					CASE 'LEFT':	{Call 'AddtoTxQue'("'C5F'")}
//					CASE 'RIGHT':	{Call 'AddtoTxQue'("'C60'")}
//				}
//			}
			Active(Find_String(cIncoming,'POWER',1)):	// POWER-(1/0)
			{
				IF(Find_String(cIncoming,'?',1))
				{
					nFL_PollingPower = 1
					Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")
				}
				ELSE
				{
					Remove_String(cIncoming,'POWER-',1)
					Switch(cIncoming)
					{
						Case '1':
						{
							// Send if the device is off
							IF([vdvDevice,CH_FULLOFF]=1)
							{
								Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$BA,$D2,$01,$00,$00,$60,$01,$00")
								
								Cancel_Wait 'Warming'
								Wait 5 'Warming'
								{
									nFL_PollingPower = 1
									Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")
								}
							}
						}
						Case '0':
						{
							// Send if the device is on
							IF([vdvDevice,CH_FULLON]=1)
							{
								Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$2A,$D3,$01,$00,$00,$60,$00,$00")
								
								Cancel_Wait 'Cooling'
								Wait 5 'Cooling'
								{
									nFL_PollingPower = 1
									Call 'AddtoTxQue'("$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00")
								}
							}
						}
					}
				}
			}
		}
	}
}

(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

[vdvDevice,20] = nFL_VidMute
[vdvDevice,21] = nFL_VidFreeze

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)

	// Power on				$BE,$EF,$03,$06,$00,$BA,$D2,$01,$00,$00,$60,$01,$00
	// Power off			$BE,$EF,$03,$06,$00,$2A,$D3,$01,$00,$00,$60,$00,$00
	// Power Poll			$BE,$EF,$03,$06,$00,$19,$D3,$02,$00,$00,$60,$00,$00
	// input video			$BE,$EF,$03,$06,$00,$6E,$D3,$01,$00,$00,$20,$01,$00
	// input svid			$BE,$EF,$03,$06,$00,$9E,$D3,$01,$00,$00,$20,$02,$00
	// input yuv			$BE,$EF,$03,$06,$00,$AE,$D1,$01,$00,$00,$20,$05,$00
	// input rgb1			$BE,$EF,$03,$06,$00,$FE,$D2,$01,$00,$00,$20,$00,$00
	// input rgb2			$BE,$EF,$03,$06,$00,$3E,$D0,$01,$00,$00,$20,$04,$00
	// input hdmi			$BE,$EF,$03,$06,$00,$0E,$D2,$01,$00,$00,$20,$03,$00
	// auto adjust			$BE,$EF,$03,$06,$00,$91,$D0,$06,$00,$0A,$20,$00,$00
	// mute on				$BE,$EF,$03,$06,$00,$6B,$D9,$01,$00,$20,$30,$01,$00
	// mute off				$BE,$EF,$03,$06,$00,$FB,$D8,$01,$00,$20,$30,$00,$00
// poll lamp low		$BE,$EF,$03,$06,$00,$C2,$FF,$02,$00,$90,$10,$00,$00
// poll lamp high		$BE,$EF,$03,$06,$00,$2A,$FD,$02,$00,$9E,$10,$00,$00














