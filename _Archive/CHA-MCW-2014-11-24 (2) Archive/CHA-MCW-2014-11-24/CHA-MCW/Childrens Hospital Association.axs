PROGRAM_NAME='Childrens Hospital Association'
(***********************************************************)
(*  FILE CREATED ON: 04/24/2014  AT: 12:59:27              *)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 10/03/2014  AT: 15:13:21        *)
(***********************************************************)
(***********************************************************)
(* System Type : NetLinx                                   *)
(***********************************************************)
(* REV HISTORY:                                            *)
(***********************************************************)
(*
    $History: $
*)

/*

ip addresses

wap #1				192.168.1.2	(SSID:AMX, login:admin,1988)
wap #2				192.168.1.3	(SSID:AMX, login:admin,1988)
cisco switch		192.168.1.4	(login:cisco,password)		(switched to .254?)

master				192.168.1.11
TP#1				192.168.1.21		(wired ip address = 192.168.1.27)
TP#2				192.168.1.22		(wired ip address = 192.168.1.28)

biamp				192.168.1.31


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SVSi Boxes [MAC prefixes: 00:19:0B:00 (EncDec-oders), 00:19:0B:80 (Transcoders)]

Room 510:
Encoder (Laptop #1)		192.168.1.61		MAC (22-6c)
Encoder (Laptop #2)		192.168.1.62		MAC (22-b6)
Encoder (Credenza PC)	192.168.1.63		MAC (1d-27)
Encoder (Bluray)		192.168.1.64		MAC (1d-a1)
Encoder (Codec)			192.168.1.65		MAC (1d-9d)

Decoder (LCD)			192.168.1.51		MAC (20-22)
Decoder (Projector)		192.168.1.52		MAC (1f-b8)
Decoder (Content)		192.168.1.53		MAC (20-e8)
Transcoder (Audio)		192.168.1.41		MAC (00-83)

Room 511:
Encoder (Laptop #1)		192.168.1.66 		MAC (1c-95)
Encoder (Credenza PC)	192.168.1.67		MAC (1d-89)
Encoder (Bluray)		192.168.1.68		MAC (1a-80)
Encoder (Codec)			192.168.1.69		MAC (22-6b)

Decoder (LCD)			192.168.1.54		MAC (20-d7)
Decoder (Content)		192.168.1.55		MAC (20-69)
Transcoder (Audio)		192.168.1.42		MAC (00-81)

Room 500:
Transcoder (Audio)		192.168.1.43		MAC (00-b0)

Hive 516:
Decoder (LCD)			192.168.1.56		MAC (20-d5)

Hive 517:                         
Decoder (LCD)			192.168.1.57 		MAC (20-c9)


*** Added 2014-05-30 ***
Comcast Tuner Encoder	192.168.1.70		MAC (??-??)		stream #10

*/


/*

items to populate:

-relay channel constants
-system startup and shutdown times

// items to go back over
double check routing Queues and syntax
double check page flip event (and feedback)

*/


(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

 dvMaster	= 0:1:0
 // 0:2:0 IS RESERVED
 dvMixer	= 0:3:0//5001:7:0//
 dvSVSi		= 0:4:0		// port used to connect to various decoders, may not be used
 
 dvDecoder_1	= 0:5:0		// Room A LCD
 dvDecoder_2	= 0:6:0		// Room A Proj
 dvDecoder_3	= 0:7:0		// Room A Content
 dvDecoder_4	= 0:8:0		// Room A Audio

 dvDecoder_5	= 0:9:0		// Room B LCD
 dvDecoder_6	= 0:10:0	// Room B Content
 dvDecoder_7	= 0:11:0	// Room audio

 dvDisplay_1	= 0:13:0
 dvProj			= 0:14:0
 dvDisplay_2	= 0:15:0


 dvCodec_1		= 5001:1:0
 dvCodec_2		= 5001:2:0
 dvCam_2		= 5001:3:0
 
 //dvDisplay_1	= 5001:4:0
// dvProj			= 5001:5:0
// dvDisplay_2	= 5001:6:0

 dvRelays		= 5001:8:0	

 dvIR_1			= 5001:9:0	// 
 dvIR_2			= 5001:10:0	//
 dvTuner		= 5001:11:0	// new comcast box added 2014-05-30
 
 // virtual devices

// Biamp Tesira //

vdvMixer			= 41001:1:0	//	used for initialization and presets only 
vdvOverflow			= 41001:2:0	// used for the mute which sends audio to 
// Fader ramps              
vdvVolume_RmA		= 41001:3:0	// Program (PA?) volume
vdvATC_Rx_RmA		= 41001:4:0	// Audio Conferencing Recieve
vdvVTC_Rx_RmA		= 41001:5:0 // Video Conferencing Recieve
// dialer block             
vdvDialer_RmA		= 41001:6:0 // VOIP Dialer Functions
vdvPrivacy_RmA		= 41001:7:0	// Privacy mic mute block

// Fader ramps              
vdvVolume_RmB		= 41001:8:0	// Program (PA?) volume
vdvATC_Rx_RmB		= 41001:9:0	// Audio Conferencing Recieve
vdvVTC_Rx_RmB		= 41001:10:0 // Video Conferencing Recieve
// dialer block             
vdvDialer_RmB		= 41001:11:0 // VOIP Dialer Functions
vdvPrivacy_RmB		= 41001:12:0	// Privacy mic mute block

// other virtuals

vdvCodec_1		= 41002:1:0
vdvCodec_2		= 41003:1:0


vdvProj			= 33001:1:0//41004:1:0	// hitatchi
vdvDisplay_1	= 33002:1:0 //41005:1:0	// sharp lc-le series
vdvDisplay_2	= 33003:1:0 //41006:1:0	// "

vdvCam_2		= 41007:1:0	// vaddio (use sony visca protocol)

vdvDebug		= 33099:1:0


// touch panels  
 dvTP1_RmA 		= 10001:1:0
 dvTP1_RmB 		= 10002:1:0
 
 dvTP_Routing_RmA	= 10001:2:0
 dvTP_Routing_RmB	= 10002:2:0

 dvTP_Volume_RmA	= 10001:3:0
 dvTP_Volume_RmB	= 10002:3:0
 
 dvTP_ATC_RmA		= 10001:4:0
 dvTP_ATC_RmB		= 10002:4:0

 dvTP_Setup_RmA		= 10001:5:0
 dvTP_Setup_RmB		= 10002:5:0

 dvTP_Bluray_RmA	= 10001:6:0
 dvTP_Bluray_RmB	= 10002:6:0
 
 dvTP_Codec_RmA		= 10001:7:0
 dvTP_Codec_RmB		= 10002:7:0
 
 dvTP_Cameras_RmA	= 10001:8:0
 dvTP_Cameras_RmB	= 10002:8:0

 dvTP_Tuner_RmA		= 10001:9:0	//
 dvTP_Tuner_RmB		= 10002:9:0	//


(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT

dev tpTPs1[]=
{
	dvTP1_RmA,
	dvTP1_RmB
}
dev tpRouting[]=
{
	dvTP_Routing_RmA,
	dvTP_Routing_RmB
}
dev tpVolume[]=
{
	dvTP_Volume_RmA,
	dvTP_Volume_RmB
}

dev tpATCs[]=
{
	dvTP_ATC_RmA,
	dvTP_ATC_RmB
}

dev tpBluray[]=
{
	dvTP_Bluray_RmA,
	dvTP_Bluray_RmB
}

dev tpSetup[]=
{
	dvTP_Setup_RmA,
	dvTP_Setup_RmB
}

dev tpCodecs[]=
{
	dvTP_Codec_RmA,
	dvTP_Codec_RmB
}
dev tpCameras[]=
{
	dvTP_Cameras_RmA,
	dvTP_Cameras_RmB
}

dev tpTuner[]=
{
	dvTP_Tuner_RmA,
	dvTP_Tuner_RmB
}

// more device arrays
dev dvaDecoders[]=
{
	dvDecoder_1,dvDecoder_2,dvDecoder_3,dvDecoder_4,
	dvDecoder_5,vdvDebug,dvDecoder_6,dvDecoder_7			// vdvDebug is used as a placeholder because RmB has no display #2
}             


dev dvaDisplays[]=
{
	dvDisplay_1,dvProj,dvDisplay_2
}

dev dvaIRs[]=
{
	dvIR_1,
	dvIR_2
}

dev dvaCodecs[]=
{
	dvCodec_1,
	dvCodec_2
}

dev vdvaCodecs[]=
{
	vdvCodec_1,
	vdvCodec_2
}

dev vdvaCameras[][]=
{
	{vdvCodec_1,vdvCam_2},
	{vdvCodec_2}
}

dev vdvaDestinations[][]=
{
	{vdvDisplay_1,vdvProj},
	{vdvDisplay_2}
}

dev vdvaVol_Ramps[][]=
{
	{vdvVolume_RmA,vdvATC_Rx_RmA,vdvVTC_Rx_RmA},
	{vdvVolume_RmB,vdvATC_Rx_RmB,vdvVTC_Rx_RmB}
}
dev vdvaPrivacy[]=
{
	vdvPrivacy_RmA,vdvPrivacy_RmB
}

integer nNum_of_Rms		= 2

RMA	= 1
RMB	= 2

indexContent	= 3	// the content destination is #3 for both rooms
indexAudio		= 4

ch_progvol	= 1
ch_atcrxvol = 2
ch_vtcrxvol = 3
ch_dialer	= 4
ch_controlstatus = 5
ch_privacy	= 6

tlFeedback		= 99
tlIP_Connect	= 100

long tlStartup_A		= 1
long tlStartup_B		= 2

long tlShutdown_A	= 3
long tlShutdown_B	= 4

long tlaStatups[]=
{
	tlStartup_A,
	tlStartup_B
}
long tlaShutdowns[]=
{
	tlShutdown_A,
	tlShutdown_B
}

/*	debugging	*/
long tlDecoder_Status_1		= 51
long tlDecoder_Status_2		= 52
long tlDecoder_Status_3		= 53
long tlDecoder_Status_4		= 54
long tlDecoder_Status_5		= 55
long tlDecoder_Status_6		= 56
long tlDecoder_Status_7		= 57

long tlaDecoders_Status[]=
{
	tlDecoder_Status_1,
	tlDecoder_Status_2,
	tlDecoder_Status_3,
	tlDecoder_Status_4,
	tlDecoder_Status_5,
	tlDecoder_Status_6,
	tlDecoder_Status_7
}                   
/*	end debug	*/

// relay channels
chScreen_Down		= 7
chScreen_Up			= 8
chLift_Proj_Down	= 5
chLift_Proj_Up		= 6
chLift_Cam_Down		= 3
chLift_Cam_Up		= 4

// codec channels
chContent_On	= 131
chContent_Off	= 130

// mixer channels
AUDIOPROC_LEVEL_UP     = 24    // Ramping:   Increment the audio processor level
AUDIOPROC_LEVEL_DN     = 25    // Ramping:   Decrement the audio processor level
AUDIOPROC_STATE        = 26    // Momentary: Cycle the audio processor state


(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

structure source_structure
{
	char sName[32]
	integer nIn			// since we're not using a traditional switch, this value represents the stream number
	char sIP_Encoder[15]		// used only for reference
	integer olFlag
}
structure dest_structure
{
	char sName[32]
	//integer nOut		// 
	char sIP_Decoder[15]
	integer olFlag
	char cmdQ_dest[100]
	dev dvDecoder
	integer flAttempting
}

structure mixer_structure
{
	char sInstanceID[32]
	char sLevel_Attribute[32]
}

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE


ip_address_struct ipDisplays[3]		// used to carry the ip's of the decoders (but this use is for their 232 ports, not the video feed)

ip_address_struct ipMixer

ip_address_struct ipAudio_Decoders[3]

ip_address_struct ipDecoders[8]		// used as an alternate way to connect to decoders

source_structure strSource[nNum_of_Rms][6]
dest_structure strDest[nNum_of_Rms][4]

mixer_structure strMixer[nNum_of_Rms][6]

// timeline variables
long lFeedback_Times[]=
{
	300
}

long lIP_Connect_Times[]=
{
	300
}

long lStartup_Times[]=
{
	1,3000
}
long lShutdown_Times[]=
{
	1,3000
}

/*	debugging	*/
long lDecoder_Status_Times[]=
{
	1000
}
integer naDecoder_Status[7]	// 0 for ok, 1 for not ok
/*	end debug	*/


// GP variables

integer nLoop_fb

// system on/off
volatile integer flSystem_Power[nNum_of_Rms]
volatile integer btSystem[]=
{
	51,52
}

volatile integer nPage[nNum_of_Rms]
volatile integer btPage[]=
{
	3,4,5	// presentation, vtc, atc
}

volatile integer nSource[nNum_of_Rms]
volatile integer btSource[]=
{
	101,102,103,104,105,106,107,108,109
}
volatile integer btDest[]=
{
	121,122,123,124
}

volatile integer nContent[nNum_of_Rms]
volatile integer btContent[]=
{
	301,302,303,304,305,306
}
integer nCam_Sel[nNum_of_Rms]={1,1}
integer btCamera[]=
{
	311,312
}

// to be used on the setup port
integer btDisplay_Setup[]=		// used for display controls
{
	1,		// display #1 power on
	2,		// display #1 power off
	3,		// display #2 power on		(not used in rmb)
	4,		// display #2 power off		"
	5,		// screen up				"
	6,		// screen down				"
	7,		// proj lift up				"
	8		// proj lift down			"
}
// camera lift up/down, same port btn numbers 301,302

// volume controls
integer btProg_Vol[]=
{
	11,12,13
}
integer btATC_Rx_Vol[]=
{
	21,22,23
}
integer btVTC_Rx_Vol[]=
{
	31,32,33
}
integer btOverflow_Vol[]=
{
	51,52,53
}

integer btnPrivacy	= 199
 
// track the state of the lift
integer flLift_Proj

// SVSi 
integer nCMD_Q_Loop
integer nPort_SVSi	= 50002	// this tcp/ip port is used to issue commands to the encoder or decoder; to access the attached serial port, open a persistent session with port 50004

// camera variable
integer nCam2_Reboot=1

(***********************************************************)
(*        SUBROUTINE/FUNCTION DEFINITIONS GO BELOW         *)
(***********************************************************)
(* EXAMPLE: DEFINE_FUNCTION <RETURN_TYPE> <NAME> (<PARAMETERS>) *)
(* EXAMPLE: DEFINE_CALL '<NAME>' (<PARAMETERS>) *)

// needs work
define_function integer fnLevel_Adjust(integer nLevel,sinteger nMax,sinteger nMin)
{
	// takes a level (nLevel) which is a range of 0 to 255, representing a dB range of -100dB to 12dB
	// it adjusts the level to match
	local_var integer nAdjusted	//
	
}

define_function fnLift_Camera(integer nParam)
{
	if(nParam)		// lift down
	{
		//pulse[dvRelays,chLift_Cam_Down]
		pulse[vdvCam_2,27]	// camera on
	}
	else
	{
		//pulse[dvRelays,chLift_Cam_Up]
		pulse[vdvCam_2,28]	// camera off
	}
}
define_function fnLift_Proj(integer nParam)
{
	flLift_Proj=nParam
	if(flLift_Proj)
		pulse[dvRelays,chLift_Proj_Down]
	else
		pulse[dvRelays,chLift_Proj_Up]
}

define_function fnDo_Route(integer source,integer destination, integer room)
{
	local_var char sCommand[5]
	sCommand=""
	
	if(destination==indexAudio)	// audio
		sCommand="'seta:'"
	else
		sCommand="'set:'"
		
	send_string 0,"'do_route ',itoa(source),',',itoa(destination),',',itoa(room),';'"		// debug
	if(source)
	{
		send_command tpRouting[room],"'TEXT',itoa(btDest[destination]),'-',strSource[room][source].sName"
		strDest[room][destination].cmdQ_dest="strDest[room][destination].cmdQ_dest,sCommand,itoa(strSource[room][source].nIn),$0D,$0A"
	}
	else
	{
		send_command tpRouting[room],"'TEXT',itoa(btDest[destination]),'-Blank'"
		strDest[room][destination].cmdQ_dest="strDest[room][destination].cmdQ_dest,sCommand,'99',$0D,$0A"	// we use stream #99 as a blank stream.  make sure no encoders are added to this system with sream #99 or the deroute function will stop working
	}

}
(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

ipMixer.IPADDRESS	= '192.168.1.31'

ipAudio_Decoders[1].IPADDRESS	= '192.168.1.41'	// 510
ipAudio_Decoders[2].IPADDRESS	= '192.168.1.42'	// 511
ipAudio_Decoders[3].IPADDRESS	= '192.168.1.43'	// 500

ipDecoders[1].IPADDRESS		= '192.168.1.51'
ipDecoders[2].IPADDRESS		= '192.168.1.52'
ipDecoders[3].IPADDRESS		= '192.168.1.53'
ipDecoders[4].IPADDRESS		= '192.168.1.41'
ipDecoders[5].IPADDRESS		= '192.168.1.54'
ipDecoders[6].IPADDRESS		= '192.168.1.55'
ipDecoders[7].IPADDRESS		= '192.168.1.42'


ipDisplays[1].IPADDRESS	= '192.168.1.51'
ipDisplays[2].IPADDRESS	= '192.168.1.52'
ipDisplays[3].IPADDRESS	= '192.168.1.54'


// mixer instance ids
strMixer[rma][ch_progvol].sInstanceID	= 'pavolrma'//'pgmvolrma'
strMixer[rma][ch_atcrxvol].sInstanceID	= 'atcvolrma'
strMixer[rma][ch_vtcrxvol].sInstanceID	= 'vtcvolrma'
strMixer[rma][ch_dialer].sInstanceID	= 'dialerrma'
strMixer[rma][ch_controlstatus].sInstanceID = 'voipCSrma'
strMixer[rma][ch_privacy].sInstanceID	= 'privacyrma'

strMixer[RmB][ch_progvol].sInstanceID	= 'pavolrmb'//'pgmvolrmb'
strMixer[RmB][ch_atcrxvol].sInstanceID	= 'atcvolrmb'
strMixer[RmB][ch_vtcrxvol].sInstanceID	= 'vtcvolrmb'
strMixer[RmB][ch_dialer].sInstanceID	= 'dialerrmb'
strMixer[RMB][ch_controlstatus].sInstanceID = 'voipCSrmb'
strMixer[RmB][ch_privacy].sInstanceID	= 'privacyrmb'

/****************************************************/
// routing table
// sources
// rma
strSource[rma][1].sName='Laptop #1'
strSource[rma][2].sName='Laptop #2'
strSource[rma][3].sName='Credenza PC'
strSource[rma][4].sName='Bluray'
strSource[rma][5].sName='Codec'
strSource[rma][6].sName='Tuner'

// stream #
strSource[rma][1].nIn=1	
strSource[rma][2].nIn=2	
strSource[rma][3].nIn=3	
strSource[rma][4].nIn=4	
strSource[rma][5].nIn=5
strSource[rma][6].nIn=10


strSource[rma][1].sIP_Encoder='192.168.1.61'
strSource[rma][2].sIP_Encoder='192.168.1.62'
strSource[rma][3].sIP_Encoder='192.168.1.63'
strSource[rma][4].sIP_Encoder='192.168.1.64'
strSource[rma][5].sIP_Encoder='192.168.1.65'
strSource[rma][6].sIP_Encoder='192.168.1.70'

// rmb
strSource[rmb][1].sName='Laptop'
strSource[rmb][2].sName='n/a'	// not used
strSource[rmb][3].sName='Credenza PC'
strSource[rmb][4].sName='Bluray'
strSource[rmb][5].sName='Codec'
strSource[rmb][6].sName='Tuner'

// stream #
strSource[rmb][1].nIn=6
//strSource[rmb][2].nIn=999	
strSource[rmb][3].nIn=7
strSource[rmb][4].nIn=8	
strSource[rmb][5].nIn=9
strSource[rmb][6].nIn=10

strSource[rmb][1].sIP_Encoder='192.168.1.66'
strSource[rmb][2].sIP_Encoder=''         
strSource[rmb][3].sIP_Encoder='192.168.1.67'
strSource[rmb][4].sIP_Encoder='192.168.1.68'
strSource[rmb][5].sIP_Encoder='192.168.1.69'
strSource[rmb][6].sIP_Encoder='192.168.1.70'

// destinations
// rma
strDest[rma][1].sName	= 'LCD'
strDest[rma][2].sName	= 'Projector'
strDest[rma][3].sName	= 'Content'
strDest[rma][4].sName	= 'Audio'

strDest[rma][1].sIP_Decoder	= '192.168.1.51'
strDest[rma][2].sIP_Decoder	= '192.168.1.52'
strDest[rma][3].sIP_Decoder	= '192.168.1.53'
strDest[rma][4].sIP_Decoder	= '192.168.1.41'

strDest[rma][1].dvDecoder	= dvDecoder_1		// lcd
strDest[rma][2].dvDecoder	= dvDecoder_2		// proj
strDest[rma][3].dvDecoder	= dvDecoder_3		// content
strDest[rma][4].dvDecoder	= dvDecoder_4		// audio

// rmb
strDest[rmb][1].sName	= 'LCD'
strDest[rmb][2].sName	= 'n/a'	// not used
strDest[rmb][3].sName	= 'Content'
strDest[rmb][4].sName	= 'Audio'

strDest[rmb][1].sIP_Decoder	= '192.168.1.54'
strDest[rmb][2].sIP_Decoder	= ''
strDest[rmb][3].sIP_Decoder	= '192.168.1.55'
strDest[rmb][4].sIP_Decoder	= '192.168.1.42'

strDest[rmb][1].dvDecoder	= dvDecoder_5		// lcd
//strDest[rmb][2].dvDecoder	= 
strDest[rmb][3].dvDecoder	= dvDecoder_6		// content
strDest[rmb][4].dvDecoder	= dvDecoder_7		// audio

/****************************************************/


timeline_create(tlFeedback,lFeedback_Times,1,timeline_absolute,timeline_repeat)

timeline_create(tlaShutdowns[rma],lShutdown_Times,2,timeline_absolute,timeline_once)
timeline_create(tlaShutdowns[rmb],lShutdown_Times,2,timeline_absolute,timeline_once)

timeline_create(tlIP_Connect,lIP_Connect_Times,1,timeline_absolute,timeline_repeat)


//define_module 'Hitachi_CP-WU8450_Comm_dr1_0_0'	proj_mod_1(vdvProj,dvProj)

define_module 'Cisco_C40_Comm_dr1_0_0'	codec_mod_1(vdvCodec_1,dvCodec_1)
define_module 'Cisco_C40_Comm_dr1_0_0'	codec_mod_2(vdvCodec_2,dvCodec_2)
define_module 'BEEF Proj'		proj_mod_1(vdvProj,dvProj)
define_module 'SHARP IP MODULE'		lcd_mod_1(vdvDisplay_1,dvDisplay_1)
define_module 'SHARP IP MODULE'		lcd_mod_2(vdvDisplay_2,dvDisplay_2)

//define_module 'IP DISPLAY CONTROL'	lcd_mod_1(vdvDisplay_1,dvDisplay_1)

//define_module
//define_module 'Sharp_LC70LE650U_Comm_dr1_0_0'	lcd_mod_1(vdvDisplay_1,dvDisplay_1)
//define_module 'Sharp_LC70LE650U_Comm_dr1_0_0'	lcd_mod_2(vdvDisplay_2,dvDisplay_2)
define_module 'Sony_EVID100_Comm_dr1_0_0'		cam_mod_1(vdvCam_2,dvCam_2)
define_module 'Biamp_Tesira_dr1_0_0'		mixer_mod_1(vdvMixer,dvMixer)
define_module 'Biamp VoIP Dialer modified - jol'	atc_mod_1(vdvDialer_RmA,dvTP_ATC_RmA)
define_module 'Biamp VoIP Dialer modified - jol'	atc_mod_2(vdvDialer_rmb,dvTP_ATC_rmb)

(***********************************************************)
(*                THE EVENTS GO BELOW                      *)
(***********************************************************)



DEFINE_EVENT	// button_events

button_event[tpTPs1,btPage]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		iTP=get_last(tpTPs1)
		iBTN=get_last(btPage)
		
		nPage[iTP]=iBTN
		switch(iBTN)
		{
			case 2:		// vtc
			{
				pulse[vdvaDestinations[itp][1],27]	// turn on lcd
				wait 70
					pulse[vdvaDestinations[itp][1],196]	// set input hdmi		
				send_command vdvaCodecs[iTP],"'passthru-xcommand standby deactivate'"
				// autoroute vtc
				fnDo_Route(5,1,iTP)
				fnDo_Route(0,indexAudio,iTP)	// clear program audio
				if(itp==rma)
				{
					fnDo_Route(5,2,rma)		// route to proj 					
					fnLift_Camera(1)
				}
			}
			default:	// page flips themselves are in the panel file
			{
				
			}
		}
	}
}
timeline_event[tlFeedback]
{
	nLoop_fb=length_array(btPage)
	while(nLoop_fb)
	{
		[tpTPs1[rma],btPage[nLoop_fb]]=(nPage[rma]=nLoop_fb)
		[tpTPs1[rmb],btPage[nLoop_fb]]=(nPage[rmb]=nLoop_fb)
		nLoop_fb--
	}
}

button_event[tpRouting,btSource]
{
	push:
	{
		local_var integer iTP
		iTP=get_last(tpRouting)
		nSource[iTP]=get_last(btSource)
		
		if(iTP==rmb)	// one-touch routing
		{
			pulse[vdvaDestinations[itp][1],27]	// turn on lcd,
			wait 70
				pulse[vdvaDestinations[itp][1],196]	// set input hdmi
			fnDo_Route(nSource[rmb],1,rmb)
			fnDo_Route(nSource[rmb],indexAudio,rmb)	// route audio
		}
	}
}
timeline_event[tlFeedback]
{
	nLoop_fb=length_array(btSource)
	while(nLoop_fb)
	{
		[tpRouting[rma],btSource[nLoop_fb]]=(nSource[rma]=nLoop_fb)
		[tpRouting[rmb],btSource[nLoop_fb]]=(nSource[rmb]=nLoop_fb)
		nLoop_fb--
	}
}
button_event[tpRouting,btDest] 
{
	push: 
	{
		local_var integer iBTN
		local_var integer iTP
		
		iTP=get_last(tpRouting)
		iBTN=get_last(btDest)
		
		to[button.input]
		
		pulse[vdvaDestinations[get_last(tpRouting)][iBTN],27]	// turn display on
		wait 70
			pulse[vdvaDestinations[get_last(tpRouting)][iBTN],196]	// set input hdmi
		if(iBTN>1)
		{
			pulse[dvRelays,chScreen_Down]
			fnLift_Proj(1)
			
			// reinforce projector power on
			wait 50
			{
				pulse[vdvProj,27]	// turn display on
			}
		}
		fnDo_Route(nSource[iTP],iBTN,iTP)
		fnDo_Route(nSource[iTP],indexAudio,iTP)	// route audio
	}
}



button_event[tpRouting,311]
button_event[tpRouting,312]
BUTTON_EVENT[tpRouting,btContent]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpRouting)
		iBTN=get_last(btContent)
		
		switch(button.input.channel)
		{
			case 311:
			case 312:
			{
				nCam_Sel[itp]=button.input.channel-310	// for camera control
				nContent[itp]=button.input.channel-210	// button #311 => index #101, 312=>102
				pulse[vdvaCodecs[iTP],chContent_Off]
				send_command vdvaCodecs,"'passthru-xConfiguration  Video MainVideoSource: ',itoa(nCam_Sel[iTP])"
				send_command tpTPs1[iTP],"'text8-Controlling: Camera #',itoa(nCam_Sel[itp])"
				fnDo_Route(0,indexAudio,iTP)
			}
			default:
			{
				pulse[vdvaCodecs[iTP],chContent_On]
				nContent[iTP]=ibtn
				fnDo_Route(nContent[itp],indexContent,iTP)		// content is destination 3 in both rooms
				fnDo_Route(nContent[itp],indexAudio,iTP)
			}
		}
	}
}
TIMELINE_EVENT[tlFeedback]
{
	nLoop_fb=length_array(btContent)
	while(nLoop_fb)
	{
		// content source buttons
		[tpRouting[rma],btContent[nLoop_fb]]=(nContent[rma]=nLoop_fb)
		[tpRouting[RmB],btContent[nLoop_fb]]=(nContent[RmB]=nLoop_fb)
		
		// camera #1
		[tpRouting[rma],311]=(nContent[rma]=101)
		[tpRouting[RmB],311]=(nContent[RmB]=101)
		
		// camera #2
		[tpRouting[rma],312]=(nContent[rma]=102)
		
		nLoop_fb--
	}
}

button_event[tpTPs1,btSystem]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		iTP=get_last(tpTPs1)
		iBTN=get_last(btSystem)
		
		switch(iBTN)
		{
			case 1:
			{
				if(!timeline_active(tlaShutdowns[iTP]))
					timeline_create(tlaStatups[itp],lShutdown_Times,2,timeline_absolute,timeline_once)
			}
			case 2:
			{
				if(!timeline_active(tlaStatups[iTP]))
					timeline_create(tlaShutdowns[itp],lShutdown_Times,2,timeline_absolute,timeline_once)
			}
		}
	}
}

BUTTON_EVENT[tpSetup,btDisplay_Setup]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		iTP=get_last(tpSetup)
		iBTN=get_last(btDisplay_Setup)
		
		to[button.input]
		
		switch(iBTN)
		{
			case 1:		// display 1 on
			{
				pulse[vdvaDestinations[iTP][1],27]
			}
			case 2:		// display 1 off
			{
				pulse[vdvaDestinations[iTP][1],28]
			}
			case 3:		// display 2 on
			{
				pulse[vdvaDestinations[iTP][2],27]
				if(itp=rma)
					fnLift_Proj(1)
			}
			case 4:
			{
				pulse[vdvaDestinations[iTP][2],28]
			}
			case 5:
			{
				pulse[dvRelays,chScreen_Up]
			}
			case 6:
			{
				pulse[dvRelays,chScreen_Down]
			}
			case 7:
			{
				fnLift_Proj(0)
				pulse[vdvProj,28]		// turn projector off automatically if lift is retracted
			}
			case 8:
			{
				fnLift_Proj(1)
			}
		}
	}
}
TIMELINE_EVENT[tlFeedback]
{
	[tpSetup[rma],btDisplay_Setup[1]]=[vdvDisplay_1,255]	// lcd power on fb
	[tpSetup[rma],btDisplay_Setup[2]]=![vdvDisplay_1,255]	// lcd power on fb
	
	[tpSetup[rma],btDisplay_Setup[3]]=[vdvProj,1]	// power on fb
	[tpSetup[rma],btDisplay_Setup[4]]=[vdvProj,4]	// power on fb
	
	[tpSetup[rmb],btDisplay_Setup[1]]=[vdvDisplay_2,255]	// lcd power on fb
	[tpSetup[rmb],btDisplay_Setup[2]]=![vdvDisplay_2,255]	// lcd power on fb
}
button_event[dvTP_Setup_RmA,301]	// camera lift down
button_event[dvTP_Setup_RmA,302]	// camera lift up
{
	push:
	{
		to[button.input]
		switch(button.input.channel)
		{
			case 301:{fnLift_Camera(0)}
			case 302:{fnLift_Camera(1)}
		}
	}
}

// device controls


button_event[tpBluray,0]
{
	push:
	{
		to[button.input]
		to[dvaIRs[get_last(tpBluray)],button.input.channel]
	}
}
button_event[tpTuner,0]
{
	push:
	{
		to[button.input]
		to[dvTuner,button.input.channel]
	}
}

button_event[tpCodecs,0]
{
	push:
	{
		local_var integer iTP
		iTP=get_last(tpCodecs)
		
		//pulse[vdvaCodecS[iTP],button.input.channel]
		
		switch(button.input.channel)	// button function
		{
			case 93:
			{
				// empty
			}
			default:
			{
				pulse[vdvaCodecS[iTP],button.input.channel]
			}
		}
		
		switch(button.input.channel)		// allows for exceptions in momentary feedback
		{
			case 93:		// pound has its own event
			case 145:		// privacy is latching feedback
			case 302:		// presentation is latching feedback
			{
				// empty
			}
			CASE 601:	// DIRECTORY
			{
				to[button.input]
				send_command vdvacodecs[itp],"'passthru-xcommand key click key:phonebook'"
			}
			case 602:	// selfview
			{
				to[button.input]
				send_command vdvacodecs[itp],"'passthru-xcommand key click key:layout'"
			}
			default:
			{
				to[button.input]
			}
		}
	}
}

BUTTON_EVENT[tpCodecs,93]	// THE # KEY ON THE VTC REMOTE
{
    PUSH:
    {
	TO[BUTTON.INPUT]
	SEND_COMMAND vdvaCodecs[get_last(tpCodecs)],"'passthru-xcommand key press key:square',$0D"
    }
    RELEASE:
    {
	SEND_COMMAND vdvaCodecs[get_last(tpCodecs)],"'passthru-xcommand key release key:square',$0D"
    }
}
timeline_event[tlFeedback]
{
	// privacy feedback
	[tpCodecs[rma],145]=[vdvaCodecs[rma],146]
	[tpCodecs[rmb],145]=[vdvaCodecs[rmb],146]
	
	// content presentation feedback
	[tpCodecs[rma],302]=[vdvaCodecs[rma],301]
	[tpCodecs[rmb],302]=[vdvaCodecs[rmb],301]
}

button_event[tpCameras,0]
{
	push: 
	{
		local_var integer iTP
		iTP=get_last(tpCameras)
		
		to[button.input]
		
		switch(button.input.channel)
		{
			case 132:
			case 133:
			case 134:
			case 135:
			
			case 158:
			case 159:
			{
				to[button.input]
				to[vdvaCameras[iTP][nCam_Sel[iTP]],button.input.channel]
			}
		}
	}
	hold[40]:
	{
		local_var integer iTP
		iTP=get_last(tpCameras)
		
		send_command tpCameras[iTP],"'ADBEEP'"
		switch(button.input.channel)
		{
			case 501:
			case 502:
			case 503:
			case 504:
			case 505:
			case 506:
			{
				send_command vdvaCameras[iTP][nCam_Sel[iTP]],"'CAMERAPRESETSAVE-',itoa(button.input.channel-500)"
			}
		}
	}
	release: 
	{
		local_var integer iTP
		iTP=get_last(tpCameras)
		
		switch(button.input.channel)
		{
			case 501:
			case 502:
			case 503:
			case 504:
			case 505:
			case 506:
			{
				send_command vdvaCameras[iTP][nCam_Sel[iTP]],"'CAMERAPRESET-',itoa(button.input.channel-500)"
			}
		}
	}
}

button_event[tpVolume,btProg_Vol]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btProg_Vol)
		
		switch(iBTN)
		{
			case 1:
			{
				to[button.input]
				to[vdvaVol_Ramps[iTP][ch_progvol],audioproc_level_up]
			}
			case 2:
			{
				to[button.input]
				to[vdvaVol_Ramps[iTP][ch_progvol],audioproc_level_dn]
			}
			case 3:
			{
				pulse[vdvaVol_Ramps[iTP][ch_progvol],audioproc_state]
			}
		}
	}
	hold[1,repeat]:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btProg_Vol)
		
		to[button.input]
		
		switch(iBTN)
		{
			case 1:
			{
				to[vdvaVol_Ramps[iTP][ch_progvol],audioproc_level_up]
			}
			case 2:
			{
				to[vdvaVol_Ramps[iTP][ch_progvol],audioproc_level_dn]
			}
		}
	}
}

button_event[tpVolume,btATC_Rx_Vol]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btATC_Rx_Vol)
		
		switch(iBTN)
		{
			case 1:
			{
				to[button.input]
				to[vdvaVol_Ramps[iTP][ch_atcrxvol],audioproc_level_up]
			}
			case 2:
			{
				to[button.input]
				to[vdvaVol_Ramps[iTP][ch_atcrxvol],audioproc_level_dn]
			}
			case 3:
			{
				pulse[vdvaVol_Ramps[iTP][ch_atcrxvol],audioproc_state]
			}
		}
	}
	hold[1,repeat]:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btATC_Rx_Vol)
		
		to[button.input]
		
		switch(iBTN)
		{
			case 1:
			{
				to[vdvaVol_Ramps[iTP][ch_atcrxvol],audioproc_level_up]
			}
			case 2:
			{
				to[vdvaVol_Ramps[iTP][ch_atcrxvol],audioproc_level_dn]
			}
		}
	}
}

button_event[tpVolume,btVTC_Rx_Vol]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btVTC_Rx_Vol)
		
		switch(iBTN)
		{
			case 1:
			{
				to[button.input]
				to[vdvaVol_Ramps[iTP][ch_VTCrxvol],audioproc_level_up]
			}
			case 2:
			{
				to[button.input]
				to[vdvaVol_Ramps[iTP][ch_VTCrxvol],audioproc_level_dn]
			}
			case 3:
			{
				pulse[vdvaVol_Ramps[iTP][ch_VTCrxvol],audioproc_state]
			}
		}
	}
	hold[1,repeat]:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btVTC_Rx_Vol)
		
		to[button.input]
		
		switch(iBTN)
		{
			case 1:
			{
				to[vdvaVol_Ramps[iTP][ch_VTCrxvol],audioproc_level_up]
			}
			case 2:
			{
				to[vdvaVol_Ramps[iTP][ch_VTCrxvol],audioproc_level_dn]
			}
		}
	}
}
button_event[tpVolume,btOverflow_Vol]
{
	push:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btOverflow_Vol)
		
		switch(iBTN)
		{
			case 1:
			{
				to[button.input]
				to[vdvOverflow,audioproc_level_up]
			}
			case 2:
			{
				to[button.input]
				to[vdvOverflow,audioproc_level_dn]
			}
			case 3:
			{
				pulse[vdvOverflow,audioproc_state]
			}
		}
	}
	hold[1,repeat]:
	{
		local_var integer iTP
		local_var integer iBTN
		
		iTP=get_last(tpVolume)
		iBTN=get_last(btOverflow_Vol)
		
		to[button.input]
		
		switch(iBTN)
		{
			case 1:
			{
				to[vdvOverflow,audioproc_level_up]
			}
			case 2:
			{
				to[vdvOverflow,audioproc_level_dn]
			}
		}
	}
}
level_event[vdvOverflow,1]
{
	send_level tpVolume[rma],51,level.value
}

timeline_event[tlFeedback]	// mute buttons
{
	[tpVolume[rma],btProg_Vol[3]]=[vdvaVol_Ramps[rma][ch_progvol],199]
	[tpVolume[rma],btATC_Rx_Vol[3]]=[vdvaVol_Ramps[rma][ch_atcrxvol],199]
	[tpVolume[rma],btVTC_Rx_Vol[3]]=[vdvaVol_Ramps[rma][ch_vtcrxvol],199]
	
	[tpVolume[rmb],btProg_Vol[3]]=[vdvaVol_Ramps[rmb][ch_progvol],199]
	[tpVolume[rmb],btATC_Rx_Vol[3]]=[vdvaVol_Ramps[rmb][ch_atcrxvol],199]
	[tpVolume[rmb],btVTC_Rx_Vol[3]]=[vdvaVol_Ramps[rmb][ch_vtcrxvol],199]
}


LEVEL_EVENT[vdvaVol_Ramps[rma], 1]
{
	local_var integer iRAMP
	iRAMP=get_last(vdvaVol_Ramps[rma])
	
	SEND_LEVEL tpVolume[rma], (iRAMP*10+1), LEVEL.VALUE
}
LEVEL_EVENT[vdvaVol_Ramps[rmb], 1]
{
	local_var integer iRAMP
	iRAMP=get_last(vdvaVol_Ramps[rmb])
	
	SEND_LEVEL tpVolume[rmb], (iRAMP*10+1), LEVEL.VALUE
}

// privacy mic mute
button_event[tpVolume,btnPrivacy]
{
	push:
	{
		pulse[vdvaPrivacy[get_last(tpVolume)],audioproc_state]
	}
}
timeline_event[tlFeedback]
{
	[tpVolume[rma],btNPrivacy]=[vdvaPrivacy[rma],199]
	[tpVolume[RmB],btNPrivacy]=[vdvaPrivacy[RmB],199]
}


DEFINE_EVENT	// timeline_events  (non-feedback) 

timeline_event[tlStartup_A]
timeline_event[tlStartup_B]
{
	local_var integer nRM
	nRM=timeline.id-tlStartup_A+1
	
	send_string 0,"'startup ',itoa(nRM)"	// debug
	
	switch(nRM)
	{
		// room A startup 
		case rma:
		{
			switch(timeline.sequence)
			{
				case 1:	// begin the timeline
				{
					
				}
				case 2:	// conclude the timeline
				{
					
				}
			}
		}
		// room B startup 
		case rmb:
		{
			switch(timeline.sequence)
			{
				case 1:	// begin the timeline
				{
					
				}
				case 2:	// conclude the timeline
				{
					
				}
			}
		}
	}
}
timeline_event[tlShutdown_A]
timeline_event[tlShutdown_B]
{
	local_var integer nRM
	nRM=timeline.id-tlShutdown_A+1
	
	send_string 0,"'shutdown ',itoa(nRM)"	// debug
	
	pulse[vdvaCodecs[nRM],28]	// put the codec to sleep
	
	off[nSource[nRM]]
	
	switch(nRM)
	{
		// room A Shutdown 
		case rma:
		{
			switch(timeline.sequence)
			{
				case 1:	// begin the timeline
				{
					send_command vdvaCodecs[nRM],"'passthru-xcommand standby deactivate'"
					send_command vdvaCodecs[nRM],"'passthru-xcommand standby activate'"
					// displays off
					pulse[vdvDisplay_1,28]
					pulse[vdvProj,28]		
					fnLift_Camera(0)
					pulse[dvRelays,chScreen_Up]
					fnLift_Proj(0)
					// deroute
					fnDo_Route(0,1,rma)
					fnDo_Route(0,2,rma)
					fnDo_Route(0,3,rma)
					fnDo_Route(0,4,rma)
					send_command tpTPs1[nRM],"'@PPK-bluray'"
				}               
				case 2:	// conclude the timeline
				{
					off[nPage[rma]]
					send_command tpTPs1[rma],"'PAGE-LOGO'"
				}
			}
		}
		// room B Shutdown 
		case rmb:
		{
			switch(timeline.sequence)
			{
				case 1:	// begin the timeline
				{
					pulse[vdvDisplay_2,28]
					// deroute
					fnDo_Route(0,1,rmb)
					fnDo_Route(0,2,rmb)
					fnDo_Route(0,3,rmb)
					fnDo_Route(0,4,rmb)
				}
				case 2:	// conclude the timeline
				{
					off[nPage[rmb]]
					send_command tpTPs1[rmb],"'PAGE-LOGO'"
				}
			}
		}
	}
}
timeline_event[tlIP_Connect]
{
	local_var integer nLoop_IP_Connect
	nLoop_IP_Connect=4
	while(nLoop_IP_Connect)
	{
		if(!strDest[rma][nLoop_IP_Connect].olFlag)
			ip_client_open(strDest[rma][nLoop_IP_Connect].dvDecoder.PORT,strDest[rma][nLoop_IP_Connect].sIP_Decoder,nPort_SVSi,1)
		if(!strDest[rmb][nLoop_IP_Connect].olFlag)
			ip_client_open(strDest[rmb][nLoop_IP_Connect].dvDecoder.PORT,strDest[rmb][nLoop_IP_Connect].sIP_Decoder,nPort_SVSi,1)
		nLoop_IP_Connect--
	}
}
/*	debugging ip sessions with SVSi boxes	*/
timeline_event[tlDecoder_Status_1]
timeline_event[tlDecoder_Status_2]
timeline_event[tlDecoder_Status_3]
timeline_event[tlDecoder_Status_4]
timeline_event[tlDecoder_Status_5]
timeline_event[tlDecoder_Status_6]
timeline_event[tlDecoder_Status_7]
{
	local_var integer iDev
	iDev=timeline.id-tlDecoder_Status_1+1
	switch(iDev)
	{
		case 1:{send_command vdvDebug,"'reboot1'"}		// if the lcd control port goes stupid
		case 5:{send_command vdvDebug,"'reboot5'"}
		case 6:{}
		//default:{send_command vdvDebug,"'reboot',itoa(iDev)"}
	}
}
/*	 end debug	*/


DEFINE_EVENT	// data_events

data_event[tpTPs1]
{
	online:
	{
		wait 20
		{
			send_command tpTPs1[get_last(tpTPs1)],"'@PPX'"
			send_command tpTPs1[get_last(tpTPs1)],"'PAGE-LOGO'"
			
			//send_command tpTPs1[get_last(tpTPs1)],"'@PPN-HEADER - MAIN'"
//			send_command tpTPs1[get_last(tpTPs1)],"'@PPN-FOOTER'"
		}
	}
}
channel_event[vdvaCodecs,252]		// configure codecs here
{
	on:
	{
		wait 20
		{
			send_command channel.device,"'passthru-xconfiguration serialport loginrequired: off'"
			send_command channel.device,"'passthru-xconfiguration audio input microphone 1 type: line'"
			send_command channel.device,"'passthru-xconfiguration audio input microphone 2 type: line'"
			send_command channel.device,"'property-User_Name,admin'"
			send_command channel.device,"'property-password,'"
		}
	}
	
}
data_event[dvaCodecs]
{
	string:
	{
		local_var integer iDev
		iDev=get_last(dvaCodecs)
		//if(find_string(data.text,'login:',1))
//			send_command vdvaCodecs[iDev],"'PASSTHRU-admin'"
//		if(find_string(data.text,'password:',1))
//			send_command vdvaCodecs[iDev],"'PASSTHRU-'"

		send_string 0,"'codec string,',itoa(iDev),'[',data.text,']'"
		
		if(find_string(data.text,'Login:',1)||find_string(data.text,'login:',1))
		{
			send_string 0,"'found login prompt for device ',itoa(get_last(dvaCodecs))"
			send_string data.device,"'admin',$0D,$0A"
		}
		if(find_string(data.text,'Password:',1)||find_string(data.text,'password:',1))
		{
			send_string 0,"'found password prompt for device ',itoa(get_last(dvaCodecs))"
			send_string data.device,"'',$0D,$0A"
		}	
		//xconfiguration serialport loginrequired: off
	}
}

// for debugging
define_variable
integer nDebug1=1
DEFINE_EVENT
data_event[dvCodec_2]
{
	string:
	{
		if(nDebug1)
			send_string 0,"'codec string[',data.text,']'"
	}
}

data_event[vdvDisplay_1]
{
	online:
	{
		send_command vdvDisplay_1,"'PROPERTY-IP_Address,',ipDisplays[1].IPADDRESS"
		send_command vdvDisplay_1,"'PROPERTY-Port,50004'"
		send_command vdvDisplay_1,"'REINIT'"
	}
}
data_event[vdvDisplay_2]
{
	online:
	{
		send_command vdvDisplay_2,"'PROPERTY-IP_Address,',ipDisplays[3].IPADDRESS"
		send_command vdvDisplay_2,"'PROPERTY-Port,50004'"
		send_command vdvDisplay_2,"'REINIT'"
	}
}
data_event[vdvProj]
{
	online:
	{
		send_command vdvproj,"'CONTROL-IP'"
		send_command vdvproj,"'IPADDRESS-',strDest[rma][2].sIP_Decoder"
	}
}

data_event[tpVolume]		// set bargraph max and min
{
	online:
	{
		send_command data.device,"'^GLL-11,0'"
		send_command data.device,"'^GLL-21,0'"
		send_command data.device,"'^GLL-31,0'"
		
		send_command data.device,"'^GLH-11,255'"
		send_command data.device,"'^GLH-21,255'"
		send_command data.device,"'^GLH-31,255'"
	}
}


data_event[vdvCam_2]
{
	online:
	{
		if(nCam2_Reboot)
		{
			send_level vdvCam_2,18,200		// zoom speed
			send_level vdvCam_2,29,200		// pan speed
			send_level vdvCam_2,30,200		// tilt speed
			wait 20
			{
				off[nCam2_Reboot]
				send_command vdvCam_2,"'REINIT'"
				send_level vdvCam_2,18,200		// zoom speed
				send_level vdvCam_2,29,200		// pan speed
				send_level vdvCam_2,30,200		// tilt speed
			}
		}	
		else
		{
			send_level vdvCam_2,18,200		// zoom speed
			send_level vdvCam_2,29,200		// pan speed
			send_level vdvCam_2,30,200		// tilt speed
		}
	}
}

data_event[vdvMixer] 
{
	online: 
	{
		SEND_COMMAND vdvMixer,"'PROPERTY-IP_Address,',ipMixer.IPADDRESS"
		//SEND_COMMAND vdvMixer,'PROPERTY-Baud_Rate,38400'
		
		//send_command vdvMixer,"'AUDIOPROCADD-',itoa(vdvOverflow.PORT),',STATE[overflow|mute|1|0]'"
		send_command vdvMixer,"'AUDIOPROCADD-',itoa(vdvOverflow.PORT),',LEVELSTATE[overflow|level|mute|1|0|3]'"
		
		
		// rma
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvaVol_Ramps[RMA][ch_progvol].PORT),',LEVELSTATE[',strMixer[rma][ch_progvol].sInstanceID,'|level|mute|1|0|3]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvaVol_Ramps[RMA][ch_atcrxvol].PORT),',LEVELSTATE[',strMixer[rma][ch_atcrxvol].sInstanceID,'|level|mute|1|0|3]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvaVol_Ramps[RMA][ch_vtcrxvol].PORT),',LEVELSTATE[',strMixer[rma][ch_vtcrxvol].sInstanceID,'|level|mute|1|0|3]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvDialer_RmA.PORT),',VoIP[',strMixer[rma][ch_dialer].sInstanceID,'|',strMixer[rma][ch_controlstatus].sInstanceID,'|1]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvPrivacy_RmA.PORT),',STATE[',strMixer[rma][ch_privacy].sInstanceID,'|mute|1|0]'"	// 
//		// rmb
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvaVol_Ramps[RmB][ch_progvol].PORT),',LEVELSTATE[',strMixer[RmB][ch_progvol].sInstanceID,'|level|mute|1|0|3]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvaVol_Ramps[RmB][ch_atcrxvol].PORT),',LEVELSTATE[',strMixer[RmB][ch_atcrxvol].sInstanceID,'|level|mute|1|0|3]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvaVol_Ramps[RmB][ch_vtcrxvol].PORT),',LEVELSTATE[',strMixer[RmB][ch_vtcrxvol].sInstanceID,'|level|mute|1|0|3]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvDialer_rmb.PORT),',VoIP[',strMixer[rmb][ch_dialer].sInstanceID,'|',strMixer[rmb][ch_controlstatus].sInstanceID,'|1]'"
		SEND_COMMAND vdvMixer,"'AUDIOPROCADD-',ITOA(vdvPrivacy_RmB.PORT),',STATE[',strMixer[RmB][ch_privacy].sInstanceID,'|mute|1|0]'"	// 
		
		SEND_COMMAND vdvMixer,"'REINIT'"
	}
}


data_event[dvaDecoders]
{
	online:
	{
		local_var integer iDev
		iDev=get_last(dvaDecoders)
		
		switch(iDev)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			{
				on[strDest[rma][iDev].olFlag]
			}
			case 5:
			case 6:		// 6 is not an actual decoder, there is no display #2 in rmb
			case 7:
			case 8:
			{
				on[strDest[rmb][iDev-4].olFlag]
			}
		}        
		if(timeline_active(tlaDecoders_Status[iDev]))
			timeline_kill(tlaDecoders_Status[iDev])
	}
	offline:
	{
		local_var integer iDev
		iDev=get_last(dvaDecoders)
		
		ip_client_close(data.device.port)
		
		switch(iDev)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			{
				off[strDest[rma][iDev].olFlag]
			}
			case 5:
			case 6:
			case 7:
			case 8:
			{
				off[strDest[rmb][iDev-4].olFlag]
			}
		}        
		if(!timeline_active(tlaDecoders_Status[iDev]))
			timeline_create(tlaDecoders_Status[iDev],lDecoder_Status_Times,1,timeline_absolute,timeline_once)
	}
	onerror:
	{
		local_var integer iDev
		local_var integer nRm
		local_var integer iDest
		iDev=get_last(dvaDecoders)
		
		switch(iDev)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			{
				nRm=rma
				iDest=iDev
			}
			case 5:
			case 6:
			case 7:
			case 8:
			{
				nRm=rmb 
				iDest=idev-4
			}
		}        
		
		send_string 0,"'ip error:',itoa(data.number),' for decoder',itoa(get_last(dvaDecoders))"
		switch(data.number)
		{
			case 2: //General failure (out of memory)
			case 4: // Unknown host
			case 6: // Connection refused
			case 7: // Connection timed out
			case 8: // Unknown connection error
			//case 9: // Already closed
			case 14: // Local port already used
			case 16: // Too many open sockets
			case 17: // Local Port Not Open
			{	
				off[strDest[nrm][iDest].olFlag]
				ip_client_close(dvaDecoders[iDev].PORT)
				timeline_kill(tlIP_Connect)
				wait 20
				{
					timeline_create(tlIP_Connect,lIP_Connect_Times,1,timeline_absolute,timeline_repeat)
				}
			}
		}
	}
}


data_event[vdvDebug]
{
	command:
	{
		local_var integer nUnit
		if(length_array(remove_string(data.text,'reboot',1)))
		{
			nUnit=atoi(data.text)
			switch(nUnit)
			{
				case 1:
				case 2:
				case 3:
				case 4:
				{    
					strDest[rma][nUnit].cmdQ_dest="'reboot',$0D,$0A"
				}
				case 5:
				case 6:
				case 7:
				case 8:
				{    
					strDest[rmb][nUnit-4].cmdQ_dest="'reboot',$0D,$0A"
				}
				default:{}
			}
		}
		if(length_array(remove_string(data.text,'audioroute',1)))
		{
			local_var integer nDev
			local_var integer nAud
			nDev=atoi(remove_string(data.text,left_string(data.text,1),1))
			nAud=atoi(remove_string(data.text,left_string(data.text,1),1))
		}
	}
}

channel_event[vdvDebug,0]
{
	on:
	{
		switch(channel.channel)
		{
			case 1:	// clear all destination online flags
			{
				local_var integer nDebug_loop
				nDebug_loop=4//length_array(strDest[rma])
				while(nDebug_loop)
				{
					send_string 0,"'clearing>',itoa(nDebug_loop)"
					off[strDest[rma][nDebug_loop].olFlag]
					off[strDest[rmb][nDebug_loop].olFlag]
					
					strDest[rma][nDebug_loop].cmdQ_dest=""
					strDest[rmb][nDebug_loop].cmdQ_dest=""
					
					nDebug_loop--
				}
			}
			case 2:
			{
				ip_client_open(strDest[rma][1].dvDecoder.PORT,strDest[rma][1].sIP_Decoder,nPort_SVSi,1)
			}
			case 3:
			{
				ip_client_close(strDest[rma][1].dvDecoder.PORT)
			}
			case 4:
			case 5:
			{
				send_string dvDecoder_1,"'set:',itoa(channel.channel-1),$0D,$0A"
			}
			case 6:
			{
				timeline_create(tlIP_Connect,lIP_Connect_Times,1,timeline_absolute,timeline_repeat)
			}
			case 7:
			{
				if(timeline_active(tlIP_Connect))
					timeline_kill(tlIP_Connect)
			}
			case 8:
			{
				send_string dvDisplay_2,"'RSPW1   ',$0D"
			}
			case 9:
			{
				ip_client_open(dvDisplay_2.PORT,'192.168.1.54',50004,1)
			}
			case 10:
			{
				ip_client_close(dvDisplay_2.PORT)
			}
			case 11:
			{
				send_string 0,"'debug test verified'"
			}
			case 12:
			{
				if(timeline_active(tlIP_Connect))
					send_string 0,"'ip connect timeline active'"
				else
					send_string 0,"'ipconnect timeline inactive'"
			}
			case 13:
			{
				send_command vdvDebug,"'reboot1'"
			}
			case 14:
			{
				send_command vdvDebug,"'reboot5'"
			}
		}
	}
}
(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

[vdvOverflow,199]=![vdvProj,1]		// if the projector is not on, the overflow gate should be muted

nCMD_Q_Loop=4
while(nCMD_Q_Loop)
{
	if(length_array(strDest[rma][nCMD_Q_Loop].cmdQ_dest) && strDest[rma][nCMD_Q_Loop].olFlag)
	{
		send_string strDest[rma][nCMD_Q_Loop].dvDecoder,"remove_string(strDest[rma][nCMD_Q_Loop].cmdQ_dest,"$0D,$0A",1)"
	}
	nCMD_Q_Loop--
}

nCMD_Q_Loop=4
while(nCMD_Q_Loop)
{
	if(length_array(strDest[rmb][nCMD_Q_Loop].cmdQ_dest) && strDest[rmb][nCMD_Q_Loop].olFlag)
	{
		send_string strDest[rmb][nCMD_Q_Loop].dvDecoder,"remove_string(strDest[rmb][nCMD_Q_Loop].cmdQ_dest,"$0D,$0A",1)"
	}
	nCMD_Q_Loop--
}

wait 100	// check projector power vs lift state
{
	if(!flLift_Proj && [vdvProj,1])	// if we track the lift as up and the projector as on
		pulse[vdvProj,28]			// turn the projector off
}

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)

